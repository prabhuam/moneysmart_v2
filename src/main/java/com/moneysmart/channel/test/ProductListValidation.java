package com.moneysmart.channel.test;
/**
*
* @author Shenll Technologies Solutions
*
*/
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.common.utility.WebserviceRequest;
import com.common.utility.WebserviceRequestListener;
import com.model.Constants;
import com.model.ExcelCalculationInputData;
import com.model.ExcelInputData;
import com.model.ExcelProductValidationInputData;
import com.model.GetExcelInput;
import com.model.Constants.EXCEL_METHODS_INPUT;
import com.model.Constants.TEST_RESULT;
import com.testing.utility.GetWebElement;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ProductListValidation implements WebserviceRequestListener {
    /*
    * Global Variables Declarations
    */
    private static final Logger LOGGER = Logger.getLogger(ProductListValidation.class.getName());
    private WebserviceRequestListener webserviceListener;
    private boolean isAllProductNameValidatedAndSucessIf = true;
    private boolean isAllProductNameValidatedAndSucessElse = true;
    int totalCalculationCount = 0;
    int calculationMethodTestRunCount = 0;
    int productValidationMethodInvocationCount = 0;
    String SuccessMessage = "Product validated successfully";
    String SuccessComments = "Product in web Page and Api are equal";
    String FailureMessage = "Product in web Page and Api are not equal";
    String FailureComments = "Product in web Page and Api are not equal";
    String locatorNameForNLFFILtercalculateButton = "calculate-btn-1";
    String locatorNameForNLFFILterFieldValidation = "validation-message";
    String productValidationExpectedResultAll = "";
    String productValidationDescriptionAll = "";
    String productValidationActualResultAll = "";
    String SuccessActualResult = "";
    String FailureActualResult = "";
    String APINodeConcatLogicType = "";
    String Test = "";
    String dotSymbol = "\\.";
    String hyphenSymbol = "-";
    JSONObject webserviceApiResult;
    SoftAssert s_assert;
    String DuitpintarProviderId = "";
    String DuitpintaremploymentId = "";
    String channelCountry = " ";
    String pageUrl = "";
    String DuitpintarProviderMonth = "";
    String DuitpintarLocation = "";
    String Duitpintarloan_purposeId = "";
    String DuitpintarcollateralId = "";
    String Duitpintarmonthly_income = "";
    String DuitpintarLocationdownpayment_percent = "";
    String Duitpintarloan_amount = "";
    String Duitpintardown_payment = "";
    String LocatorName = "";
    GetExcelInput excelInputInstance = null;
    HashMap<String, HashMap<String, String>> apiNoeTypeAndMapValue = null;
    /*
    * Web Element Configure Values
    */
    String ParentCalssForMoneySmarSite = "//*[1][@class='filters-table-contents']";
    String ChildOneCalssForParentCalss = "filters-table-content";
    @Test
    public void DoFilterAPICall(ITestContext testContext) {
        try {
            // Here we need to get the first set of filter input value to call
            // API. In zero position, there will be the headers of each filters
            calculationMethodTestRunCount++;
            LOGGER.info("Calculation count = " + calculationMethodTestRunCount);
            // Init the GetExcelInput object. Use this.
            initExcelInputInstacne();
            // Get and set the each filter values in Website.
           
            Thread.sleep(10000);
            GetExcelInput getInput = new GetExcelInput();
            String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
            pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
            EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
            channelCountry = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_Country);
            String Browser_Firefox = getInput.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Firefox);
            String Browser_Chrome = getInput.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Chrome);
            String Browser_Safari = getInput.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Safari);
            // ////
            getWebDriver().navigate().refresh();
            Thread.sleep(8000);
            // int year = Calendar.getInstance().get(Calendar.YEAR);
            LOGGER.info("n pageUrl = " + pageUrl);
            LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
            Thread.sleep(8000);
            String token = getWebDriver().getPageSource();
            String exacttoke = token.replaceAll("\"", "Q");
            String exacttoken[] = exacttoke.split("\r\n|'|\r");
            if (Browser_Chrome.contains("Y")) {
                LOGGER.info("Test " + Browser_Chrome);
                LOGGER.info(exacttoken[1]);
                Constants.MYSTORE.ACESSTOKEN = exacttoken[1];
                LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
            }
            else if (Browser_Firefox.contains("Y")) {
                LOGGER.info("Test " + Browser_Firefox);
                LOGGER.info(exacttoken[5]);
                Constants.MYSTORE.ACESSTOKEN = exacttoken[5];
                LOGGER.info("Test" + Constants.MYSTORE.ACESSTOKEN);
                LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
            }
            else {
                LOGGER.info("Test " + Browser_Safari);
                LOGGER.info(exacttoken[1]);
                LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
                Constants.MYSTORE.ACESSTOKEN = exacttoken[1];
                LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++++");
                getWebDriver().navigate().to(pageUrl);
                Thread.sleep(8000);
            }
            // /////
            LOGGER.info("channelCountry2channelCountry2channelCountry2= " + channelCountry);
            String APIResponse = "";
            if (channelName.equalsIgnoreCase("savings-account") || channelName.contains("Savings")
            || channelName.contains("savings")) {
                if (channelCountry.equals("sg")) {
                    APIResponse = callAPiAndGetResponseForSavings();
                    } else {
                    doGetAndSetEachFilterInputInWebsite(channelName, calculationMethodTestRunCount);
                    // Click calculation button to display the products for
                    // calculated input
                    doClickCalculateButton();
                    APIResponse = callAPiAndGetResponse(getFiltersHashMap(), calculationMethodTestRunCount);
                }
                } else {
                doGetAndSetEachFilterInputInWebsite(channelName, calculationMethodTestRunCount);
                // Click calculation button to display the products for
                // calculated input
                doClickCalculateButton();
                APIResponse = callAPiAndGetResponse(getFiltersHashMap(), calculationMethodTestRunCount);
            }
            if (APIResponse != null && !APIResponse.isEmpty()) {
                setAPIResult(APIResponse);
                ExcelInputData excelInput = ExcelInputData.getInstance();
                JSONObject webResultJson;
                try {
                    webResultJson = new JSONObject(APIResponse);
                    excelInput.setAPIResultJson((Object) webResultJson);
                    } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    LOGGER.log(Level.SEVERE, "EXCEPTION", e1);
                }
                LOGGER.info("Response fetched from API");
                } else {
                // Please set a result map here that this method failed to
                // retrieve Response from API
                // And also set in attribute to display it in Report
                LOGGER.info("Error to fetch result from API");
            }
            // Set Result Map here
            } catch (Exception e) {
            // TODO Auto-generated catch block
            // Set Result Map here for Exception
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
    }
    /*
    * Initialiser Methods
    */
    public void initExcelInputInstacne() throws Exception {
        if (this.excelInputInstance == null) {
            this.excelInputInstance = new GetExcelInput();
        }
    }
    /*
    * Get Input Methods
    */
    private HashMap<String, ArrayList<String>> getFiltersHashMap() throws Exception {
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        ExcelCalculationInputData excelCalculationInputData = excelInputDataObject.getExcelCalculationInputData();
        HashMap<String, ArrayList<String>> calculationResultArray = excelCalculationInputData.getMethodInput();
        return calculationResultArray;
    }
    /*
    * Do Action Methods
    */
    private void doGetAndSetEachFilterInputInWebsite(String channelName, int calculationInput) throws Exception {
        for (Object key : getFiltersHashMap().keySet()) {
            String keyval = (String) key;
            ArrayList<String> inputValuesArray = getFiltersHashMap().get(keyval);
            String calcInputValue = inputValuesArray.get(calculationInput);
            doFilterActionAndSetInputValueBasedOnFilterType(channelName, getFilterWebElement(keyval),
            getLocator(keyval), calcInputValue);
            LOGGER.info("detailPageProductName " + keyval);
        }
    }
    public boolean isCutomTemplate() throws Exception {
        boolean isCutomTemplate = false;
        // DO YOUR WORK HERE
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map channelMap = (Map) YAMLObjectValue;
        if (channelMap.containsKey("configuration_file")) {
            isCutomTemplate = true;
        }
        return isCutomTemplate;
    }
    public boolean isNeedToGetValueFromExcelEvenIfNotCutomTemplate(String overrideKey, String currentMethodName)
    throws Exception {
        boolean isNeedToGetValueFromExcelEvenIfCutomTemplate = false;
        String overrideExcelValue = getValueFromExcel(currentMethodName, overrideKey);
        if (overrideExcelValue != null) {
            if (overrideExcelValue.equals("Y")) {
                isNeedToGetValueFromExcelEvenIfCutomTemplate = true;
            }
        }
        return isNeedToGetValueFromExcelEvenIfCutomTemplate;
    }
    public WebDriver getWebDriver() throws Exception {
        ExcelInputData excelInput = ExcelInputData.getInstance();
        WebDriver webDriver = excelInput.getWebDriver();
        return webDriver;
    }
    private void prepareDriver() throws Exception {
        String detailPageProductName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageProduct_MethodNameKey, EXCEL_METHODS_INPUT.DetailPageProductName);
        LOGGER.info("detailPageProductName " + detailPageProductName);
        LOGGER.info("prepare driver called");
        Thread.sleep(6000);
        getWebDriver().findElement(By.xpath("//a[contains(.,'" + detailPageProductName + "')]")).click();
        Thread.sleep(8000);
        /**
        * Set Driver commonly
        */
    }
    // Get Excel Value Methods
    public String getValueFromExcel(String methodNameKey, String valueKey) throws Exception {
        String valueFromExcel = this.excelInputInstance
        .get_A_Value_Using_Key_Of_Filters_Method(methodNameKey, valueKey);
        return valueFromExcel;
    }
    public WebElement getFilterWebElement(String currentFilterMethodName) throws Exception {
        GetWebElement obj = new GetWebElement();
        String Locator = getValueFromExcel(currentFilterMethodName, "Web Element Locator");
        LocatorName = getValueFromExcel(currentFilterMethodName, "Web Element");
        LOGGER.info("sdeswdwdwdwewe " + LocatorName);
        WebElement filterWebElement = obj.getWebElemntFromWebElemntType(getWebDriver(), Locator, LocatorName);
        return filterWebElement;
    }
    public String getLocator(String currentFilterMethodName) throws Exception {
        String requiredValue = "";
        requiredValue = getValueFromExcel(currentFilterMethodName, "Input Control");
        return requiredValue;
    }
    public String getLocatorName(String currentFilterMethodName) throws Exception {
        String requiredValue = "";
        if (!isCutomTemplate()) {
            if (isNeedToGetValueFromExcelEvenIfNotCutomTemplate("Override Yaml Value", currentFilterMethodName)) {
                // Get any values here if you need any values from excel even if
                // it is a not a custom template
                requiredValue = getValueFromExcel(currentFilterMethodName, "Web Element");
                } else {
                // get This value from YAML
                requiredValue = getLocatorNameFromYAML(currentFilterMethodName);
            }
            } else {
            // Get Valued from EXCEL
            requiredValue = getValueFromExcel(currentFilterMethodName, "Web Element");
        }
        return requiredValue;
    }
    public String getLocatorNameFromYAML(String currentMethodName) throws Exception {
        String locatorName = "";
        if (getFilterMapFromYAML(currentMethodName).containsKey("span_class")) {
            locatorName = (String) getTableFieldsMapFromYAML(currentMethodName).get("span_class");
        }
        return locatorName;
    }
    public Map getTableFieldsMapFromYAML(String currentMethodName) throws Exception {
        Map FilterComponentMap = null;
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map objectMap = (Map) YAMLObjectValue;
        Object FilterObject = objectMap.get("table_fields");
        Map FilterMap = (Map) FilterObject;
        if (!FilterMap.containsKey("custom_template")) {
            Object ColumbObject = FilterMap.get("columns");
            Map ColumnMap = (Map) ColumbObject;
            Object FieldMapObject = ColumnMap.get(currentMethodName);
            FilterComponentMap = (Map) FieldMapObject;
        }
        return FilterComponentMap;
    }
    public Map getFilterMapFromYAML(String currentFilterMethodName) throws Exception {
        Map FilterComponentMap = null;
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map objectMap = (Map) YAMLObjectValue;
        Object FilterObject = objectMap.get("filter");
        Map FilterMap = (Map) FilterObject;
        if (!FilterMap.containsKey("custom_template")) {
            Object ValidationsObject = FilterMap.get("validations");
            Map ValidationsMap = (Map) ValidationsObject;
            Object RulesObject = ValidationsMap.get("rules");
            Map RulesMap = (Map) RulesObject;
            String KEYInYAML = getValueFromExcel(currentFilterMethodName, "Filter Key In YAML");
            Object FilterComponentObject = RulesMap.get(KEYInYAML);
            FilterComponentMap = (Map) FilterComponentObject;
        }
        return FilterComponentMap;
    }
    /*
    * Action Methods
    */
    public void doFilterActionAndSetInputValueBasedOnFilterType(String channelName, WebElement webElement,
    String filtercomponentType, String calculationInput) throws Exception {
        LOGGER.info("channelCountry " + channelCountry);
        if (channelCountry.contains("sg")) {
            if (filtercomponentType.equals("textbox")) {
                webElement.clear();
                Thread.sleep(3000);
                webElement.sendKeys(calculationInput);
            }
            else if (filtercomponentType.equals("selectbox")) {
                LOGGER.info("12312312312313 LocatorName" + LocatorName);
                WebElement element = getWebDriver().findElement(By.xpath(LocatorName));
                LOGGER.info("12312312312313 elementelementelement" + element);
                LOGGER.info("12312312312313 calculationInput" + calculationInput);
                Select se = new Select(element);
                Thread.sleep(2000);
                se.selectByVisibleText(calculationInput);
                } else if (filtercomponentType.equals("checkbox")) {
                WebElement wb = webElement;
                WebElement chkbox = wb.findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
                chkbox.click();
                Thread.sleep(3000);
                } else if (filtercomponentType.equals("radiobutton")) {
                webElement.click();
                Thread.sleep(3000);
                WebElement rbutton = getWebDriver().findElement(
                By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
                rbutton.click();
                Thread.sleep(3000);
            }
            } else {
            if (filtercomponentType.equals("textbox")) {
                webElement.clear();
                Thread.sleep(3000);
                webElement.sendKeys(calculationInput);
                webElement.click();
            }
            if (filtercomponentType.equals("readonly")) {
                String newvalue = webElement.getText();
                LOGGER.info("newvalue " + newvalue);
                } else if (filtercomponentType.equals("selectbox")) {
                LOGGER.info("webElementwebElement " + webElement);
                /*
                * webElement.click(); Thread.sleep(3000);
                */
                if (channelName.equals("multi-purpose-loan")) {
                    if (pageUrl.contains("kredit-multiguna")) {
                        WebElement iam = getWebDriver().findElement(
                        By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
                        iam.click();
                        Thread.sleep(3000);
                        Duitpintarloan_purposeId = getWebDriver().findElement(By.id("loan_purpose")).getAttribute(
                        "value");
                        LOGGER.info("Duitpintarloan_purposeId= " + Duitpintarloan_purposeId);
                        Thread.sleep(3000);
                        DuitpintarcollateralId = getWebDriver().findElement(By.id("collateral_type")).getAttribute(
                        "value");
                        LOGGER.info("DuitpintarcollateralId= " + DuitpintarcollateralId);
                        } else {
                        LOGGER.info("12312312312313 " + channelName);
                        WebElement iam = getWebDriver().findElement(
                        By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
                        iam.click();
                        Thread.sleep(3000);
                    }
                    } else if (channelName.contains("auto-loans")) {
                    WebElement iam = getWebDriver().findElement(
                    By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
                    iam.click();
                    Thread.sleep(3000);
                    DuitpintarProviderMonth = getWebDriver().findElement(By.name("filters[loan_tenure]")).getAttribute(
                    "value");
                    LOGGER.info("DuitpintarProviderMonth= " + DuitpintarProviderMonth);
                    Thread.sleep(3000);
                    DuitpintarLocation = getWebDriver().findElement(By.id("location")).getAttribute("value");
                    LOGGER.info("DuitpintarLocation= " + DuitpintarLocation);
                    Duitpintarmonthly_income = getWebDriver().findElement(By.id("monthly_income"))
                    .getAttribute("value");
                    LOGGER.info("Duitpintarmonthly_income= " + Duitpintarmonthly_income);
                    Thread.sleep(3000);
                    DuitpintarLocationdownpayment_percent = getWebDriver().findElement(
                    By.name("filters[downpayment_percent]")).getAttribute("value");
                    LOGGER.info("DuitpintarLocationdownpayment_percent= " + DuitpintarLocationdownpayment_percent);
                    Thread.sleep(3000);
                    Duitpintarloan_amount = getWebDriver().findElement(By.name("filters[loan_amount]")).getAttribute(
                    "value");
                    LOGGER.info("Duitpintarloan_amount= " + Duitpintarloan_amount);
                    Thread.sleep(3000);
                    Duitpintardown_payment = getWebDriver().findElement(By.name("filters[down_payment]")).getAttribute(
                    "value");
                    LOGGER.info("Duitpintardown_payment= " + Duitpintardown_payment);
                    } else if (channelName.contains("savings-account")) {
                    LOGGER.info("Saving-id " + channelName);
                    WebElement iam = getWebDriver().findElement(
                    By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
                    iam.click();
                    Thread.sleep(3000);
                    } else {
                    WebElement iam = getWebDriver().findElement(
                    By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
                    iam.click();
                    Thread.sleep(3000);
                    DuitpintarProviderId = getWebDriver().findElement(By.id("inquiry_provider")).getAttribute("value");
                    LOGGER.info("TqTqTqTqTqTq= " + DuitpintarProviderId);
                    Thread.sleep(3000);
                    DuitpintaremploymentId = getWebDriver().findElement(By.id("inquiry_employment")).getAttribute(
                    "value");
                    LOGGER.info("DuitpintaremploymentId= " + DuitpintaremploymentId);
                }
                } else if (filtercomponentType.equals("checkbox")) {
                WebElement wb = webElement;
                WebElement chkbox = wb.findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
                chkbox.click();
                Thread.sleep(5000);
            }
        }
    }
    /*
    * Other Helper Methods
    */
    public void doClickCalculateButton() throws Exception {
        Thread.sleep(5000);
        getWebDriver().findElement(By.id(locatorNameForNLFFILtercalculateButton)).click();
        Thread.sleep(10000);
        // Check whether calculation button is allowed or not and return a
        // boolean
    }
    /*
    * Do Helper Methods
    */
    private String callAPiAndGetResponse(HashMap<String, ArrayList<String>> filtersKeyValue, int apiInvocationCount)
    throws Exception {
        LOGGER.info("Call API and get response Method");
        String apiRequestUrl = createWebserviceRequestUrl(filtersKeyValue, apiInvocationCount);
        WebserviceRequest webserviceRequest = new WebserviceRequest();
        webserviceListener = this;
        LOGGER.info("Webservice Request : " + apiRequestUrl);
        String jsonResult = webserviceRequest.GET(webserviceListener, apiRequestUrl);
        if (jsonResult.isEmpty() || jsonResult == null) {
            LOGGER.info("Result is empty or null");
        }
        return jsonResult;
    }
    private String callAPiAndGetResponseForSavings() throws Exception {
        LOGGER.info("Savings API");
        String apiRequestUrl = "https://api.loangarage.com/api/v1/single-page-wizard-product/savings-account/summary?access_token="
        + Constants.MYSTORE.ACESSTOKEN
        + "&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light&sort%5B0%5D=cpa&sort%5B1%5D=specifications.rate&order%5B0%5D=desc&order%5B1%5D=desc";
        Constants.MYSTORE.SINGLEPRODUCTJSONAPI = apiRequestUrl;
        WebserviceRequest webserviceRequest = new WebserviceRequest();
        webserviceListener = this;
        LOGGER.info("Webservice Request : " + apiRequestUrl);
        String jsonResult = webserviceRequest.GET(webserviceListener, apiRequestUrl);
        if (jsonResult.isEmpty() || jsonResult == null) {
            LOGGER.info("Result is empty or null");
        }
        return jsonResult;
    }
    /*
    * Product Name Validation Start
    */
    @Test
    public void DoProductValidation(ITestContext testContext) {
        String testMethodName = "DoProductValidation";
        String testMethodNameReport = "Product List Validation";
        String productValidationMethodNameKey = "DoProductValidation";
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        try {
            LOGGER.info("Filter Validation Count = " + productValidationMethodInvocationCount);
            productValidationMethodInvocationCount++;
            int getRespectiveProductValidationCountFromExcel = productValidationMethodInvocationCount - 1;
            LOGGER.info("getRespectiveProductValidationCountFromExcel = "
            + getRespectiveProductValidationCountFromExcel);
            String CountryType = "";
            String PageUrl = "";
            String executeOrNotExcel = "";
            String Locator = "";
            String LocatorName = "";
            String APINodeExcel = "";
            String APINodeYAML = "";
            String productValidationDescription = "";
            String productValidationExpectedResult = "";
            String productValidationActualResult = "";
            String APINodeExcelSecond = "";
            /*
            * Get required input values from Excel. Filter section Get Values
            * start
            */
            ExcelInputData excelInputData = ExcelInputData.getInstance();
            GetExcelInput getInput = new GetExcelInput();
            String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
            EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
            LOGGER.info("channelNamechannelNamechannelName = " + channelName);
            ArrayList<HashMap<String, ExcelProductValidationInputData>> excelInputProductValidatinArray = excelInputData
            .getExcelProductValidationInputData();
            HashMap<String, ExcelProductValidationInputData> excelInputProductValidationrMap = excelInputProductValidatinArray
            .get(getRespectiveProductValidationCountFromExcel);
            productValidationMethodNameKey = (String) excelInputProductValidationrMap.keySet().toArray()[getRespectiveProductValidationCountFromExcel];
            productValidationMethodNameKey = productValidationMethodNameKey.trim();
            LOGGER.info("n =====================" + productValidationMethodNameKey + "=====================");
            executeOrNotExcel = getInput.get_A_Value_Using_Key_Of_A_Method(productValidationMethodNameKey.toString()
            .trim(), TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
            LOGGER.info("executeOrNotExcel all method = " + executeOrNotExcel);
            executeOrNotExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
            productValidationMethodNameKey.toString().trim(), TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
            LOGGER.info("executeOrNotExcel pro method = " + executeOrNotExcel);
            LOGGER.info(" executeOrNotExcel : " + executeOrNotExcel);
            if (executeOrNotExcel != null && !executeOrNotExcel.isEmpty() && !executeOrNotExcel.equalsIgnoreCase("Y")) {
                throw new SkipException("Product validation skippped for Filter: " + productValidationMethodNameKey);
            }
            ExcelProductValidationInputData productValidation_InputData = excelInputProductValidationrMap
            .get(productValidationMethodNameKey);
            HashMap<String, String> productValidationInputDataMap = productValidation_InputData.getMethodInput();
            CountryType = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
            "Country");
            PageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
            EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
            Locator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey,
            "Web Element Locator");
            LocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey,
            "Web Element");
            APINodeExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey,
            "Api Node");
            if (APINodeExcel.contains("|")) {
                APINodeExcelSecond = APINodeExcel;
                String rep = APINodeExcel.replace("|", hyphenSymbol);
                String trimVal = rep.split(hyphenSymbol)[1];
                LOGGER.info("trimVal: " + trimVal);
                APINodeExcel = trimVal.toString().trim();
            }
            productValidationDescription = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
            productValidationMethodNameKey, "Test Description");
            productValidationExpectedResult = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
            productValidationMethodNameKey, "Expected Test Result");
            productValidationActualResult = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
            productValidationMethodNameKey, "Actual Result");
            if (productValidationDescriptionAll.length() == 0) {
                productValidationDescriptionAll = productValidationDescriptionAll + productValidationDescription;
                } else {
                productValidationDescriptionAll = productValidationDescriptionAll + ", " + productValidationDescription;
            }
            if (productValidationExpectedResultAll.length() == 0) {
                productValidationExpectedResultAll = productValidationExpectedResultAll
                + productValidationExpectedResult;
                } else {
                productValidationExpectedResultAll = productValidationExpectedResultAll + ", "
                + productValidationExpectedResult;
            }
            if (productValidationActualResultAll.length() == 0) {
                productValidationActualResultAll = productValidationActualResultAll + productValidationActualResult;
                } else {
                productValidationActualResultAll = productValidationActualResultAll + ", "
                + productValidationActualResult;
            }
            String staticLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Bank Name",
            "Web Element Locator");
            String staticElement = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Bank Name",
            "Web Element");
            LOGGER.info(".. staticLocator = " + staticLocator);
            LOGGER.info(".. Locator = " + Locator);
            LOGGER.info(".. LocatorName = " + LocatorName);
            LOGGER.info(".. APINodeExcel = " + APINodeExcel);
            Locator = "className";
            LOGGER.info(".. staticElement = " + staticElement);
            LOGGER.info(".. staticLocator = " + Locator);
            /*
            * Read YAML Input
            */
            ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
            Object YAMLObjectValue = excelInputDataObject.getYAMLData();
            Map objectMap = (Map) YAMLObjectValue;
            Object TableFieldsObject = objectMap.get("table_fields");
            Map TableFieldsMap = (Map) TableFieldsObject;
            Object ColumnsObject = TableFieldsMap.get("columns");
            Map ColumnsMap = (Map) ColumnsObject;
            if (channelName.contains("auto-loans")) {
                if (!productValidationMethodNameKey.equals("Bank Name")) {
                    LOGGER.info("n ColumnsMap = " + ColumnsMap);
                    LOGGER.info(".. productValidationMethodNameKey= " + productValidationMethodNameKey);
                    Object ProductValidationComponentObject = ColumnsMap.get(productValidationMethodNameKey);
                    LOGGER.info(".. ColumnsMap ColumnsMap= " + ColumnsMap);
                    LOGGER.info(".. ProductValidationComponentObject = " + ProductValidationComponentObject);
                    Map ProductValidationComponentMap = (Map) ProductValidationComponentObject;
                    LOGGER.info(".. ProductValidationComponentMap = " + ProductValidationComponentMap);
                    LOGGER.info(".. staticLocator = " + Locator);
                    Locator = "className";
                    APINodeYAML = (String) ProductValidationComponentMap.get("value");
                    LocatorName = (String) ProductValidationComponentMap.get("span_class");
                    LOGGER.info("n AutoLoan==LocatorName = " + LocatorName);
                    if (LocatorName.equals("int_rate")) {
                        LOGGER.info("ToTal--CoUnt-- =  LocatorName " + LocatorName);
                        LocatorName = LocatorName.replaceAll("int_rate", "rate-type").toString().trim();
                        // Locator = "cssSelector";
                        LOGGER.info("n ===LocatorName = " + LocatorName);
                        } else if (LocatorName.equals("down_payment")) {
                        LOGGER.info("ToTal--CoUnt-- =  LocatorName " + LocatorName);
                        LocatorName = LocatorName.replaceAll("down_payment", "lock").toString().trim();
                        Locator = "className";
                        LOGGER.info("n ===LocatorName = " + LocatorName);
                    }
                    else if (LocatorName.equals("installment")) {
                        LOGGER.info("ToTal--CoUnt-- =  LocatorName " + LocatorName);
                        LocatorName = LocatorName.replaceAll("installment", "rate").toString().trim();
                        Locator = "className";
                        LOGGER.info("n ===LocatorName = " + LocatorName);
                        } else {
                        LOGGER.info("ToTal--CoUnt-- =  LocatorName " + LocatorName);
                        LocatorName = LocatorName.replaceAll("first_payment", "instalment").toString().trim();
                        Locator = "className";
                        LOGGER.info("n ===LocatorName = " + LocatorName);
                    }
                }
                } else if (channelName.contains("Savings Account")) {
                if (!productValidationMethodNameKey.equals("Bank Name")) {
                    LOGGER.info("n productMethodNameKey = " + productValidationMethodNameKey);
                    LOGGER.info("n ColumnsMap = " + ColumnsMap);
                    LOGGER.info(".. productValidationMethodNameKey= " + productValidationMethodNameKey);
                    Object ProductValidationComponentObject = ColumnsMap.get(productValidationMethodNameKey);
                    LOGGER.info(".. ColumnsMap ColumnsMap= " + ColumnsMap);
                    LOGGER.info(".. ProductValidationComponentObject = " + ProductValidationComponentObject);
                    Map ProductValidationComponentMap = (Map) ProductValidationComponentObject;
                    LOGGER.info(".. ProductValidationComponentMap = " + ProductValidationComponentMap);
                    LOGGER.info(".. staticLocator = " + Locator);
                    Locator = "className";
                    APINodeYAML = (String) ProductValidationComponentMap.get("value");
                    LocatorName = (String) ProductValidationComponentMap.get("span_class");
                    LOGGER.info("n ColumnsMap==LocatorName = " + LocatorName);
                    if (LocatorName.equals("results__item")) {
                        LOGGER.info("ToTal--CoUnt-- =  LocatorName " + LocatorName);
                        LocatorName = LocatorName.replaceAll("results__item", ".col-md-3.col-xs-12.box__results__item")
                        .toString().trim();
                        Locator = "cssSelector";
                        LOGGER.info("n ===LocatorName = " + LocatorName);
                    }
                }
                } else {
                if (!TableFieldsMap.containsKey("custom_template")) {
                    if (!productValidationMethodNameKey.equals("Bank Name")) {
                        LOGGER.info("n ColumnsMap = " + ColumnsMap);
                        LOGGER.info(".. productValidationMethodNameKey= " + productValidationMethodNameKey);
                        Object ProductValidationComponentObject = ColumnsMap.get(productValidationMethodNameKey);
                        Map ProductValidationComponentMap = (Map) ProductValidationComponentObject;
                        Locator = "className";
                        APINodeYAML = (String) ProductValidationComponentMap.get("value");
                        LocatorName = (String) ProductValidationComponentMap.get("span_class");
                    }
                }
            }
            /*
            * Check Condition
            */
            // Get the no of input counts from Excel
            ExcelInputData excelInput = ExcelInputData.getInstance();
            ExcelCalculationInputData excelCalculationInputData = excelInput.getExcelCalculationInputData();
            HashMap<String, ArrayList<String>> calculationResultArray = excelCalculationInputData.getMethodInput();
            Set<String> calculationResultKeySet = calculationResultArray.keySet();
            int filterInputValueCount = 0;
            for (String filterKey : calculationResultKeySet) {
                filterInputValueCount = calculationResultArray.get(filterKey).size();
                break;
            }
            LOGGER.info("Input count = " + filterInputValueCount);
            WebElement element = getWebDriver().findElement(By.xpath(ParentCalssForMoneySmarSite));
            List<WebElement> contentList = element.findElements(By.className(ChildOneCalssForParentCalss));
            Thread.sleep(10000);
            boolean isAllProductNameValidatedAndSucess = true;
            if (productValidationMethodNameKey.equals("Bank Name")) {
                String needTrim = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                productValidationMethodNameKey, "Trim");
                String trimValue = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                productValidationMethodNameKey, "Trim Value");
                String isConcat = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                productValidationMethodNameKey, "Concat");
                String concatType = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                productValidationMethodNameKey, "Concat Type");
                String concatValue = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                productValidationMethodNameKey, "Concat Value");
                LOGGER.info("Locator = " + Locator);
                LOGGER.info("LocatorName = " + LocatorName);
                LOGGER.info("APINodeExcel = " + APINodeExcel);
                LOGGER.info("needTrim = " + needTrim);
                LOGGER.info("trimValue = " + trimValue);
                LOGGER.info("isConcat = " + isConcat);
                LOGGER.info("concatType = " + concatType);
                LOGGER.info("concatValue = " + concatValue);
                boolean isAnotherApiNode = false;
                boolean isNeedTrim = false;
                boolean isNeedisConcat = false;
                String productTypeAnotherAPINode = "";
                if (needTrim.contains("Y")) {
                    isNeedTrim = true;
                }
                if (isConcat.contains("Y")) {
                    isNeedisConcat = true;
                }
                // Get the array from Web
                List<String> checkingValueArrayFromWeb = getListOfProductValueFromWebForParticularTypeInArray(
                contentList, Locator, LocatorName);
                LOGGER.info("ToTal--CoUnt-- = " + checkingValueArrayFromWeb.size());
                for (int webValueCoutn = 0; webValueCoutn < checkingValueArrayFromWeb.size(); webValueCoutn++) {
                    LOGGER.info("Web Value Name = " + checkingValueArrayFromWeb.get(webValueCoutn));
                }
                List<String> checkingValueArrayFromApi = getListOfProductValueFromAPIForParticularTypeInArray(
                getAPIResult(), APINodeExcel, isAnotherApiNode, productTypeAnotherAPINode, dotSymbol,
                CountryType, true, isNeedTrim, trimValue, isNeedisConcat, concatType, concatValue);
                for (int apiValueCoutn = 0; apiValueCoutn < checkingValueArrayFromApi.size(); apiValueCoutn++) {
                    LOGGER.info("API Value Name = " + checkingValueArrayFromApi.get(apiValueCoutn));
                }
                for (int productCount = 0; productCount < checkingValueArrayFromApi.size(); productCount++) {
                    String productInWebAndAPI = "";
                    String productInWeb = checkingValueArrayFromWeb.get(productCount);
                    String productInApi = checkingValueArrayFromApi.get(productCount);
                    LOGGER.info("Product Validation   xxxxxxxxx productInApi:" + productInApi);
                    LOGGER.info("Product Validation   xxxxxxxxx productInWeb:" + productInWeb);
                    if (productInApi.contains("Rp")) {
                        String replace_Value = productInWeb.replace("Rp", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInApi = replace_Value.split(hyphenSymbol)[1];
                        LOGGER.info("productInApi Rp : " + productInApi);
                    }
                    /*
                    * Checking Area
                    */
                    LOGGER.info("productInWeb check: " + productInWeb);
                    LOGGER.info("productInApi check: " + productInApi);
                    if (APINodeExcelSecond.contains("|")) {
                        String rep = APINodeExcelSecond.replace("|", hyphenSymbol);
                        String trimVal = rep.split(hyphenSymbol)[0];
                        LOGGER.info("LOGGER: " + trimVal);
                        String apiVal = getAnotherAPIValue(getAPIResult(), trimVal, dotSymbol, CountryType,
                        productCount);
                        LOGGER.info("apiVal: " + apiVal);
                        productInApi = apiVal.toString().trim() + " " + productInApi;
                        LOGGER.info("conact api value: " + productInApi);
                    }
                    if (productInWeb.toString().trim().equals(productInApi.toString().trim())) {
                        LOGGER.info("Product Validation : Passed");
                        LOGGER.info("productInWeb : " + productInWeb);
                        LOGGER.info("productInApi : " + productInApi);
                        SuccessActualResult = "There are " + calculationMethodTestRunCount
                        + " calculation records in the excel. So this method excuted "
                        + calculationMethodTestRunCount + "times" + "n";
                        } else {
                        isAllProductNameValidatedAndSucess = false;
                        LOGGER.info("Product Validation : Failed");
                        LOGGER.info("productInWeb : " + productInWeb);
                        LOGGER.info("productInApi : " + productInApi);
                        productInWebAndAPI = productInWeb + ", " + productInApi;
                        LOGGER.info("Product Validation  productNameWebAndAPI:" + productInWebAndAPI);
                        FailureActualResult = FailureActualResult + "(" + checkingValueArrayFromApi.get(productCount)
                        + ") This product is not matching with API product " + "("
                        + checkingValueArrayFromWeb.get(productCount) + ") n";
                    }
                    /*
                    * Checking Area
                    */
                    if (isAllProductNameValidatedAndSucess) {
                        isAllProductNameValidatedAndSucessIf = true;
                        } else {
                        isAllProductNameValidatedAndSucessIf = false;
                    }
                }
                } else {
                String productTypeAPINode = APINodeExcel;
                boolean isAnotherApiNode = false;
                String productTypeAnotherAPINode = "";
                if (!TableFieldsMap.containsKey("custom_template")) {
                    if (!productValidationMethodNameKey.equals("Bank Name")) {
                        concatIfAnyStringsNeedToConcatInYAML(APINodeYAML, CountryType);
                        HashMap<String, String> resultMapAPI = getAPINodeValueToParse(APINodeYAML, CountryType);
                        productTypeAPINode = resultMapAPI.get("apiNodeValue");
                        if (resultMapAPI.get("isAnotherAPIConcat").contains("Y")) {
                            isAnotherApiNode = true;
                            productTypeAnotherAPINode = resultMapAPI.get("apiValueTobeConcat");
                        }
                    }
                    } else {
                    APINodeYAML = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(
                    productValidationMethodNameKey, "Api Node");
                    concatIfAnyStringsNeedToConcatInYAML(APINodeYAML, CountryType);
                    HashMap<String, String> resultMapAPI = getAPINodeValueToParse(APINodeYAML, CountryType);
                    productTypeAPINode = resultMapAPI.get("apiNodeValue");
                    if (resultMapAPI.get("isAnotherAPIConcat").contains("Y")) {
                        isAnotherApiNode = true;
                        productTypeAnotherAPINode = resultMapAPI.get("apiValueTobeConcat");
                    }
                }
                // Get the array from Web
                List<String> checkingValueArrayFromWeb = getListOfProductValueFromWebForParticularTypeInArray(
                contentList, Locator, LocatorName);
                LOGGER.info("Web ValuecheckingValueArrayFromWebcheckingValueArrayFromWebcheckingValueArrayFromWeb  = "
                + checkingValueArrayFromWeb.size());
                for (int webValueCoutn = 0; webValueCoutn < checkingValueArrayFromWeb.size(); webValueCoutn++) {
                    LOGGER.info("Web Value  = " + checkingValueArrayFromWeb.get(webValueCoutn));
                }
                // Get the array from API Result
                if (getAPIResult() == null || productTypeAPINode == null) {
                    LOGGER.info("Json/productTypeAPINode has null value : " + productTypeAPINode);
                }
                List<String> checkingValueArrayFromApi = getListOfProductValueFromAPIForParticularTypeInArray(
                getAPIResult(), productTypeAPINode, isAnotherApiNode, productTypeAnotherAPINode, dotSymbol,
                CountryType, false, false, "", false, "", "");
                for (int apiValueCoutn = 0; apiValueCoutn < checkingValueArrayFromApi.size(); apiValueCoutn++) {
                    LOGGER.info("API Value  = " + checkingValueArrayFromApi.get(apiValueCoutn));
                }
                int productcountinweb = 0;
                WebElement showall;
                try {
                    showall = getWebDriver().findElement(By.id("total-info"));
                    if (showall.isDisplayed()) {
                        String totalProductCount = getWebDriver().findElement(By.id("total-info")).getText();
                        Thread.sleep(9000);
                        String[] productvalues = totalProductCount.split("products");
                        String totalproductvalue = productvalues[0];
                        productcountinweb = Integer.parseInt(totalproductvalue.trim());
                        } else {
                        LOGGER.info("not display toyal info:");
                    }
                    } catch (Exception e) {
                    showall = getWebDriver().findElement(By.className("all-results-loaded"));
                    if (showall.isDisplayed()) {
                        WebElement elementTotalProd = null;
                        By elementIdentifiedBy = By.className("all-results-loaded");
                        Thread.sleep(5000);
                        elementTotalProd = getWebDriver().findElement(elementIdentifiedBy);
                        Thread.sleep(3000);
                        String allResultsShowed = "";
                        if (elementTotalProd != null) {
                            allResultsShowed = elementTotalProd.getText().toString().trim();
                            LOGGER.info("allResultsShowed not null :" + allResultsShowed);
                        }
                        if (allResultsShowed.contains("Showing")) {
                            String replaceVal = allResultsShowed.replace("Showing", hyphenSymbol);
                            String firstVal = replaceVal.split(hyphenSymbol)[1];
                            LOGGER.info("firstVal :" + firstVal);
                            String replaceSpace = firstVal.replace("matching", hyphenSymbol);
                            String totalproductvalue = replaceSpace.split(hyphenSymbol)[0];
                            LOGGER.info("totalproductvalue :" + totalproductvalue);
                            productcountinweb = Integer.parseInt(totalproductvalue.toString().trim());
                            } else if (allResultsShowed.contains("Menampilkan")) {
                            String replaceVal = allResultsShowed.replace("Menampilkan", hyphenSymbol);
                            String firstVal = replaceVal.split(hyphenSymbol)[1];
                            LOGGER.info("MenampilkanfirstVal :" + firstVal);
                            String replaceSpace = firstVal.replace("sesuai", hyphenSymbol);
                            String totalproductvalue = replaceSpace.split(hyphenSymbol)[0];
                            LOGGER.info("sesuaitotalproductvalue :" + totalproductvalue);
                            productcountinweb = Integer.parseInt(totalproductvalue.toString().trim());
                        }
                        } else {
                        LOGGER.info("not display all reslt info:");
                    }
                }
                int productcountinapi = checkingValueArrayFromApi.size();
                productcountinweb = checkingValueArrayFromWeb.size();
                LOGGER.info("----WEB----:" + productcountinweb + "----API----" + productcountinapi);
                if (productcountinweb != productcountinapi) {
                    LOGGER.info("Product Validation  productNameWebAndAPI:" + checkingValueArrayFromApi);
                    LOGGER.info("Product Validation  checkingValueArrayFromWeb:" + checkingValueArrayFromWeb);
                    isAllProductNameValidatedAndSucess = false;
                    FailureActualResult = "There are " + calculationMethodTestRunCount
                    + " calculation records in the excel. So this method excuted "
                    + calculationMethodTestRunCount + "times" + "n";
                    FailureActualResult = FailureActualResult + "Total Product count in API is "
                    + checkingValueArrayFromApi + ". But total products display in website is "
                    + checkingValueArrayFromWeb + ". n";
                }
                int leastProductCount = 0;
                if (productcountinweb < productcountinapi) {
                    leastProductCount = productcountinweb;
                    } else {
                    leastProductCount = productcountinapi;
                }
                for (int productCount = 0; productCount < leastProductCount; productCount++) {
                    String productInWebAndAPI = "";
                    String productInWeb = checkingValueArrayFromWeb.get(productCount);
                    String productInApi = checkingValueArrayFromApi.get(productCount);
                    if (productInWeb.contains("%")) {
                        String replace_Value = productInWeb.replace("%", hyphenSymbol);
                        LOGGER.info("replace Value %: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb % : " + productInWeb);
                    }
                    if (productInApi.contains("%")) {
                        String replace_Value = productInApi.replace("%", hyphenSymbol);
                        LOGGER.info("replace Value %: " + replace_Value);
                        productInApi = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInApi % : " + productInApi);
                    }
                    if (productInWeb.contains("Interest")) {
                        String replace_Value = productInWeb.replace("Interest", hyphenSymbol);
                        LOGGER.info("replace  Interest: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Interest : " + productInWeb);
                    }
                    if (productInWeb.contains("Interest")) {
                        String replace_Value = productInWeb.replace("Interest", hyphenSymbol);
                        LOGGER.info("replace  Interest: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Interest : " + productInWeb);
                    }
                    if (productInWeb.contains("Interest")) {
                        String replace_Value = productInWeb.replace("Interest", hyphenSymbol);
                        LOGGER.info("replace  Interest: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Interest : " + productInWeb);
                    }
                    if (productInApi.contains("Rp.")) {
                        String replace_Value = productInApi.replace("Rp.", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInApi = replace_Value.split(hyphenSymbol)[1];
                        LOGGER.info("productInApi Rp : " + productInApi);
                    }
                    if (productInWeb.contains("Suku Bunga")) {
                        String replace_Value = productInWeb.replace("Suku Bunga", hyphenSymbol);
                        LOGGER.info("replaceValue Suku Bunga: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Suku Bunga : " + productInWeb);
                    }
                    if (productInWeb.contains("Studies")) {
                        String replace_Value = productInWeb.replace("Studies", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Studies : " + productInWeb);
                    }
                    if (productInWeb.contains("bulan")) {
                        String replaceValueAnnually = productInWeb.replace("bulan", hyphenSymbol);
                        LOGGER.info("replaceValue bulan: " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb bulan: " + productInWeb);
                    }
                    if (productInWeb.contains("Tenor Pinjaman")) {
                        String replaceValueAnnually = productInWeb.replace("Tenor Pinjaman", hyphenSymbol);
                        LOGGER.info("replaceValue Tenor Pinjaman: " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWebTenor Pinjaman: " + productInWeb);
                    }
                    if (productInWeb.contains("Tiap Tahun")) {
                        String replaceValueAnnually = productInWeb.replace("Tiap Tahun", hyphenSymbol);
                        LOGGER.info("replaceValue Tiap Tahun: " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWebTiap Tahun: " + productInWeb);
                    }
                    if (productInWeb.contains("Tiap Bulan")) {
                        String replaceValueAnnually = productInWeb.replace("Tiap Bulan", hyphenSymbol);
                        LOGGER.info("replaceValue Tiap Tahun: " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWebTiap Bulan: " + productInWeb);
                    }
                    if (productInWeb.contains(",")) {
                        String replaceValueComa = productInWeb.replace(",", "");
                        LOGGER.info("replaceValue Coma : " + replaceValueComa);
                        productInWeb = replaceValueComa;
                        LOGGER.info("productInWeb Coma: " + productInWeb);
                    }
                    if (productInApi.contains(",")) {
                        String replaceValueComa = productInApi.replace(",", "");
                        LOGGER.info("replaceValue Coma : " + replaceValueComa);
                        productInWeb = productInApi;
                        LOGGER.info("productInAPI Coma: " + productInWeb);
                    }
                    if (productInWeb.contains("IDR")) {
                        String replace_Value = productInWeb.replace("IDR", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[1];
                        LOGGER.info("productInWeb IDR : " + productInWeb);
                    }
                    if (productInWeb.contains("Down Payment")) {
                        String replace_Value = productInWeb.replace("Down Payment", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Down Payment : " + productInWeb);
                    }
                    if (productInWeb.contains("Installment")) {
                        String replace_Value = productInWeb.replace("Installment", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb Installment : " + productInWeb);
                    }
                    if (productInWeb.contains("Rp.")) {
                        String replace_Value = productInWeb.replace("Rp.", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replace_Value);
                        productInWeb = replace_Value.split(hyphenSymbol)[1];
                        LOGGER.info("productInWeb Rp : " + productInWeb);
                    }
                    if (productInWeb.contains("months")) {
                        String replaceValueAnnually = productInWeb.replace("months", hyphenSymbol);
                        LOGGER.info("replaceValue months: " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb months: " + productInWeb);
                    }
                    if (productInApi.contains("months")) {
                        String replaceValuemonths = productInApi.replace("months", hyphenSymbol);
                        LOGGER.info("replaceValue months: " + replaceValuemonths);
                        productInApi = replaceValuemonths.split(hyphenSymbol)[0];
                        LOGGER.info("productInApi months: " + productInApi);
                    }
                    if (productInWeb.contains("Annually")) {
                        String replaceValueAnnually = productInWeb.replace("Annually", hyphenSymbol);
                        LOGGER.info("replaceValue Annually " + replaceValueAnnually);
                        productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed Annually: " + productInWeb);
                    }
                    if (productInWeb.contains("Per Month")) {
                        String replaceValue = productInWeb.replace("Per Month", hyphenSymbol);
                        LOGGER.info("replaceValue per month: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed per month: " + productInWeb);
                        productInWeb = productInWeb.replace(",", "");
                        LOGGER.info(" After Trim replaceValuemonth in Web: " + productInWeb);
                        if (productInWeb.contains("IDR")) {
                            String _replaceValue = productInWeb.replace("IDR", hyphenSymbol);
                            LOGGER.info("replaceValue: " + _replaceValue);
                            productInWeb = _replaceValue.split(hyphenSymbol)[1];
                            LOGGER.info("productInWeb IDR : " + productInWeb);
                        }
                        } else if (productInWeb.contains("Interest Earned")) {
                        String replaceValue = productInWeb.replace("Interest Earned", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("Interest Earned trimmed : " + productInWeb);
                        } else if (productInWeb.contains("Annual Interest Rate")) {
                        String replaceValue = productInWeb.replace("Annual Interest Rate", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("Annual Interest Rate trimmed : " + productInWeb);
                        } else if (productInWeb.contains("Interest Rate")) {
                        String replaceValue = productInWeb.replace("Interest Rate", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed : " + productInWeb);
                    }
                    // /////////////////
                    else if (productInWeb.contains("Loan")) {
                        if (productInWeb.contains("Tenure")) {
                            String replaceValue = productInWeb.replace("Loan Tenure", hyphenSymbol);
                            LOGGER.info("replaceValue tenure: " + replaceValue);
                            productInWeb = replaceValue.split(hyphenSymbol)[0];
                            LOGGER.info("productInWeb trimmed loan: " + productInWeb);
                            } else {
                            String replaceValue = productInWeb.replace("Loan", hyphenSymbol);
                            LOGGER.info("replaceValue: " + replaceValue);
                            productInWeb = replaceValue.split(hyphenSymbol)[0];
                            LOGGER.info("productInWeb trimmed loan: " + productInWeb);
                        }
                        String replaceValue = productInWeb.replace("Loan Tenure", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed : " + productInWeb);
                        } else if (productInWeb.contains("Tenure")) {
                        if (productInWeb.contains("Loan Tenure")) {
                            } else {
                            String replaceValue = productInWeb.replace("Tenure", hyphenSymbol);
                            LOGGER.info("replaceValue: " + replaceValue);
                            productInWeb = replaceValue.split(hyphenSymbol)[0];
                            LOGGER.info("productInWeb Tenure : " + productInWeb);
                        }
                        } else if (productInWeb.contains("Monthly rest")) {
                        String replaceValue = productInWeb.replace("Monthly rest", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed : " + productInWeb);
                        } else if (productInWeb.contains("Initial Deposit")) {
                        String replaceValue = productInWeb.replace("Initial Deposit", hyphenSymbol);
                        LOGGER.info("replaceValue Initial Deposit: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed  Initial Deposit: " + productInWeb);
                        } else if (productInWeb.contains("Excellent")) {
                        String replaceValue = productInWeb.replace("Excellent", hyphenSymbol);
                        LOGGER.info("replaceValue Excellent: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed Excellent: " + productInWeb);
                        } else if (productInWeb.contains("Very Good")) {
                        String replaceValue = productInWeb.replace("Very Good", hyphenSymbol);
                        LOGGER.info("replaceValue Very Good: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed Very Good: " + productInWeb);
                        } else if (productInWeb.contains("Satisfactory")) {
                        String replaceValue = productInWeb.replace("Satisfactory", hyphenSymbol);
                        LOGGER.info("replaceValue Satisfactory: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed Satisfactory: " + productInWeb);
                        } else if (productInWeb.contains("Flat rate")) {
                        String replaceValue = productInWeb.replace("Flat rate", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed : " + productInWeb);
                        } else if (productInWeb.contains("USD")) {
                        String replaceValue = productInWeb.replace("USD", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[1];
                        LOGGER.info("productInWeb trimmed USD: " + productInWeb);
                        } else if (productInWeb.contains("Deposit Amount")) {
                        String replaceValue = productInWeb.replace("Deposit Amount", hyphenSymbol);
                        LOGGER.info("replaceValue: " + replaceValue);
                        productInWeb = replaceValue.split(hyphenSymbol)[0];
                        LOGGER.info("productInWeb trimmed Deposit Amount: " + productInWeb);
                    }
                    LOGGER.info("productInWeb checker: " + productInWeb);
                    LOGGER.info("productInApi checker: " + productInApi);
                    if (channelName.contains("auto-loans")) {
                        if (productValidationMethodNameKey.equals("Interest")) {
                            if (isPremiumProductAvailable()) {
                                productInApi = calculateAndGetAutoLogicOne(true, productInApi, CountryType, 0);
                                } else {
                                productInApi = calculateAndGetAutoLogicOne(false, productInApi, CountryType,
                                productCount);
                            }
                            } else if (productValidationMethodNameKey.equals("Down Payment")) {
                            if (isPremiumProductAvailable()) {
                                productInApi = calculateAndGetAutoLogicTwo(true, productInApi, CountryType, 0);
                                } else {
                                productInApi = calculateAndGetAutoLogicTwo(false, productInApi, CountryType,
                                productCount);
                            }
                            } else if (productValidationMethodNameKey.equals("Installment")) {
                            if (isPremiumProductAvailable()) {
                                productInApi = calculateAndGetAutoLogicThree(true, productInApi, CountryType, 0);
                                } else {
                                productInApi = calculateAndGetAutoLogicThree(false, productInApi, CountryType,
                                productCount);
                            }
                            } else if (productValidationMethodNameKey.equals("First Payment")) {
                            if (isPremiumProductAvailable()) {
                                productInApi = calculateAndGetAutoLogicFour(true, productInApi, CountryType, 0);
                                } else {
                                productInApi = calculateAndGetAutoLogicFour(false, productInApi, CountryType,
                                productCount);
                            }
                        }
                    }
                    LOGGER.info("Product Validation   %%%%%%% productInApi:" + productInApi);
                    LOGGER.info("Product Validation   %%%%%%% productInWeb:" + productInWeb);
                    if (productInWeb.toString().trim().equalsIgnoreCase(productInApi.toString().trim())) {
                        LOGGER.info("Product Validation : Passed");
                        SuccessActualResult = "There are " + calculationMethodTestRunCount
                        + " calculation records in the excel. So this method excuted "
                        + calculationMethodTestRunCount + "times" + "n";
                        } else {
                        isAllProductNameValidatedAndSucess = false;
                        LOGGER.info("Product Validation : Failed");
                        LOGGER.info("productInWeb : " + productInWeb);
                        LOGGER.info("productInApi : " + productInApi);
                        productInWebAndAPI = productInWeb + ", " + productInApi;
                        LOGGER.info("Product Validation  else case:" + productInWebAndAPI);
                    }
                    /*
                    * Checking Area
                    */
                    if (isAllProductNameValidatedAndSucess) {
                        isAllProductNameValidatedAndSucessElse = true;
                        } else {
                        isAllProductNameValidatedAndSucessElse = false;
                        LOGGER.info("Product Validation failed" + FailureActualResult);
                    }
                }
            }
            if (isAllProductNameValidatedAndSucessIf && isAllProductNameValidatedAndSucessElse) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, SuccessActualResult);
                } else {
                LOGGER.info("Product Validation failed" + FailureActualResult);
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureActualResult);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureActualResult);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, FailureActualResult);
                Assert.fail(FailureActualResult);
            }
            } catch (Exception e) {
            // TODO Auto-generated catch block
            productValidationMethodNameKey = "DoProductValidation";
            resultMap.put(TEST_RESULT.R_METHOD_NAME, testMethodNameReport);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, "Y");
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, productValidationExpectedResultAll);
            resultMap.put(TEST_RESULT.R_DESRIPTION, productValidationDescriptionAll);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, productValidationActualResultAll);
            testContext.setAttribute(testMethodName, resultMap);
            LOGGER.info("Failure method name = " + resultMap.get(TEST_RESULT.R_METHOD_NAME));
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
            } finally {
            int currentValidationCount = productValidationMethodInvocationCount - 1;
            ExcelInputData excelInputData = ExcelInputData.getInstance();
            ArrayList<HashMap<String, ExcelProductValidationInputData>> excelInputProductValidatinArray = excelInputData
            .getExcelProductValidationInputData();
            int totalValidationCount = excelInputProductValidatinArray.size() - 1;
            LOGGER.info("currentValidationCount = " + currentValidationCount);
            LOGGER.info("totalValidationCount = " + totalValidationCount);
            if (currentValidationCount == totalValidationCount) {
                resultMap.put(TEST_RESULT.R_METHOD_NAME, testMethodNameReport);
                resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, productValidationExpectedResultAll);
                resultMap.put(TEST_RESULT.R_DESRIPTION, productValidationDescriptionAll);
                resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, "Y");
                testContext.setAttribute(testMethodName, resultMap);
                try {
                    prepareDriver();
                    Thread.sleep(5000); // 24-08-2016
                    } catch (Exception e) {
                    // TODO Auto-generated catch block
                    LOGGER.log(Level.SEVERE, "EXCEPTION", e);
                }
            }
        }
    }
    private List<String> setProductNameInCollectionAndRemoveDuplicate(List<WebElement> contentList, String staticElement) {
        // TODO Auto-generated method stub
        return null;
    }
    public List<String> getListOfProductValueFromWebForParticularTypeInArray(List<WebElement> contentList,
    String webElementLocator, String webElement) throws Exception {
        List<String> webProdArray = new ArrayList<String>();
        for (WebElement content : contentList) {
            GetWebElement webElementObject = new GetWebElement();
            WebElement productWebElement = webElementObject.getWebElemntFromParentElemntType(content,
            webElementLocator, webElement);
            String productInterestRateWeb = productWebElement.getText();
            webProdArray.add(productInterestRateWeb);
            LOGGER.info("productInterestRateWeb  = " + productInterestRateWeb);
        }
        return webProdArray;
    }
    public boolean isPremiumProductAvailable() throws Exception {
        if (getAPIResult().has("premium")) {
            Object aObj = getAPIResult().get("premium");
            if (aObj instanceof JSONObject) {
                return true;
            }
        }
        return false;
    }
    public String replaceCurrencyIfAny(String finalProductValue) {
        String replacedCurrencyVal = finalProductValue;
        if (finalProductValue.contains("Rp. ")) {
            replacedCurrencyVal = finalProductValue.replace("Rp. ", "").trim().toString();
        }
        if (finalProductValue.contains("IDR ")) {
            replacedCurrencyVal = finalProductValue.replace("IDR ", "").trim().toString();
        }
        if (finalProductValue.contains("$ ")) {
            replacedCurrencyVal = finalProductValue.replace("$ ", "").trim().toString();
        }
        return replacedCurrencyVal;
    }
    public String calculateAndGetAutoLogicOne(boolean isPremium, String finalProductValue, String countryType,
    int apiProdCount) throws Exception {
        String calculatedValue = "";
        String nodeValueOne = "product.calculation_result.apr";
        String nodeValueTwo = "product.calculation_result.parameters.interest_rate";
        if (isPremium) {
            if (finalProductValue.contains("flat_rate")) {
                calculatedValue = getAnotherAPIValueForSponsor(getAPIResult(), nodeValueOne, dotSymbol, countryType);
                } else {
                calculatedValue = getAnotherAPIValueForSponsor(getAPIResult(), nodeValueTwo, dotSymbol, countryType);
            }
            System.out.print("logic one : premium = " + calculatedValue);
            } else {
            if (finalProductValue.contains("flat_rate")) {
                calculatedValue = getAnotherAPIValue(getAPIResult(), nodeValueOne, dotSymbol, countryType, apiProdCount);
                } else {
                calculatedValue = getAnotherAPIValue(getAPIResult(), nodeValueTwo, dotSymbol, countryType, apiProdCount);
            }
            System.out.print("logic one : not premium = " + calculatedValue);
        }
        System.out.print("Final logic one : calculatedValue = " + calculatedValue);
        return calculatedValue;
    }
    public String calculateAndGetAutoLogicTwo(boolean isPremium, String finalProductValue, String countryType,
    int apiProdCount) throws Exception {
        System.out.print("Final logic two : finalProductValue = " + finalProductValue);
        String calculatedValue = "";
        String nodeValueOne = "product.calculation_result.params.loan_amount";
        if (isPremium) {
            String getLoanAmntValueFromPremium = getAnotherAPIValueForSponsor(getAPIResult(), nodeValueOne, dotSymbol,
            countryType);
            System.out.print("logic two : premium = " + getLoanAmntValueFromPremium);
            String replacedCurrencyValue = replaceCurrencyIfAny(finalProductValue);
            int calValue = Integer.parseInt(replacedCurrencyValue) - Integer.parseInt(getLoanAmntValueFromPremium);
            calculatedValue = String.valueOf(calValue);
            } else {
            String getLoanAmntValue = getAnotherAPIValue(getAPIResult(), nodeValueOne, dotSymbol, countryType,
            apiProdCount);
            System.out.print("logic two : not premium = " + getLoanAmntValue);
            String replacedCurrencyValue = replaceCurrencyIfAny(finalProductValue);
            System.out.print("replaceCurrencyIfAny = " + replaceCurrencyIfAny(finalProductValue));
            System.out.print("replacedCurrencyValue = " + replacedCurrencyValue);
            replacedCurrencyValue = replacedCurrencyValue.replaceAll("^\\s+", "");
            System.out.print("replacedCurrencyValue  after trim = " + replacedCurrencyValue);
            int calValue = Integer.parseInt(replacedCurrencyValue) - Integer.parseInt(getLoanAmntValue);
            System.out.print("calValuecalValue = " + calValue);
            calculatedValue = String.valueOf(calValue);
            System.out.print("Final logic two : calculatedVa = " + calculatedValue);
        }
        System.out.print("Final logic two : calculatedValue = " + calculatedValue);
        return calculatedValue;
    }
    public String calculateAndGetAutoLogicThree(boolean isPremium, String finalProductValue, String countryType,
    int apiProdCount) throws Exception {
        String calculatedValue = "";
        String nodeValueOne = "product.calculation_result.installment";
        String nodeValueTwo = "product.calculation_result.average_monthly_instalment";
        if (isPremium) {
            if (finalProductValue.contains("flat_rate")) {
                calculatedValue = getAnotherAPIValueForSponsor(getAPIResult(), nodeValueOne, dotSymbol, countryType);
                System.out.print("logic three :  premium flat_rate= " + calculatedValue);
                } else {
                calculatedValue = getAnotherAPIValueForSponsor(getAPIResult(), nodeValueTwo, dotSymbol, countryType);
                System.out.print("logic three :  premium not flat_rate= " + calculatedValue);
            }
            } else {
            if (finalProductValue.contains("flat_rate")) {
                calculatedValue = getAnotherAPIValue(getAPIResult(), nodeValueOne, dotSymbol, countryType, apiProdCount);
                System.out.print("logic three : not premium flat_rate= " + calculatedValue);
                } else {
                calculatedValue = getAnotherAPIValue(getAPIResult(), nodeValueTwo, dotSymbol, countryType, apiProdCount);
                System.out.print("logic three : not premium not flat_rate= " + calculatedValue);
            }
        }
        System.out.print("Final logic three : calculatedValue = " + calculatedValue);
        return calculatedValue;
    }
    public String calculateAndGetAutoLogicFour(boolean isPremium, String finalProductValue, String countryType,
    int apiProdCount) throws Exception {
        String calculatedValue = "";
        if (isPremium) {
            if (finalProductValue.contains("flat_rate")) {
                String purchase_price = getAnotherAPIValueForSponsor(getAPIResult(),
                "product.calculation_result.params.purchase_price", dotSymbol, countryType);
                String loan_amount = getAnotherAPIValueForSponsor(getAPIResult(),
                "product.calculation_result.params.loan_amount", dotSymbol, countryType);
                String monthly_payment = getAnotherAPIValueForSponsor(getAPIResult(),
                "product.calculation_result.monthly_payment", dotSymbol, countryType);
                System.out.print("logic four : premium flat_rate purchase_price= " + purchase_price);
                System.out.print("logic four : premium flat_rate loan_amount= " + loan_amount);
                System.out.print("logic four : premium flat_rate monthly_payment= " + monthly_payment);
                int finalValueCalculated = Integer.parseInt(purchase_price) - Integer.parseInt(loan_amount)
                + Integer.parseInt(monthly_payment);
                calculatedValue = String.valueOf(finalValueCalculated);
                System.out.print("logic four :  premium flat_rate= " + calculatedValue);
                } else {
                String purchase_price = getAnotherAPIValueForSponsor(getAPIResult(),
                "product.calculation_result.params.purchase_price", dotSymbol, countryType);
                String loan_amount = getAnotherAPIValueForSponsor(getAPIResult(),
                " product.calculation_result.loan_amount", dotSymbol, countryType);
                String monthly_payment = getAnotherAPIValueForSponsor(getAPIResult(),
                "product.calculation_result.average_monthly_instalment", dotSymbol, countryType);
                System.out.print("logic four : premium not flat_rate purchase_price= " + purchase_price);
                System.out.print("logic four : premium not flat_rate loan_amount= " + loan_amount);
                System.out.print("logic four : premium not flat_rate monthly_payment= " + monthly_payment);
                int finalValueCalculated = Integer.parseInt(purchase_price) - Integer.parseInt(loan_amount)
                + Integer.parseInt(monthly_payment);
                calculatedValue = String.valueOf(finalValueCalculated);
                System.out.print("logic four :  premium not flat_rate= " + calculatedValue);
            }
            } else {
            if (finalProductValue.contains("flat_rate")) {
                String purchase_price = getAnotherAPIValue(getAPIResult(),
                "product.calculation_result.params.purchase_price", dotSymbol, countryType, apiProdCount);
                String loan_amount = getAnotherAPIValue(getAPIResult(),
                "product.calculation_result.params.loan_amount", dotSymbol, countryType, apiProdCount);
                String monthly_payment = getAnotherAPIValue(getAPIResult(),
                "product.calculation_result.monthly_payment", dotSymbol, countryType, apiProdCount);
                System.out.print("logic four : not premium flat_rate purchase_price= " + purchase_price);
                System.out.print("logic four : not premium flat_rate loan_amount= " + loan_amount);
                System.out.print("logic four : not premium flat_rate monthly_payment= " + monthly_payment);
                int finalValueCalculated = Integer.parseInt(purchase_price) - Integer.parseInt(loan_amount)
                + Integer.parseInt(monthly_payment);
                calculatedValue = String.valueOf(finalValueCalculated);
                System.out.print("logic four : not premium flat_rate= " + calculatedValue);
                } else {
                String purchase_price = getAnotherAPIValue(getAPIResult(),
                "product.calculation_result.params.purchase_price", dotSymbol, countryType, apiProdCount);
                String loan_amount = getAnotherAPIValue(getAPIResult(), " product.calculation_result.loan_amount",
                dotSymbol, countryType, apiProdCount);
                String monthly_payment = getAnotherAPIValue(getAPIResult(),
                "product.calculation_result.average_monthly_instalment", dotSymbol, countryType, apiProdCount);
                System.out.print("logic four : not premium not flat_rate purchase_price= " + purchase_price);
                System.out.print("logic four : not premium not flat_rate loan_amount= " + loan_amount);
                System.out.print("logic four : not premium not flat_rate monthly_payment= " + monthly_payment);
                int finalValueCalculated = Integer.parseInt(purchase_price) - Integer.parseInt(loan_amount)
                + Integer.parseInt(monthly_payment);
                calculatedValue = String.valueOf(finalValueCalculated);
                System.out.print("logic four : not premium not flat_rate= " + calculatedValue);
            }
        }
        System.out.print("Final logic four : calculatedValue = " + calculatedValue);
        return calculatedValue;
    }
    public List<String> getListOfProductValueFromAPIForParticularTypeInArray(JSONObject jsonResult,
    String productTypeAPINode, boolean isAnotherApiNode, String productTypeAnotherAPINode,
    String splitApiNodeWithASymbol, String CountryType, boolean isBanknameValidation, boolean isNeedTrim,
    String splitBankName, boolean isConcat, String concatType, String concatValue) throws JSONException {
        String apiNodeValueLogicOne = "autoLoanCalculationLogicOne";
        String apiNodeValueLogicTwo = "autoLoanCalculationLogicTwo";
        String apiNodeValueLogicThree = "autoLoanCalculationLogicThree";
        String apiNodeValueLogicFour = "autoLoanCalculationLogicFour";
        if (productTypeAPINode.contains(apiNodeValueLogicOne) || productTypeAPINode.contains(apiNodeValueLogicThree)
        || productTypeAPINode.contains(apiNodeValueLogicFour)) {
            productTypeAPINode = "product.specifications.calculation_type";
            } else if (productTypeAPINode.contains(apiNodeValueLogicTwo)) {
            productTypeAPINode = "product.calculation_result.params.purchase_price";
        }
        LOGGER.info("productTypeAPINode IN LIST ARRAY = " + productTypeAPINode);
        String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
        List<String> apiProducts = new ArrayList<String>();
        if (jsonResult.has("premium")) {
            LOGGER.info("specifications available ");
            Object aObj = jsonResult.get("premium");
            if (aObj instanceof JSONObject) {
                System.out.println(aObj);
                JSONObject premiumParams = jsonResult.getJSONObject("premium");
                JSONObject sponsoredParams = premiumParams.getJSONObject("sponsored");
                String finalProductValue = "";
                JSONObject nextJsonObject = null;
                String PrefeatureNode = "";
                if (!isBanknameValidation) {
                    for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {
                        LOGGER.info("apiNodeCount =ok " + apiNodeCount);
                        if (apiNodeCount == apiNodes.length - 1) {
                            LOGGER.info("nextJsonObject =ok " + nextJsonObject);
                            LOGGER.info("PrefeatureNode =ok " + PrefeatureNode);
                            if (nextJsonObject != null) {
                                if (PrefeatureNode.length() > 0) {
                                    if (PrefeatureNode.equals("features") || PrefeatureNode.equals("requirements")) {
                                        JSONArray nextJsonArr = nextJsonObject.getJSONArray(PrefeatureNode);
                                        LOGGER.info("nextJsonArr = " + nextJsonArr);
                                        for (int featureCount = 0; featureCount < nextJsonArr.length(); featureCount++) {
                                            JSONObject featureJson = nextJsonArr.getJSONObject(featureCount);
                                            LOGGER.info("featureJson = " + featureJson);
                                            LOGGER.info("apiNodeFinalValue = " + featureJson.getString("code"));
                                            String codeVale = featureJson.getString("code");
                                            if (codeVale.equals("initial_deposit") || codeVale.equals("processing_fee")
                                            || codeVale.equals("location")) {
                                                String value = featureJson.getString("value");
                                                LOGGER.info("Value 123 = " + value);
                                                if (value.equals("null") || value.equals(null) || value == null) {
                                                    String strValue = featureJson.getString("value_str");
                                                    LOGGER.info("strValue 123 = " + strValue);
                                                    finalProductValue = strValue;
                                                    } else {
                                                    finalProductValue = value;
                                                }
                                            }
                                        }
                                        PrefeatureNode = "";
                                        } else {
                                        LOGGER.info("No features...");
                                    }
                                    } else {
                                    LOGGER.info("apiNodeFinalValue = "
                                    + nextJsonObject.getString(apiNodes[apiNodeCount]));
                                    finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
                                }
                            }
                            } else {
                            String apiSubNode = apiNodes[apiNodeCount];
                            LOGGER.info("apiSubNode = " + apiSubNode);
                            if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
                                PrefeatureNode = apiSubNode;
                            }
                            LOGGER.info("PrefeatureNode1231 = " + PrefeatureNode);
                            if (nextJsonObject == null) {
                                if (sponsoredParams.has(apiSubNode)) {
                                    Object subObject = sponsoredParams.get(apiSubNode);
                                    if (subObject instanceof JSONObject) {
                                        nextJsonObject = sponsoredParams.getJSONObject(apiSubNode);
                                        LOGGER.info("sponsoredParams = " + nextJsonObject.toString());
                                        } else if (subObject instanceof JSONArray) {
                                        JSONArray nextJsonArr = sponsoredParams.getJSONArray(apiSubNode);
                                        JSONObject newJson = nextJsonArr.getJSONObject(0);
                                        nextJsonObject = newJson;
                                    }
                                }
                                } else {
                                if (nextJsonObject.has(apiSubNode)) {
                                    Object subObject = nextJsonObject.get(apiSubNode);
                                    if (subObject instanceof JSONObject) {
                                        nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                                        LOGGER.info("nextJsonObject = " + nextJsonObject.toString());
                                        } else if (subObject instanceof JSONArray) {
                                        JSONObject newJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                                        JSONArray nextJsonArr = newJsonObject.getJSONArray(apiSubNode);
                                        JSONObject newJson = nextJsonArr.getJSONObject(0);
                                        nextJsonObject = newJson;
                                    }
                                }
                            }
                            if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
                                nextJsonObject = sponsoredParams;
                            }
                        }
                    }
                    String withAnotherApiValue = "";
                    if (isAnotherApiNode) {
                        withAnotherApiValue = getAnotherAPIValueForSponsor(jsonResult, productTypeAnotherAPINode,
                        splitApiNodeWithASymbol, CountryType);
                    }
                    finalProductValue = concatIfAnyStringsNeedToConcatWithAPIResult(productTypeAPINode, CountryType,
                    finalProductValue, withAnotherApiValue);
                    } else {
                    finalProductValue = sponsoredParams.getString(apiNodes[apiNodes.length - 1]);
                    if (isNeedTrim) {
                        if (finalProductValue.contains(splitBankName)) {
                            finalProductValue = finalProductValue.split(splitBankName)[0].trim();
                            LOGGER.info("finalProductValue.split(splitBankName)[0].trim()"
                            + finalProductValue.split(splitBankName)[0].trim());
                        }
                    }
                    if (isConcat) {
                        if (concatType.equals("suffix")) {
                            finalProductValue = finalProductValue + " " + concatValue;
                            } else {
                            finalProductValue = concatValue + " " + finalProductValue;
                        }
                    }
                }
                apiProducts.add(finalProductValue);
                } else {
                LOGGER.info("Empty premium value...");
            }
        }
        LOGGER.info("apiNodes[0] = " + apiNodes[0]);
        JSONArray productArray = jsonResult.getJSONArray(apiNodes[0]);
        for (int apiProdCount = 0; apiProdCount < productArray.length(); apiProdCount++) {
            JSONObject productData = productArray.getJSONObject(apiProdCount);
            String finalProductValue = "";
            JSONObject nextJsonObject = null;
            String PrefeatureNode = "";
            if (!isBanknameValidation) {
                for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {
                    if (apiNodeCount == apiNodes.length - 1) {
                        if (nextJsonObject != null) {
                            if (PrefeatureNode.length() > 0) {
                                if (PrefeatureNode.equals("features") || PrefeatureNode.equals("requirements")) {
                                    JSONArray nextJsonArr = nextJsonObject.getJSONArray(PrefeatureNode);
                                    LOGGER.info("nextJsonArr cccc = " + nextJsonArr);
                                    for (int featureCount = 0; featureCount < nextJsonArr.length(); featureCount++) {
                                        JSONObject featureJson = nextJsonArr.getJSONObject(featureCount);
                                        LOGGER.info("apiNodeFinalValue = " + featureJson.getString("code"));
                                        String codeVale = featureJson.getString("code");
                                        if (codeVale.equals("initial_deposit") || codeVale.equals("processing_fee")
                                        || codeVale.equals("location")) {
                                            String value = featureJson.getString("value");
                                            LOGGER.info("Value 123 = " + value);
                                            if (value.equals("null") || value.equals(null) || value == null) {
                                                String strValue = featureJson.getString("value_str");
                                                LOGGER.info("strValue 123 = " + strValue);
                                                finalProductValue = strValue;
                                                } else {
                                                finalProductValue = value;
                                            }
                                        }
                                    }
                                    PrefeatureNode = "";
                                    } else {
                                    LOGGER.info("No features...");
                                }
                                } else {
                                LOGGER.info("apiNodeFinalValue = " + nextJsonObject.getString(apiNodes[apiNodeCount]));
                                finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
                            }
                        }
                        } else {
                        String apiSubNode = apiNodes[apiNodeCount];
                        LOGGER.info("apiSubNode = " + apiSubNode);
                        if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
                            PrefeatureNode = apiSubNode;
                        }
                        if (nextJsonObject == null) {
                            if (productData.has(apiSubNode)) {
                                Object subObject = productData.get(apiSubNode);
                                if (subObject instanceof JSONObject) {
                                    nextJsonObject = productData.getJSONObject(apiSubNode);
                                    LOGGER.info("not sponsor  = " + nextJsonObject.toString());
                                    } else if (subObject instanceof JSONArray) {
                                    JSONArray nextJsonArr = productData.getJSONArray(apiSubNode);
                                    JSONObject newJson = nextJsonArr.getJSONObject(0);
                                    nextJsonObject = newJson;
                                }
                            }
                            } else {
                            if (nextJsonObject.has(apiSubNode)) {
                                Object subObject = nextJsonObject.get(apiSubNode);
                                if (subObject instanceof JSONObject) {
                                    nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                                    LOGGER.info("not sponsor nextJsonObject = " + nextJsonObject.toString());
                                    } else if (subObject instanceof JSONArray) {
                                    JSONObject newJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                                    JSONArray nextJsonArr = newJsonObject.getJSONArray(apiSubNode);
                                    JSONObject newJson = nextJsonArr.getJSONObject(0);
                                    nextJsonObject = newJson;
                                }
                            }
                        }
                        if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
                            nextJsonObject = productData;
                        }
                    }
                }
                String withAnotherApiValue = "";
                if (isAnotherApiNode) {
                    withAnotherApiValue = getAnotherAPIValue(jsonResult, productTypeAnotherAPINode,
                    splitApiNodeWithASymbol, CountryType, apiProdCount);
                }
                LOGGER.info("isAnotherApiNode123 " + isAnotherApiNode);
                if (withAnotherApiValue == null) {
                    LOGGER.info("withAnotherApiValue123 is null");
                    withAnotherApiValue = "";
                    } else {
                    LOGGER.info("withAnotherApiValue123 " + withAnotherApiValue);
                }
                LOGGER.info("isAnotherApiNode123 " + isAnotherApiNode);
                LOGGER.info("withAnotherApiValue123 " + withAnotherApiValue);
                finalProductValue = concatIfAnyStringsNeedToConcatWithAPIResult(productTypeAPINode, CountryType,
                finalProductValue, withAnotherApiValue);
                } else {
                finalProductValue = productData.getString(apiNodes[apiNodes.length - 1]);
                if (isNeedTrim) {
                    if (finalProductValue.contains(splitBankName)) {
                        finalProductValue = finalProductValue.split(splitBankName)[0].trim();
                        LOGGER.info("finalProductValue.split(splitBankName)[0].trimdwdw()"
                        + finalProductValue.split(splitBankName)[0].trim());
                    }
                }
                if (isConcat) {
                    if (concatType.equals("suffix")) {
                        finalProductValue = finalProductValue + " " + concatValue;
                        } else {
                        finalProductValue = concatValue + " " + finalProductValue;
                    }
                }
            }
            LOGGER.info("finalProductValue " + finalProductValue);
            apiProducts.add(finalProductValue);
        }
        return apiProducts;
    }
    public String getAnotherAPIValueForSponsor(JSONObject jsonResult, String productTypeAPINode,
    String splitApiNodeWithASymbol, String CountryType) throws JSONException {
        LOGGER.info("productTypeAPINode = " + productTypeAPINode);
        String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
        LOGGER.info("LOGGER1" + apiNodes);
        String apiProductsAnotherValue = "";
        if (jsonResult.has("premium")) {
            LOGGER.info("specifications available ");
            Object aObj = jsonResult.get("premium");
            if (aObj instanceof JSONObject) {
                System.out.println(aObj);
                JSONObject premiumParams = jsonResult.getJSONObject("premium");
                JSONObject sponsoredParams = premiumParams.getJSONObject("sponsored");
                String finalProductValue = "";
                JSONObject nextJsonObject = null;
                for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {
                    if (apiNodeCount == apiNodes.length - 1) {
                        if (nextJsonObject != null) {
                            LOGGER.info("apiNodeFinalValue string = "
                            + nextJsonObject.getString(apiNodes[apiNodeCount]));
                            finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
                        }
                        } else {
                        String apiSubNode = apiNodes[apiNodeCount];
                        LOGGER.info("apiSubNode = " + apiSubNode);
                        if (nextJsonObject == null) {
                            if (sponsoredParams.has(apiSubNode)) {
                                Object subObject = sponsoredParams.getJSONObject(apiSubNode);
                                if (subObject instanceof JSONObject) {
                                    nextJsonObject = sponsoredParams.getJSONObject(apiSubNode);
                                    LOGGER.info("sponsoredParams = " + nextJsonObject.toString());
                                }
                            }
                            } else {
                            if (nextJsonObject.has(apiSubNode)) {
                                Object subObject = nextJsonObject.getJSONObject(apiSubNode);
                                if (subObject instanceof JSONObject) {
                                    nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                                    LOGGER.info("nextJsonObject = " + nextJsonObject.toString());
                                }
                            }
                        }
                    }
                }
                apiProductsAnotherValue = finalProductValue;
                } else {
                LOGGER.info("Empty premium value...");
            }
        }
        return apiProductsAnotherValue;
    }
    public String getAnotherAPIValue(JSONObject jsonResult, String productTypeAPINode, String splitApiNodeWithASymbol,
    String CountryType, int particularProductCount) throws JSONException {
        LOGGER.info("productTypeAPINode = " + productTypeAPINode);
        String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
        LOGGER.info("LOGGER1" + apiNodes);
        String apiProductsAnotherValue = "";
        LOGGER.info("apiNodes[0] = " + apiNodes[0]);
        JSONArray productArray = jsonResult.getJSONArray(apiNodes[0]);
        JSONObject productData = productArray.getJSONObject(particularProductCount);
        String finalProductValue = "";
        JSONObject nextJsonObject = null;
        for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {
            if (apiNodeCount == apiNodes.length - 1) {
                if (nextJsonObject != null) {
                    LOGGER.info("apiNodeFinalValue  anotherapi = " + nextJsonObject.getString(apiNodes[apiNodeCount]));
                    finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
                    } else {
                    if (productData.has(apiNodes[apiNodeCount])) {
                        Object subObject = productData.getJSONObject(apiNodes[apiNodeCount]);
                        if (subObject instanceof JSONObject) {
                            nextJsonObject = productData.getJSONObject(apiNodes[apiNodeCount]);
                            LOGGER.info("productData = " + nextJsonObject.toString());
                            LOGGER.info(" straight node finalProductValue = " + finalProductValue);
                            } else if (subObject instanceof String) {
                            finalProductValue = productData.getString(apiNodes[apiNodeCount]);
                            LOGGER.info(" straight node finalProductValue = " + finalProductValue);
                        }
                    }
                }
                } else {
                String apiSubNode = apiNodes[apiNodeCount];
                LOGGER.info("apiSubNode = " + apiSubNode);
                if (nextJsonObject == null) {
                    if (productData.has(apiSubNode)) {
                        Object subObject = productData.getJSONObject(apiSubNode);
                        if (subObject instanceof JSONObject) {
                            nextJsonObject = productData.getJSONObject(apiSubNode);
                            LOGGER.info("productData = " + nextJsonObject.toString());
                        }
                    }
                    } else {
                    if (nextJsonObject.has(apiSubNode)) {
                        Object subObject = nextJsonObject.getJSONObject(apiSubNode);
                        if (subObject instanceof JSONObject) {
                            nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
                            LOGGER.info("nextJsonObject = " + nextJsonObject.toString());
                        }
                    }
                }
            }
        }
        apiProductsAnotherValue = finalProductValue;
        return apiProductsAnotherValue;
    }
    public HashMap<String, String> getAPINodeValueToParse(String concatValueWithOtherRequiredValue, String countryType) {
        HashMap<String, String> finalApiNodeValueToParse = new HashMap<String, String>();
        String finalString = "";
        String isApiNeedToConcat = "N";
        String apiNodeTobeConcat = "";
        LOGGER.info("concatValueWithOtherRequiredValue = " + concatValueWithOtherRequiredValue);
        HashMap<String, HashMap<String, String>> getValuesFromYAML = concatIfAnyStringsNeedToConcatInYAML(
        concatValueWithOtherRequiredValue, countryType);
        LOGGER.info("getValuesFromYAML = " + getValuesFromYAML);
        if (getValuesFromYAML.containsKey(("percentage"))) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("percentage");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("currency")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("currency");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("join_two_api_value")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("join_two_api_value");
            LOGGER.info("singleTypeValue = " + singleTypeValue);
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
                isApiNeedToConcat = "Y";
                apiNodeTobeConcat = singleTypeValue.get("apiConcatNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("currency_with_two_api_value")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("currency_with_two_api_value");
            LOGGER.info("singleTypeValue = " + singleTypeValue);
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("normal")) {
            // normal mode
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("normal");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("renderBenefits")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("renderBenefits");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
        }
        else if (getValuesFromYAML.containsKey("trans")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("trans");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("productRating")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("productRating");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
            } else if (getValuesFromYAML.containsKey("location")) {
            HashMap<String, String> singleTypeValue = getValuesFromYAML.get("location");
            if (singleTypeValue.get("apiNodeValue") != null) {
                finalString = singleTypeValue.get("apiNodeValue");
            }
        }
        if (apiNodeTobeConcat.equals("product.calculation_result.params.loan_tenure_units")) {
            apiNodeTobeConcat = apiNodeTobeConcat.replace("product.calculation_result.params.loan_tenure_units",
            "product.calculation_result.params.loan_tenure_unit");
            LOGGER.info("NODE_VALUE = " + apiNodeTobeConcat);
        }
        LOGGER.info("finalString = " + finalString);
        LOGGER.info("isAnotherAPIConcat =" + isApiNeedToConcat);
        LOGGER.info("apiNodeTobeConcat = " + apiNodeTobeConcat);
        finalApiNodeValueToParse.put("apiNodeValue", finalString);
        finalApiNodeValueToParse.put("isAnotherAPIConcat", isApiNeedToConcat);
        finalApiNodeValueToParse.put("apiValueTobeConcat", apiNodeTobeConcat);
        return finalApiNodeValueToParse;
    }
    public String concatIfAnyStringsNeedToConcatWithAPIResult(String concatValueWithOtherRequiredValue,
    String countryType, String valueShouldBeConcatenated, String withValue) {
        String finalString = "";
        if (APINodeConcatLogicType.contains(("percentage"))) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("percentage");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.equals("currency")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("currency");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.contains("join_two_api_value")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("join_two_api_value");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.contains("currency_with_two_api_value")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("currency_with_two_api_value");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.contains("normal")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("normal");
            // normal mode
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
        }
        // Because no Concat Or Trim Logic used
        else if (APINodeConcatLogicType.contains("renderBenefits")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("renderBenefits");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.contains("trans")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("trans");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
        }
        // Because no Rating used
        else if (APINodeConcatLogicType.contains("productRating")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("productRating");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
            } else if (APINodeConcatLogicType.contains("location")) {
            HashMap<String, String> singleTypeValue = apiNoeTypeAndMapValue.get("location");
            finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
        }
        LOGGER.info("concatIfAnyStringsNeedToConcatWithAPIResult = " + finalString);
        return finalString;
    }
    public String concatLogic(HashMap<String, String> singleTypeValue, String valueShouldBeConcatenated,
    String withAnotherAPiValue) {
        LOGGER.info("valueShouldBeConcatenated = " + valueShouldBeConcatenated);
        LOGGER.info("withAnotherAPiValue = " + withAnotherAPiValue);
        LOGGER.info("singleTypeValue = " + singleTypeValue);
        String finalString = valueShouldBeConcatenated;
        String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        if (!channelName.contains("Savings Account")) {
            if (singleTypeValue.containsKey(("isConcat"))) {
                String isConcat = singleTypeValue.get("isConcat");
                LOGGER.info("isConcat = " + isConcat);
                if (isConcat.equals("Y")) {
                    String concatType = singleTypeValue.get("concatType");
                    String concatValue = singleTypeValue.get("concatValue");
                    String needSpace = singleTypeValue.get("needSpace");
                    String isApiValueNeedToConcat = singleTypeValue.get("isApiValueNeedToConcat");
                    LOGGER.info("isApiValueNeedToConcat = " + withAnotherAPiValue);
                    if (isApiValueNeedToConcat.equals("Y")) {
                        String apiConcatNodeValue = withAnotherAPiValue;
                        if (concatType.equals("prefix")) {
                            if (needSpace.equals("Y")) {
                                finalString = apiConcatNodeValue + valueShouldBeConcatenated;
                                } else {
                                finalString = apiConcatNodeValue + " " + valueShouldBeConcatenated;
                            }
                            } else {
                            if (needSpace.equals("Y")) {
                                finalString = valueShouldBeConcatenated + " " + apiConcatNodeValue;
                                } else {
                                finalString = valueShouldBeConcatenated + apiConcatNodeValue;
                            }
                        }
                        } else {
                        if (concatType.equals("prefix")) {
                            if (needSpace.equals("Y")) {
                                finalString = concatValue + " " + valueShouldBeConcatenated;
                                } else {
                                finalString = concatValue + valueShouldBeConcatenated;
                            }
                            } else {
                            if (needSpace.equals("Y")) {
                                finalString = valueShouldBeConcatenated + " " + concatValue;
                                } else {
                                finalString = valueShouldBeConcatenated + concatValue;
                            }
                        }
                    }
                }
            }
        }
        LOGGER.info("concatLogic = " + finalString);
        return finalString;
    }
    public HashMap<String, HashMap<String, String>> concatIfAnyStringsNeedToConcatInYAML(
    String concatValueWithOtherRequiredValue, String countryType) {
        apiNoeTypeAndMapValue = new HashMap<String, HashMap<String, String>>();
        String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        if (channelName.contains("auto-loans")) {
            String autoLoanLogicOne = "{% if product.specifications.calculation_type =='flat_rate' %} {{ product.calculation_result.apr }} {% else %} {{ product.calculation_result.average_interest_rate }} {% endif %}%";
            String autoLoanLogicTwo = "{{ (product.calculation_result.params.purchase_price|replace({',': ''}) - product.calculation_result.params.loan_amount|replace({',': ''}) )|currency }}";
            String autoLoanLogicThree = "{% if product.specifications.calculation_type =='flat_rate' %} {{ product.calculation_result.installment|currency }} {% else %} {{ product.calculation_result.average_monthly_instalment|currency }} {% endif %}";
            String autoLoanLogicFour = "{% if product.specifications.calculation_type =='flat_rate' %} {{ ((product.calculation_result.params.purchase_price|replace({',': ''}) - product.calculation_result.params.loan_amount|replace({',': ''}) ) + product.calculation_result.monthly_payment)|currency }} {% else %} {{ ((product.calculation_result.params.purchase_price|replace({',': ''}) - product.calculation_result.loan_amount|replace({',': ''}) ) + product.calculation_result.average_monthly_instalment)|currency }} {% endif %}";
            if (concatValueWithOtherRequiredValue.contains(autoLoanLogicOne)) {
                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                String apiNodeValueLogic = "autoLoanCalculationLogicOne";
                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                apiNoeTypeAndNodeValue.put("concatType", "suffix");
                apiNoeTypeAndNodeValue.put("concatValue", "%");
                apiNoeTypeAndNodeValue.put("needSpace", "N");
                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeValueLogic);
                apiNoeTypeAndMapValue.put("percentage", apiNoeTypeAndNodeValue);
                APINodeConcatLogicType = "percentage";
                } else if (concatValueWithOtherRequiredValue.contains(autoLoanLogicTwo)) {
                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                String apiNodeValueLogic = "autoLoanCalculationLogicTwo";
                String currencyValue = "$";
                if (countryType.equals("id")) {
                    currencyValue = "Rp.";
                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                    } else {
                    apiNoeTypeAndNodeValue.put("needSpace", "N");
                }
                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeValueLogic);
                apiNoeTypeAndMapValue.put("currency", apiNoeTypeAndNodeValue);
                APINodeConcatLogicType = "currency";
                } else if (concatValueWithOtherRequiredValue.contains(autoLoanLogicThree)) {
                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                String apiNodeValueLogic = "autoLoanCalculationLogicThree";
                String currencyValue = "$";
                if (countryType.equals("id")) {
                    currencyValue = "Rp.";
                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                    } else {
                    apiNoeTypeAndNodeValue.put("needSpace", "N");
                }
                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeValueLogic);
                apiNoeTypeAndMapValue.put("currency", apiNoeTypeAndNodeValue);
                APINodeConcatLogicType = "currency";
                } else if (concatValueWithOtherRequiredValue.contains(autoLoanLogicFour)) {
                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                String apiNodeValueLogic = "autoLoanCalculationLogicFour";
                String currencyValue = "$";
                if (countryType.equals("id")) {
                    currencyValue = "Rp.";
                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                    } else {
                    apiNoeTypeAndNodeValue.put("needSpace", "N");
                }
                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeValueLogic);
                apiNoeTypeAndMapValue.put("currency", apiNoeTypeAndNodeValue);
                APINodeConcatLogicType = "currency";
            }
            } else {
            concatValueWithOtherRequiredValue = concatValueWithOtherRequiredValue.replace("{{", hyphenSymbol);
            concatValueWithOtherRequiredValue = concatValueWithOtherRequiredValue.replace("}}", hyphenSymbol);
            LOGGER.info("LLOOGGEERR_123 = " + concatValueWithOtherRequiredValue);
        String test = "%}-";
        LOGGER.info("12312323 = " + test);
    if (concatValueWithOtherRequiredValue.contains("%}-")
    && concatValueWithOtherRequiredValue.contains("|currency")
    && !concatValueWithOtherRequiredValue
.equals("{% if product.features.initial_deposit matches '/^[0-9]+$/' %}{{ product.features.initial_deposit|currency }}{% else %}{{product.features.initial_deposit}}{% endif %}")) {
                                String splitTheFirstColonRight = concatValueWithOtherRequiredValue.split(test)[1];
                                LOGGER.info("LLOOGGEERR_asdf = " + concatValueWithOtherRequiredValue);
                                String splittedCloseDoubleBracesDefault = "";
                                if (splitTheFirstColonRight.contains("|currency")) {
                                    String replacedCurrency = splitTheFirstColonRight.replace("|currency", hyphenSymbol);
                                    splittedCloseDoubleBracesDefault = replacedCurrency.split(hyphenSymbol)[0];
                                    LOGGER.info("LLOOGGEERR = " + splitTheFirstColonRight);
                                    HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                    String apiNodeOnly = "";
                                    apiNodeOnly = splittedCloseDoubleBracesDefault.trim().toString();
                                    LOGGER.info("CURRENCY = " + apiNodeOnly);
                                    LOGGER.info("splittedCloseDoubleBraces = " + apiNodeOnly);
                                    String currencyValue = "$";
                                    if (countryType.equals("id")) {
                                        currencyValue = "Rp.";
                                        apiNoeTypeAndNodeValue.put("needSpace", "Y");
                                        } else {
                                        apiNoeTypeAndNodeValue.put("needSpace", "N");
                                    }
                                    apiNoeTypeAndNodeValue.put("isConcat", "Y");
                                    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                    apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                    apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
                                    apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                    APINodeConcatLogicType = "currency_with_two_api_value";
                                    apiNoeTypeAndMapValue.put("currency_with_two_api_value", apiNoeTypeAndNodeValue);
                                }
                                } else if (concatValueWithOtherRequiredValue.contains("-%")) {
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                String apiNodeOnly = "";
                                String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
                                String splittedCloseDoubleBraces = splittedOpenDoubleBraces.split(hyphenSymbol)[0];
                                apiNodeOnly = splittedCloseDoubleBraces.trim().toString();
                                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "suffix");
                                apiNoeTypeAndNodeValue.put("concatValue", "%");
                                apiNoeTypeAndNodeValue.put("needSpace", "N");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                apiNoeTypeAndMapValue.put("percentage", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "percentage";
                                } else if (concatValueWithOtherRequiredValue.contains("|currency")) {
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                String apiNodeOnly = "";
                                String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
                                String replacedCurrency = splittedOpenDoubleBraces.replace("|currency", hyphenSymbol);
                                String splittedCloseDoubleBraces = replacedCurrency.split(hyphenSymbol)[0];
                                apiNodeOnly = splittedCloseDoubleBraces.trim().toString();
                                LOGGER.info("CURRENCY = " + apiNodeOnly);
                                LOGGER.info("splittedCloseDoubleBraces = " + splittedCloseDoubleBraces);
                                String currencyValue = "$";
                                if (countryType.equals("id")) {
                                    currencyValue = "Rp.";
                                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                                    } else {
                                    apiNoeTypeAndNodeValue.put("needSpace", "N");
                                }
                                apiNoeTypeAndNodeValue.put("isConcat", "Y");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
                                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                apiNoeTypeAndMapValue.put("currency", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "currency";
                                } else if (concatValueWithOtherRequiredValue.contains("|renderBenefits")) {
                                GetExcelInput getInput = new GetExcelInput();
                                String newLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits",
                                "Web Element Locator");
                                String newLocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits",
                                "Web Element");
                                String newAPINodeExcel = "product.benefits.description";
                                LOGGER.info("BENEFIT_APINodeExcel = " + newLocator);
                                LOGGER.info("BENEFIT_APINodeExcel = " + newLocatorName);
                                LOGGER.info("BENEFIT_APINodeExcel = " + newAPINodeExcel);
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                apiNoeTypeAndNodeValue.put("isConcat", "N");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                apiNoeTypeAndNodeValue.put("concatValue", "");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", newAPINodeExcel);
                                apiNoeTypeAndMapValue.put("renderBenefits", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "renderBenefits";
                            }
                            else if (concatValueWithOtherRequiredValue.contains("|trans")) {
                                GetExcelInput getInput = new GetExcelInput();
                                String newLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits",
                                "Web Element Locator");
                                String newLocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits",
                                "Web Element");
                                String newAPINodeExcel = "product.calculation_result.params.loan_tenure";
                                LOGGER.info("Minimum Tenure newLocator = " + newLocator);
                                LOGGER.info("Minimum Tenure newLocatorName= " + newLocatorName);
                                LOGGER.info("Minimum Tenure newAPINodeExcel = " + newAPINodeExcel);
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                apiNoeTypeAndNodeValue.put("isConcat", "N");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                apiNoeTypeAndNodeValue.put("concatValue", "");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", newAPINodeExcel);
                                apiNoeTypeAndMapValue.put("trans", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "trans";
                            }
                            else if (concatValueWithOtherRequiredValue.contains("|productRating")) {
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                String apiNodeOnly = "";
                                String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
                                String replacedproductRating = splittedOpenDoubleBraces.replace("|productRating", hyphenSymbol);
                                String splittedCloseDoubleBraces = replacedproductRating.split(hyphenSymbol)[0];
                                apiNodeOnly = splittedCloseDoubleBraces.trim().toString();
                                apiNoeTypeAndNodeValue.put("isConcat", "N");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                apiNoeTypeAndNodeValue.put("concatValue", "");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                apiNoeTypeAndMapValue.put("productRating", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "productRating";
                            }
                            else if (concatValueWithOtherRequiredValue.contains("product.requirements.code.location.value_str")) {
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                String apiNodeOnly = "product.requirements.code.location.value_str";
                                apiNoeTypeAndNodeValue.put("isConcat", "N");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "prefix");
                                apiNoeTypeAndNodeValue.put("concatValue", "");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                apiNoeTypeAndMapValue.put("location", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "location";
                            }
                            else if (concatValueWithOtherRequiredValue.contains("- -")) {
                                LOGGER.info("LLOOGGEERR_0000 = " + concatValueWithOtherRequiredValue);
                                if (concatValueWithOtherRequiredValue.contains("is defined ?")
                                && concatValueWithOtherRequiredValue.contains("|ucfirst")) {
                                    HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                    String splitTheFirstValue = concatValueWithOtherRequiredValue.split("- -")[0];
                                    String splitTheSecondValue = concatValueWithOtherRequiredValue.split("- -")[1];
                                    String firstDefinedValeReplaceVal = splitTheFirstValue.replace("is defined ?", hyphenSymbol);
                                    String firstDefinedValeTrimVal = firstDefinedValeReplaceVal.split(hyphenSymbol)[1];
                                    String secondDefinedValeVal = firstDefinedValeTrimVal.split(hyphenSymbol)[0];
                                    secondDefinedValeVal = secondDefinedValeVal.toString().trim();
                                    // second api val
                                    String replacedSecondNodeVal = splitTheSecondValue.replace("|ucfirst", hyphenSymbol);
                                    String secondSpleVal = replacedSecondNodeVal.split(hyphenSymbol)[0];
                                    secondSpleVal = secondSpleVal.toString().trim();
                                    apiNoeTypeAndNodeValue.put("isConcat", "Y");
                                    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "Y");
                                    apiNoeTypeAndNodeValue.put("concatType", "suffix");
                                    apiNoeTypeAndNodeValue.put("concatValue", "");
                                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                                    apiNoeTypeAndNodeValue.put("apiNodeValue", secondDefinedValeVal);
                                    apiNoeTypeAndNodeValue.put("apiConcatNodeValue", secondSpleVal);
                                    apiNoeTypeAndMapValue.put("join_two_api_value", apiNoeTypeAndNodeValue);
                                    APINodeConcatLogicType = "join_two_api_value";
                                    } else if (concatValueWithOtherRequiredValue.contains("|number_format")) {
                                    LOGGER.info("saving-split = " + concatValueWithOtherRequiredValue);
                                    HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                    String splitTheFirstValue = concatValueWithOtherRequiredValue.split("- -")[0];
                                    String splitTheSecondValue = concatValueWithOtherRequiredValue.split("- -")[1];
                                    LOGGER.info("saving-split-null = " + splitTheSecondValue);
                                    String splittedSecondCloseBraceValue = splitTheSecondValue.replace("|number_format", hyphenSymbol);
                                    LOGGER.info("saving-split-date= " + splittedSecondCloseBraceValue);
                                    String secondSpleVal = splittedSecondCloseBraceValue.split(hyphenSymbol)[0];
                                    secondSpleVal = secondSpleVal.toString().trim();
                                    LOGGER.info("saving-split-sp = " + secondSpleVal);
                                    apiNoeTypeAndNodeValue.put("isConcat", "N");
                                    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                    apiNoeTypeAndNodeValue.put("concatType", "suffix");
                                    apiNoeTypeAndNodeValue.put("concatValue", "%");
                                    apiNoeTypeAndNodeValue.put("needSpace", "N");
                                    apiNoeTypeAndNodeValue.put("apiNodeValue", secondSpleVal);
                                    apiNoeTypeAndMapValue.put("normal", apiNoeTypeAndNodeValue);
                                    APINodeConcatLogicType = "normal";
                                    } else {
                                    HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                    String splitTheFirstValue = concatValueWithOtherRequiredValue.split("- -")[0];
                                    String splitTheSecondValue = concatValueWithOtherRequiredValue.split("- -")[1];
                                    String splittedFirstOpenBraceValue = splitTheFirstValue.split(hyphenSymbol)[1];
                                    String splittedFirstVal = splittedFirstOpenBraceValue.trim().toString();
                                    String splittedSecondCloseBraceValue = splitTheSecondValue.split(hyphenSymbol)[0];
                                    String splittedSecondVal = splittedSecondCloseBraceValue.trim().toString();
                                    apiNoeTypeAndNodeValue.put("isConcat", "Y");
                                    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "Y");
                                    apiNoeTypeAndNodeValue.put("concatType", "suffix");
                                    apiNoeTypeAndNodeValue.put("concatValue", "");
                                    apiNoeTypeAndNodeValue.put("needSpace", "Y");
                                    apiNoeTypeAndNodeValue.put("apiNodeValue", splittedFirstVal);
                                    apiNoeTypeAndNodeValue.put("apiConcatNodeValue", splittedSecondVal);
                                    apiNoeTypeAndMapValue.put("join_two_api_value", apiNoeTypeAndNodeValue);
                                    APINodeConcatLogicType = "join_two_api_value";
                                }
                                } else {
                                HashMap<String, String> apiNoeTypeAndNodeValue = new HashMap<String, String>();
                                String apiNodeOnly = "";
                                String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
                                String splittedCloseDoubleBraces = splittedOpenDoubleBraces.split(hyphenSymbol)[0];
                                apiNodeOnly = splittedCloseDoubleBraces.trim().toString();
                                apiNoeTypeAndNodeValue.put("isConcat", "N");
                                apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
                                apiNoeTypeAndNodeValue.put("concatType", "suffix");
                                apiNoeTypeAndNodeValue.put("concatValue", "%");
                                apiNoeTypeAndNodeValue.put("needSpace", "N");
                                apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
                                apiNoeTypeAndMapValue.put("normal", apiNoeTypeAndNodeValue);
                                APINodeConcatLogicType = "normal";
                            }
                        }
                        return apiNoeTypeAndMapValue;
                    }
                    public double roundAValue(String roundDecimal, String roundValue) {
                        double finalRoundValue = 0.0;
                        DecimalFormat decimalFormat = null;
                        LOGGER.info("Value before round logic = " + roundValue);
                        if (roundDecimal.equals("1")) {
                            decimalFormat = new DecimalFormat("#.#");
                            } else if (roundDecimal.equals("2")) {
                            decimalFormat = new DecimalFormat("#.##");
                            } else if (roundDecimal.equals("3")) {
                            decimalFormat = new DecimalFormat("#.###");
                            } else if (roundDecimal.equals("4")) {
                            decimalFormat = new DecimalFormat("#.####");
                            } else if (roundDecimal.equals("5")) {
                            decimalFormat = new DecimalFormat("#.#####");
                            } else if (roundDecimal.equals("6")) {
                            decimalFormat = new DecimalFormat("#.######");
                        }
                        decimalFormat.setRoundingMode(RoundingMode.CEILING);
                        Double roundingValue = Double.parseDouble(roundValue);
                        String roundedValue = decimalFormat.format(roundingValue);
                        finalRoundValue = Double.parseDouble(roundedValue);
                        LOGGER.info("Value after round logic = " + roundedValue);
                        return finalRoundValue;
                    }
                    // * Custom methods
                    public String createWebserviceRequestUrl(HashMap<String, ArrayList<String>> filtersKeyValue, int apiInvocationCount) {
                        GetExcelInput getInput = new GetExcelInput();
                        String channelname = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(
                        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
                        String apiUrl = "";
                        try {
                            ExcelInputData excelinputobj = ExcelInputData.getInstance();
                            Object configobjYAML = excelinputobj.getYAMLConfigData();
                            Map configmap = (Map) configobjYAML;
                            LOGGER.info("configyaml" + configmap);
                            Object appobject = configmap.get("app");
                            Map appmap = (Map) appobject;
                            Object hostobject = appmap.get("hosts");
                            Map hostmap = (Map) hostobject;
                            String apivalues = (String) hostmap.get("api");
                            LOGGER.info("apivalues = " + apivalues);
                            Object productobjYAML = excelinputobj.getYAMLData();
                            Map productmap = (Map) productobjYAML;
                            Object tablefields = productmap.get("table_fields");
                            Map tablefieldsmap = (Map) tablefields;
                            Object sorting = tablefieldsmap.get("sorting_params");
                            Map sortingmap = (Map) sorting;
                            if (productmap.get("route") instanceof String)
                            channelname = (String) productmap.get("route");
                            else
                            channelname = (String) ((ArrayList) productmap.get("route")).get(0);
                            LOGGER.info("channelname = " + channelname);
                            Object filterval = productmap.get("filter");
                            Map filtervalmap = (Map) filterval;
                            Object defaultval = filtervalmap.get("default_values");
                            ExcelInputData excelInputData = ExcelInputData.getInstance();
                            String apichannels = "";
                            apichannels = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
                            "Api Channel Name");
                            Map defaultvalsmap = (Map) defaultval;
                            String str = "";
                            if (channelCountry.contains("sg")) {
                                LOGGER.info("If Con : 1");
                                str = ""
                                + apivalues
                                + "/api/v1/single-page-wizard-product/"
                                + apichannels
                                + "/summary?premium=1&calculate=1&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light";
                                } else if (apichannels.contains("auto-loans")) {
                                LOGGER.info("else If Con : 2");
                                if (pageUrl.contains("en_sg/auto-loans")) {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=en_sg&country=1&limit=15&masthead_variation=light";
                                    } else {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
                                }
                                } else if (apichannels.contains("personal-loans")) {
                                LOGGER.info("else If Con : 2");
                                if (pageUrl.contains("en_sg/unsecured-loans")) {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=en_sg&country=1&limit=15&masthead_variation=light";
                                    } else {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
                                }
                                } else if (apichannels.contains("multi-purpose-loan")) {
                                LOGGER.info("else If Con : 2");
                                if (pageUrl.contains("en_sg/multi-purpose-loan")) {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=en_sg&country=1&limit=15&masthead_variation=light";
                                    LOGGER.info("str_EN_SG" + str);
                                    } else {
                                    str = ""
                                    + apivalues
                                    + "/api/v1/single-page-wizard-product/"
                                    + apichannels
                                    + "/summary?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
                                    LOGGER.info("str_ID_ID" + str);
                                }
                                } else {
                                LOGGER.info(" else Con : 3");
                                str = ""
                                + apivalues
                                + "/api/v1/single-page-wizard-product/"
                                + apichannels
                                + "/summary?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
                            }
                            GetExcelInput getAMethod = new GetExcelInput();
                            HashMap<String, String> excelToFilterKey = new HashMap<>();
                            for (Object key : filtersKeyValue.keySet()) {
                                String keyval = (String) key;
                                String keyFromExcel = getAMethod.get_A_Value_Using_Key_Of_Filters_Method(keyval, "Filter Key In YAML");
                                excelToFilterKey.put(keyFromExcel, keyval);
                            }
                            int countSort = 0;
                            int countOrder = 0;
                            if (apichannels.contains("personal-loans") || apichannels.contains("fixed-deposit")
                            || apichannels.contains("renovation-loan") || apichannels.contains("education-loans")
                            || apichannels.contains("car-loan")) {
                                Object main = sortingmap.get("main");
                                List<Map> dashArray = (ArrayList<Map>) main;
                                for (Object KeyObject : dashArray) {
                                    for (Object Key : ((Map) KeyObject).keySet()) {
                                        LOGGER.info("SORT_KEY_first : " + (String) Key);
                                        String sortval = (String) Key;
                                        if (sortval.contains("sort")) {
                                            str += "&" + sortval + "[" + countSort + "]" + "=" + ((Map) KeyObject).get(sortval);
                                            countSort++;
                                            } else {
                                            str += "&" + sortval + "[" + countOrder + "]" + "=" + ((Map) KeyObject).get(sortval);
                                            countOrder++;
                                        }
                                        LOGGER.info("SORT_KEY : " + str);
                                    }
                                }
                                } else {
                                Object mandatoryObject = sortingmap.get("mandatory");
                                Map firstMandatoryMap = (Map) mandatoryObject;
                                Object sort = firstMandatoryMap.get("sort");
                                List<Map> sortArray = (ArrayList<Map>) sort;
                                for (int i = 0; i < sortArray.size(); i++) {
                                    str += "&" + "sort" + "[" + countSort + "]" + "=" + sortArray.get(i);
                                    countSort++;
                                }
                                LOGGER.info("SORT_Mandatory_KEY : " + str);
                                Object order = firstMandatoryMap.get("order");
                                List<Map> orderArray = (ArrayList<Map>) order;
                                for (int j = 0; j < orderArray.size(); j++) {
                                    str += "&" + "order" + "[" + countOrder + "]" + "=" + orderArray.get(j);
                                    countOrder++;
                                }
                                LOGGER.info("ORDER_Mandatory_KEY : " + str);
                                Object main = sortingmap.get("main");
                                List<Map> dashArray = (ArrayList<Map>) main;
                                for (Object KeyObject : dashArray) {
                                    for (Object Key : ((Map) KeyObject).keySet()) {
                                        LOGGER.info("SORT_KEY_first : " + (String) Key);
                                        String sortval = (String) Key;
                                        if (sortval.contains("sort")) {
                                            str += "&" + sortval + "[" + countSort + "]" + "=" + ((Map) KeyObject).get(sortval);
                                            countSort++;
                                            } else {
                                            str += "&" + sortval + "[" + countOrder + "]" + "=" + ((Map) KeyObject).get(sortval);
                                            countOrder++;
                                        }
                                        LOGGER.info("SORT_KEY : " + str);
                                    }
                                }
                            }
                            for (Object key : defaultvalsmap.keySet()) {
                                String keyval = (String) key;
                                LOGGER.info("KEY : " + keyval);
                                LOGGER.info("VAL: " + defaultvalsmap.get(keyval));
                                if (!keyval.equals("loan_tenure_unit") && !keyval.equals("period_unit")) {
                                    LOGGER.info("KEY : " + keyval);
                                    LOGGER.info("channelname : " + channelname);
                                    if (channelname.equalsIgnoreCase("fixed-deposit") || channelname.contains("Fixed")
                                    || channelname.contains("fixed")) {
                                        String actualValue = filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                        LOGGER.info("KEY PEriond: " + keyval);
                                        if (keyval.equals("period")) {
                                            actualValue = actualValue.replace("month", hyphenSymbol);
                                            actualValue = actualValue.replace("", hyphenSymbol);
                                            actualValue = actualValue.split(hyphenSymbol)[1].trim();
                                            LOGGER.info("KEY PERIOD: " + actualValue.toString().trim());
                                            str += "&filters[" + keyval + "]=" + actualValue.toString().trim();
                                            } else {
                                            str += "&filters[" + keyval + "]=" + actualValue;
                                        }
                                    }
                                    if (channelname.equalsIgnoreCase("personal-loan")
                                    && pageUrl.equals("http://www.moneysmart.sg/personal-loan")) {
                                        String actualValue = filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                        LOGGER.info("KEY citizenship: " + keyval);
                                        if (keyval.equals("citizenship")) {
                                            actualValue = actualValue.replace(" / PR", hyphenSymbol);
                                            actualValue = actualValue.split(hyphenSymbol)[0].trim();
                                            LOGGER.info("KEY PERIOD: " + actualValue.toString().trim());
                                            str += "&filters[" + keyval + "]=" + actualValue.toString().trim();
                                            } else {
                                            str += "&filters[" + keyval + "]=" + actualValue;
                                        }
                                    } else if (channelname.equalsIgnoreCase("education-loan") || channelname.contains("Education")
                                    || channelname.contains("education")) {
                                        String actualValue = filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                        LOGGER.info("KEY PEriond: " + keyval);
                                        LOGGER.info("KEY PEriond: " + actualValue);
                                        if (actualValue.equalsIgnoreCase("Singapore") || (actualValue.contains("Singapore"))) {
                                            str += "&filters[" + keyval + "]=" + "local";
                                            LOGGER.info("KEY PEriond: 114 " + str);
                                            } else {
                                            str += "&filters[" + keyval + "]=" + actualValue;
                                            LOGGER.info("KEY PEriond: 112 " + str);
                                        }
                                        } else {
                                        if (channelCountry.contains("sg")) {
                                            str += "&filters[" + keyval + "]="
                                            + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                            LOGGER.info("keyvalkeyvalkeyval = " + keyval);
                                            LOGGER.info("filtersKeyValue = " + filtersKeyValue);
                                            } else {
                                            if (pageUrl.contains("kredit-multiguna")) {
                                                if (keyval.contains("collateral_type")) {
                                                    str += "&filters[" + keyval + "]=" + DuitpintarcollateralId;
                                                    } else if (keyval.contains("loan_purpose")) {
                                                    str += "&filters[" + keyval + "]=" + Duitpintarloan_purposeId;
                                                    } else {
                                                    str += "&filters[" + keyval + "]="
                                                    + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                                }
                                                LOGGER.info("strProvider = " + str);
                                                LOGGER.info("keyvalkeyvalkeyval = " + keyval);
                                                LOGGER.info("filtersKeyValue = " + filtersKeyValue);
                                            } else if (pageUrl.contains("auto-loans"))
                                            {
                                                LOGGER.info("123456789 = " + keyval);
                                                LOGGER.info("keyvalkeyvalkeyvalkeyval = " + keyval);
                                                LOGGER.info("key--------lkeyval = " + keyval);
                                                if (keyval.equals("loan_tenure")) {
                                                    LOGGER.info("loan_tenureKeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + DuitpintarProviderMonth;
                                                    LOGGER.info("loan_tenurestr = " + str);
                                                    } else if (keyval.equals("coverage_areas")) {
                                                    LOGGER.info("coverage_areasKeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + DuitpintarLocation;
                                                    LOGGER.info("coverage_areasstr = " + str);
                                                    } else if (keyval.equals("downpayment_percent")) {
                                                    LOGGER.info("downpayment_percent KeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + DuitpintarLocationdownpayment_percent;
                                                    LOGGER.info("downpayment_percent str = " + str);
                                                    } else if (keyval.equals("down_payment")) {
                                                    LOGGER.info("down_payment KeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + Duitpintardown_payment;
                                                    LOGGER.info("down_payment str = " + str);
                                                    } else if (keyval.equals("loan_amoun")) {
                                                    LOGGER.info("loan_amoun KeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + Duitpintarloan_amount;
                                                    LOGGER.info("loan_amoun str = " + str);
                                                    } else if (keyval.equals("monthly_income")) {
                                                    LOGGER.info("monthly_income KeyValue = " + keyval);
                                                    str += "&filters[" + keyval + "]=" + Duitpintarmonthly_income;
                                                    LOGGER.info("monthly_income str = " + str);
                                                    } else {
                                                    str += "&filters[" + keyval + "]="
                                                    + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                                }
                                                LOGGER.info("Location = " + str);
                                                LOGGER.info("Locationkeyval = " + keyval);
                                                LOGGER.info("LocationfiltersKeyValue = " + filtersKeyValue);
                                                } else {
                                                if (keyval.contains("provider")) {
                                                    str += "&filters[" + keyval + "]=" + DuitpintarProviderId;
                                                    } else if (keyval.contains("employment")) {
                                                    str += "&filters[" + keyval + "]=" + DuitpintaremploymentId;
                                                    } else {
                                                    str += "&filters[" + keyval + "]="
                                                    + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
                                                }
                                                LOGGER.info("strProvider = " + str);
                                                LOGGER.info("keyvalkeyvalkeyval = " + keyval);
                                                LOGGER.info("filtersKeyValue = " + filtersKeyValue);
                                            }
                                        }
                                    }
                                } else
                                str += "&filters[" + keyval + "]=" + defaultvalsmap.get(keyval);
                            }
                            String FinalAPI = str + "&access_token=" + Constants.MYSTORE.ACESSTOKEN;
                            apiUrl = FinalAPI;
                            Constants.MYSTORE.SINGLEPRODUCTJSONAPI = apiUrl;
                            LOGGER.info("apiUrl = " + apiUrl);
                            LOGGER.info("filtersKeyValue = " + filtersKeyValue);
                            if (filtersKeyValue.containsKey("Studying")) {
                                LOGGER.info("For Studying1 = " + filtersKeyValue.get("Studying").get(apiInvocationCount));
                                LOGGER.info("trim = " + filtersKeyValue.get("Studying").toString().trim());
                                } else {
                                LOGGER.info("For null = ");
                            }
                            } catch (Exception e) {
                            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
                        }
                        return apiUrl;
                    }
                    public String Splitcountry(String countryValue) {
                        String[] Countryarray = countryValue.split(",");
                        String Allfiltercountries = "";
                        for (int countrycount = 0; countrycount < Countryarray.length; countrycount++) {
                            LOGGER.info("contryvalue " + Countryarray[countrycount]);
                            if (Allfiltercountries.length() == 0) {
                                Allfiltercountries = "filters%5Bcountries%5D%5B%5D=" + Countryarray[countrycount];
                                } else {
                                Allfiltercountries = Allfiltercountries + "&" + "filters%5Bcountries%5D%5B%5D="
                                + Countryarray[countrycount];
                            }
                        }
                        return Allfiltercountries;
                    }
                    public void setAPIResult(String apiResponse) {
                        JSONObject webResult = null;
                        try {
                            webResult = new JSONObject(apiResponse);
                            } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
                        }
                        this.webserviceApiResult = webResult;
                    }
                    public JSONObject getAPIResult() {
                        return webserviceApiResult;
                    }
                    public void setCalculationCount(int calculationCount) {
                        this.totalCalculationCount = calculationCount;
                    }
                    public int getCalculationCount() {
                        return totalCalculationCount;
                    }
                    @DataProvider(name = "getAPIResultFromDataProvider")
                    public Object[][] getAPIResultFromDataProvider() {
                        Object[][] jsonObject = new Object[1][1];
                        jsonObject[0][0] = getAPIResult();
                        return jsonObject;
                    }
                    @Override
                    public void webserviceRequestSuccessListener(int statusCode, String statusMessage, String apiResult) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void webserviceRequestFailureListener(int statusCode, String statusMessage) {
                        // TODO Auto-generated method stub
                    }
                    }
     