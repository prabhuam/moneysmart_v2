package com.moneysmart.channel.test;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.util.HashMap;
import java.util.Set;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.model.Constants.EXCEL_METHODS_INPUT;
import com.model.Constants.TEST_RESULT;
import com.model.ExcelInputData;
import com.model.GetExcelInput;
import com.selenium.base.BaseClass;
import java.util.logging.Level;
import java.util.logging.Logger;
public class BLogin extends BaseClass {
    private static final Logger LOGGER = Logger.getLogger(BLogin.class.getName());
    SoftAssert s_assert;
    /*
    * Method Start
    */
    @Test
    public void LoadMoneySmartWebpage(ITestContext testContext) {
        String testMethodNameTestNG = "LoadMoneySmartWebpage";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        /*
        * Excel Keys
        */
        String MethodNameKey = "Open Channel Url";
        String NameKey = "Name";
        String URLKey = "URL";
        /*
        * Custom messages
        */
        String SuccessMessage = "Web page opened successfully";
        String SuccessComments = "Browser web url and excel web url are equal";
        String FailureMessage = "Failed to load Web page";
        String FailureComments = "Browser web url and excel web url are not equal";
        /*
        * Step 1 Read Excel Input data...
        */
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.TestDescriptionKey);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.ActualResultKey);
        try {
            super.setUp();
            /*
            * Step 2 Check whether the method should execute or not
            */
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo LoadMoneySmartWebpage= " + executeYesOrNo);
            if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                throw new SkipException("This method skipped");
            }
            /*
            * Step 3 Start executing this method...
            */
            driver.get(pageUrl);
            driver.manage().window().maximize();
            String currentWebUrl = driver.getCurrentUrl();
            ExcelInputData excelInput = ExcelInputData.getInstance();
            excelInput.setWebDriver(driver);
            /*
            * Checking Area
            */
            if (currentWebUrl.equals(pageUrl)) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            }
            /*
            * Common Parameter
            */
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            /*
            * Other Return Required Parameter
            */
            resultMap.put(NameKey, channelName);
            resultMap.put(URLKey, pageUrl);
            /*
            * Step 3 END executing this method...
            */
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            /*
            * Set Attribute for prinint result in HTML
            */
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
    }
    /*
    * Method End
    */
    /*
    * FbLogin Validation
    */
    @Test
    public void DoFbLogin(ITestContext testContext) throws InterruptedException {
        String testMethodNameTestNG = "DoFbLogin";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        // * Excel Keys
        String MethodNameKey = "Facebook Login";
        String EmailKey = "Email";
        String PasswordKey = "Password";
        // * Custom messages
        String SuccessMessage = "FaceBook Login successfully";
        String SuccessComments = "FaceBook Login successfully and Validated";
        String FailureMessage = "Failed to Login FaceBook";
        String FailureComments = "Failed to FaceBook Login";
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String emailID = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, EmailKey);
        System.out.print("n emailID = " + emailID);
        String password = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, PasswordKey);
        System.out.print("n passd = " + password);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FacebookLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        try {
            // super.setUp();
            // * Step 2 Check whether the method should execute or not
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo DoThreeIcon2= " + executeYesOrNo);
            if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                throw new SkipException("This method skipped");
            }
            Thread.sleep(9000);
            driver.findElement(By.id("calculate-btn-1")).click();
            Thread.sleep(8000);
            driver.findElement(By.xpath("html/body/div[1]/div[6]/div/div/div[2]/form/div[1]/button[1]")).click();
            Thread.sleep(9000);
            Set<String> AllWindowHandles = driver.getWindowHandles();
            String window1 = (String) AllWindowHandles.toArray()[0];
            System.out.print("window1 handle code = " + AllWindowHandles.toArray()[0]);
            String window2 = (String) AllWindowHandles.toArray()[1];
            System.out.print("nwindow2 handle code = " + AllWindowHandles.toArray()[1]);
            driver.switchTo().window(window2);
            Thread.sleep(2000);
            driver.manage().window().maximize();
            driver.findElement(By.id("email")).sendKeys(emailID);
            driver.findElement(By.id("pass")).sendKeys(password);
            Thread.sleep(4000);
            driver.findElement(By.id("loginbutton")).click();
            Thread.sleep(5000);
            driver.switchTo().window(window1);
            Thread.sleep(2000);
            String url = driver.getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(9000);
            driver.findElement(By.cssSelector(".signed-in.has-dropdown.has-children.menu-item-has-children.menu-item"))
            .click();
            Actions actions = new Actions(driver);
            WebElement mouseover = driver.findElement(By
            .cssSelector(".signed-in.has-dropdown.has-children.menu-item-has-children.menu-item"));
            actions.moveToElement(mouseover);
            actions.click().perform();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Sign Out")).click();
            Thread.sleep(9000);
            WebElement element = driver.findElement(By
            .xpath("html/body/div[1]/div[2]/div[1]/div/div/div[2]/ul[1]/li[1]/a"));
            // * Checking Area
            if (element.isDisplayed()) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            // * Other Return Required Parameter
            resultMap.put(EmailKey, emailID);
            resultMap.put(PasswordKey, password);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            // * Set Attribute for prinint result in HTML
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
        driver.get("http://www.facebook.com");
        Thread.sleep(4000);
        driver.findElement(By.id("userNavigationLabel")).click();
        Thread.sleep(4000);
        driver.findElement(By.xpath("//span[contains(.,'Log Out')]")).click();
        Thread.sleep(4000);
        driver.get(pageUrl);
        driver.manage().window().maximize();
    }
    /*
    * Email Login Validation
    */
    @Test
    public void Login(ITestContext testContext) throws Exception {
        String testMethodNameTestNG = "Login";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        // * Excel Keys
        String MethodNameKey = "Email Login";
        String EmailKey = "Email";
        String PasswordKey = "Password";
        // * Custom messages
        String SuccessMessage = "Email Login Validation Passed";
        String SuccessComments = "Login with email working successfully";
        String FailureMessage = "Email Login Validation Failed";
        String FailureComments = "Login with email is not working";
        // Declaration Web related
        String webElement_Name_Username = "html/body/div[1]/div[7]/div/div/div/div[2]/form/div[2]/div[1]/input";
        String webElement_Name_Password = "html/body/div[1]/div[7]/div/div/div/div[2]/form/div[2]/div[2]/div/input";
        String webElement_XPath_LoginButton = "html/body/div[1]/div[7]/div/div/div/div[2]/form/div[2]/button";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String email = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, EmailKey);
        String password = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.EmailLogin_Validation_MethodNameKey, PasswordKey);
        try {
            // super.setUp();
            // * Step 2 Check whether the method should execute or not
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo DoThreeIcon2= " + executeYesOrNo);
            if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                throw new SkipException("This method skipped");
            }
            driver.get(pageUrl);
            Thread.sleep(4000);
            driver.findElement(By.id("calculate-btn-1")).click();
            Thread.sleep(8000);
            driver.findElement(By.className("nlf-login-trg")).click();
            Thread.sleep(4000);
            driver.findElement(By.linkText("Login with Email")).click();
            Thread.sleep(4000);
            driver.findElement(By.xpath(webElement_XPath_LoginButton)).click();
            driver.findElement(By.xpath(webElement_Name_Username)).clear();
            driver.findElement(By.xpath(webElement_Name_Username)).sendKeys(email);
            driver.findElement(By.xpath(webElement_Name_Password)).clear();
            driver.findElement(By.xpath(webElement_Name_Password)).sendKeys(password);
            driver.findElement(By.xpath(webElement_XPath_LoginButton)).click();
            WebElement loginbutton = driver.findElement(By.className("login-trg"));
            String text = loginbutton.getText();
            Thread.sleep(10000);
            // * Checking Area
            if (text.equals("Login")) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TEST_RESULT.R_DESRIPTION, description);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            // * Other Return Required Parameter
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            // * Set Attribute for prinint result in HTML
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
    }
    /* Gplus Login Validation */
    @Test
    public HashMap<String, Object> DoGplusLogin(WebDriver driver) throws Exception {
        // * Excel Keys
        String MethodNameKey = "GooglePlus Login";
        String EmailKey = "Email";
        String PasswordKey = "Password";
        // * Custom messages
        String SuccessMessage = "GPlus Login successfully";
        String SuccessComments = "GPlus Login successfully and Validated";
        String FailureMessage = "Failed to Login GPlus";
        String FailureComments = "Failed to Login GPlus Invalid Credential";
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GooglePlusLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        String emailID = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GooglePlusLogin_Validation_MethodNameKey, EmailKey);
        System.out.print("n emailID = " + emailID);
        String password = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GooglePlusLogin_Validation_MethodNameKey, PasswordKey);
        System.out.print("n passd = " + password);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GooglePlusLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GooglePlusLogin_Validation_MethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        // driver.get(pageUrl);
        Thread.sleep(9000);
        driver.findElement(By.id("calculate-btn-1")).click();
        WebElement myDynamicElements = (new WebDriverWait(driver, 15)).until(ExpectedConditions
        .presenceOfElementLocated(By.id("calculate-btn-1")));
        // Thread.sleep(6000);
        driver.findElement(By.xpath("html/body/div[1]/div[6]/div/div/div[2]/form/div[1]/button[2]")).click();
        WebElement myDynamicGP = (new WebDriverWait(driver, 15)).until(ExpectedConditions.presenceOfElementLocated(By
        .xpath("html/body/div[1]/div[6]/div/div/div[2]/form/div[1]/button[2]")));
        Set<String> G_AllWindowHandles = driver.getWindowHandles();
        String G_window1 = (String) G_AllWindowHandles.toArray()[0];
        System.out.print("window1 handle code = " + G_AllWindowHandles.toArray()[0]);
        String G_window2 = (String) G_AllWindowHandles.toArray()[1];
        System.out.print("nwindow2 handle code = " + G_AllWindowHandles.toArray()[1]);
        driver.switchTo().window(G_window2);
        Thread.sleep(4000);
        driver.manage().window().maximize();
        driver.findElement(By.id("Email")).sendKeys(emailID);
        driver.findElement(By.id("next")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Passwd")).sendKeys(password);
        driver.findElement(By.id("signIn")).click();
        Thread.sleep(2000);
        driver.switchTo().window(G_window1);
        Thread.sleep(2000);
        String url2 = driver.getCurrentUrl();
        LOGGER.info("n Current Page URl:" + url2);
        Thread.sleep(9000);
        Thread.sleep(6000);
        driver.findElement(By.cssSelector(".signed-in.has-dropdown.has-children.menu-item-has-children.menu-item"))
        .click();
        Actions actions = new Actions(driver);
        WebElement mouseover = driver.findElement(By
        .cssSelector(".signed-in.has-dropdown.has-children.menu-item-has-children.menu-item"));
        actions.moveToElement(mouseover);
        actions.click().perform();
        Thread.sleep(2000);
        driver.findElement(By.linkText("Sign Out")).click();
        Thread.sleep(9000);
        WebElement element = driver
        .findElement(By.xpath("html/body/div[1]/div[2]/div[1]/div/div/div[2]/ul[1]/li[1]/a"));
        // * Checking Area
        if (element.isDisplayed()) {
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
            resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
            resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
            } else {
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
            resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
        }
        // * Common Parameter
        resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
        resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
        resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
        resultMap.put(TEST_RESULT.R_DESRIPTION, description);
        // * Other Return Required Parameter
        resultMap.put(EmailKey, emailID);
        resultMap.put(PasswordKey, password);
        driver.get("http://www.gmail.com");
        Thread.sleep(9000);
        Thread.sleep(9000);
        driver.findElement(
        By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/a/span"))
        .click();
        Thread.sleep(4000);
        driver.findElement(
        By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[4]/div[2]/div[3]/div[2]/a"))
        .click();
        Thread.sleep(4000);
        driver.get("http://www.moneysmart.sg/home-loan");
        driver.manage().window().maximize();
        return resultMap;
    }
}