package com.moneysmart.channel.test;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.common.utility.WebserviceRequest;
import com.common.utility.WebserviceRequestListener;
import com.model.Constants;
import com.model.Constants.TEST_RESULT;
import com.model.ExcelInputData;
import com.model.GetExcelInput;
import com.model.Constants.EXCEL_METHODS_INPUT;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ZDetails implements WebserviceRequestListener {
    private static final Logger LOGGER = Logger.getLogger(ZDetails.class.getName());
    private WebserviceRequestListener webserviceListener;
    /*
    * Data Provider Methods
    */
    String expectedResultExcel = "";
    String testDescriptionExcel = "";
    String actualResultExcel = "";
    String executeOrNotExcel = "";
    String expecedResultFromExcel = "";
    String cururl = "";
    String actualFilterFailureResult = "";
    String actualResultFromExcel = "";
    SoftAssert s_assert;
    boolean isAllDetailsPassed = true;
    @Test
    public void ZApplyNow(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "ZApplyNow";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "DetailPage Apply NowButton";
        String Web_Element = "WebElement Applay NowButton";
        String Apply_Now_URL = "Apply Now URL";
        String SuccessMessage = "Apply Now Working";
        String SuccessComments = "Apply Now Present and Working";
        String FailureMessage = "Apply Now Present Not Working";
        String FailureComments = "Apply Now Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey,
        Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String ApplyNowURL = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailApplyNowMethodNameKey, Apply_Now_URL);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("ApplyNowURL = " + ApplyNowURL);
        LOGGER.info("ApplyNowURL WebElement = " + WebElement);
        LOGGER.info("ApplyNowURL WebElement channelName = " + channelName);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            getWebDriver().navigate().to(pageUrl);
            Thread.sleep(10000);
            getWebDriver().findElement(By.xpath(WebElement)).click();
            Thread.sleep(9000);
            if (!channelName.contains("Car Loan")) {
                LOGGER.info("FIRST = " + channelName);
                if (!channelName.contains("multi-purpose-loan")) {
                    LOGGER.info("SECOND = " + channelName);
                    if (!channelName.contains("auto-loans")) {
                        LOGGER.info("Third = " + channelName);
                        Set<String> AllWindowHandles = getWebDriver().getWindowHandles();
                        window1 = (String) AllWindowHandles.toArray()[0];
                        System.out.print("window1 handle code = " + AllWindowHandles.toArray()[0]);
                        String window2 = (String) AllWindowHandles.toArray()[1];
                        System.out.print("nwindow2 handle code = " + AllWindowHandles.toArray()[1]);
                        String window = "";
                        getWebDriver().switchTo().window(window2);
                        Thread.sleep(10000);
                    }
                }
            }
            String url = getWebDriver().getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(1000);
            if (url.contains(ApplyNowURL)) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap == null) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
        try {
            getWebDriver().switchTo().window(window1);
            } catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        Thread.sleep(10000);
    }
    @Test
    public void ZZApplyNow(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "ZZApplyNow";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Footer Apply NowButton";
        String Web_Element = "WebElement Applay NowButton";
        String Apply_Now_URL = "Apply Now URL";
        String SuccessMessage = "Apply Now Working";
        String SuccessComments = "Apply Now Present and Working";
        String FailureMessage = "Apply Now Present Not Working";
        String FailureComments = "Apply Now Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String ApplyNowURL = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageFooterApplyNowMethodNameKey, Apply_Now_URL);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("ApplyNowURL = " + ApplyNowURL);
        LOGGER.info("ApplyNowURL WebElement = " + WebElement);
        LOGGER.info("ApplyNowURL WebElement channelName = " + channelName);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            getWebDriver().navigate().to(pageUrl);
            Thread.sleep(10000);
            getWebDriver().findElement(By.xpath(WebElement)).click();
            Thread.sleep(9000);
            if (!channelName.contains("Car Loan")) {
                LOGGER.info("FIRST = " + channelName);
                if (!channelName.contains("multi-purpose-loan")) {
                    LOGGER.info("SECOND = " + channelName);
                    if (!channelName.contains("auto-loans")) {
                        LOGGER.info("Third = " + channelName);
                        Set<String> AllWindowHandles = getWebDriver().getWindowHandles();
                        window1 = (String) AllWindowHandles.toArray()[0];
                        System.out.print("window1 handle code = " + AllWindowHandles.toArray()[0]);
                        String window2 = (String) AllWindowHandles.toArray()[1];
                        System.out.print("nwindow2 handle code = " + AllWindowHandles.toArray()[1]);
                        String window = "";
                        getWebDriver().switchTo().window(window2);
                        Thread.sleep(10000);
                    }
                }
            }
            String url = getWebDriver().getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(1000);
            if (url.contains(ApplyNowURL)) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap == null) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
        try {
            getWebDriver().switchTo().window(window1);
            } catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        Thread.sleep(10000);
    }
    @Test
    public void ZZGotoSite(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "GotoSite";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Go to Site";
        String Web_Element = "WebElement Go to Site";
        String Apply_Now_URL = "Go to Site URL";
        String SuccessMessage = "Go to Site URL Working";
        String SuccessComments = "Go to Site URL Present and Working";
        String FailureMessage = "Go to Site URL Present Not Working";
        String FailureComments = "Go to Site URL Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        EXCEL_METHODS_INPUT.TestDescriptionKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.GotoSiteMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        EXCEL_METHODS_INPUT.ActualResultKey);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String ApplyNowURL = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.GotoSiteMethodNameKey,
        Apply_Now_URL);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("Go to Site URL = " + ApplyNowURL);
        LOGGER.info("Go to Site URL WebElement = " + WebElement);
        LOGGER.info("Go to Site URL WebElement channelName = " + channelName);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            getWebDriver().navigate().to(pageUrl);
            Thread.sleep(10000);
            getWebDriver().findElement(By.xpath(WebElement)).click();
            Thread.sleep(9000);
            if (!channelName.contains("Car Loan")) {
                LOGGER.info("FIRST = " + channelName);
                if (!channelName.contains("multi-purpose-loan")) {
                    LOGGER.info("SECOND = " + channelName);
                    if (!channelName.contains("auto-loans")) {
                        LOGGER.info("Third = " + channelName);
                        Set<String> AllWindowHandles = getWebDriver().getWindowHandles();
                        window1 = (String) AllWindowHandles.toArray()[0];
                        System.out.print("window1 handle code = " + AllWindowHandles.toArray()[0]);
                        String window2 = (String) AllWindowHandles.toArray()[1];
                        System.out.print("nwindow2 handle code = " + AllWindowHandles.toArray()[1]);
                        String window = "";
                        getWebDriver().switchTo().window(window2);
                        Thread.sleep(10000);
                    }
                }
            }
            String url = getWebDriver().getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(1000);
            if (url.contains(ApplyNowURL)) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap == null) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
        try {
            getWebDriver().switchTo().window(window1);
            } catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        Thread.sleep(10000);
    }
    @Test
    public void ZZFooterGotoSite(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "FooterGotoSite";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Footer Go to Site";
        String Web_Element = "WebElement Go to Site";
        String Apply_Now_URL = "Go to Site URL";
        String SuccessMessage = "Footer Go to Site URL Working";
        String SuccessComments = "Footer Go to Site URL Present and Working";
        String FailureMessage = "Footer Go to Site URL Present Not Working";
        String FailureComments = "Footer Go to Site URL Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey,
        Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
        String ApplyNowURL = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.FooterGotoSiteMethodNameKey, Apply_Now_URL);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("Go to Site URL = " + ApplyNowURL);
        LOGGER.info("Go to Site URL WebElement = " + WebElement);
        LOGGER.info("Go to Site URL WebElement channelName = " + channelName);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            getWebDriver().navigate().to(pageUrl);
            Thread.sleep(10000);
            getWebDriver().findElement(By.xpath(WebElement)).click();
            Thread.sleep(9000);
            if (!channelName.contains("Car Loan")) {
                LOGGER.info("FIRST = " + channelName);
                if (!channelName.contains("multi-purpose-loan")) {
                    LOGGER.info("SECOND = " + channelName);
                    if (!channelName.contains("auto-loans")) {
                        LOGGER.info("Third = " + channelName);
                        Set<String> AllWindowHandles = getWebDriver().getWindowHandles();
                        window1 = (String) AllWindowHandles.toArray()[0];
                        System.out.print("window1 handle code = " + AllWindowHandles.toArray()[0]);
                        String window2 = (String) AllWindowHandles.toArray()[1];
                        System.out.print("nwindow2 handle code = " + AllWindowHandles.toArray()[1]);
                        String window = "";
                        getWebDriver().switchTo().window(window2);
                        Thread.sleep(10000);
                    }
                }
            }
            String url = getWebDriver().getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(1000);
            if (url.contains(ApplyNowURL)) {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
                resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
                resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
                resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
                resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            // * Common Parameter
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap == null) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, description);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
        }
        try {
            getWebDriver().switchTo().window(window1);
            } catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        Thread.sleep(10000);
    }
    @Test
    public void ZDetailtest(ITestContext testContext) {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        String TestMethodName = "ZDetailtest";
        String TestMethodNameReport = "Detail Validation";
        WebElement element = null;
        GetExcelInput getInput = new GetExcelInput();
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        try {
            ExcelInputData excelinputobj = ExcelInputData.getInstance();
            Object productobjYAML = excelinputobj.getYAMLData();
            Map productmap = (Map) productobjYAML;
            Object tablefields = productmap.get("detail_fields");
            if (channelName.contains("Savings Account")) {
                Map savingTableFields = (Map) tablefields;
                Object savingMainDetailFields = savingTableFields.get("main_detail");
                LOGGER.info("Main_Details" + savingMainDetailFields);
                List<Map> tablefieldarray = (ArrayList<Map>) savingMainDetailFields;
                int webElementCoutn = 0;
                for (Map singleMapInTable : tablefieldarray) {
                    LOGGER.info("SECTION maps: " + singleMapInTable.toString());
                    for (Object sectionMapKey : singleMapInTable.keySet()) {
                        LOGGER.info("SECTION: " + sectionMapKey.toString());
                        if (!sectionMapKey.toString().trim().equals("Savings Account Rating")) {
                            if (!sectionMapKey.toString().trim().equals("Bonus Interest Rate")) {
                                LOGGER.info("SECTION: insied " + sectionMapKey.toString());
                                if (!sectionMapKey.toString().trim().equals("Interest Rate")) {
                                    LOGGER.info("SECTION: insied_one " + sectionMapKey.toString());
                                    HashMap<String, String> SECTION = new HashMap<String, String>();
                                    String parentelement = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "ParentElement");
                                    String childelement = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "ClildElement");
                                    String keyelement = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "KeyElement");
                                    String valueelement = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "ValueElement");
                                    String testDescriptionExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "Test Description");
                                    String actualResultExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "Actual Result");
                                    String expecedResultFromExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "Expected Test Result");
                                    String executeOrNotExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    sectionMapKey.toString(), "Execute");
                                    executeOrNotExcel = executeOrNotExcels;
                                    if (expecedResultFromExcel.length() > 0) {
                                        expecedResultFromExcel = expecedResultFromExcel + " , "
                                        + expecedResultFromExcels;
                                        } else {
                                        expecedResultFromExcel = expecedResultFromExcels;
                                    }
                                    if (testDescriptionExcel.length() > 0) {
                                        testDescriptionExcel = testDescriptionExcel + " , " + testDescriptionExcels;
                                        } else {
                                        testDescriptionExcel = testDescriptionExcels;
                                    }
                                    if (channelName.contains("Savings Account")) {
                                        WebElement webElement = getWebDriver().findElement(By.xpath(parentelement));
                                        List<WebElement> contentList = webElement.findElements(By
                                        .cssSelector(childelement));
                                        LOGGER.info("Inside If SECTION-parentelement: " + webElement);
                                        LOGGER.info("Inside If SECTION-childelement: " + contentList);
                                        for (WebElement singleSectionWebElement : contentList) {
                                            String productNameWeb = singleSectionWebElement.findElement(
                                            By.className(valueelement)).getText();
                                            String producttitWeb = singleSectionWebElement.findElement(
                                            By.className(keyelement)).getText();
                                            LOGGER.info("InsideproductNameWeb " + producttitWeb + " --- "
                                            + productNameWeb);
                                            SECTION.put(producttitWeb, productNameWeb);
                                        }
                                        } else {
                                        WebElement webElement = getWebDriver().findElement(By.xpath(parentelement));
                                        List<WebElement> contentList = webElement.findElements(By
                                        .className(childelement));
                                        LOGGER.info("SECTION-parentelement: " + webElement);
                                        LOGGER.info("SECTION-childelement: " + contentList);
                                        for (WebElement singleSectionWebElement : contentList) {
                                            String productNameWeb = singleSectionWebElement.findElement(
                                            By.className(valueelement)).getText();
                                            String producttitWeb = singleSectionWebElement.findElement(
                                            By.className(keyelement)).getText();
                                            SECTION.put(producttitWeb, productNameWeb);
                                        }
                                    }
                                    LOGGER.info("SECTION From web: " + SECTION);
                                    //
                                    Map fields = (Map) singleMapInTable.get(sectionMapKey);
                                    String channelUURL = getInput.get_A_Value_Using_Key_Of_A_Method(
                                    EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
                                    EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
                                    for (Object filedKey : fields.keySet()) {
                                        LOGGER.info("filedKey" + filedKey.toString());
                                        if (filedKey.toString().equals("fields")) {
                                            LOGGER.info("filedKey" + filedKey.toString());
                                            LOGGER.info("filedKeyValue " + fields.get(filedKey));
                                            if (fields.get(filedKey) instanceof Map) {
                                                Map field_map = (Map) fields.get(filedKey);
                                                LOGGER.info(" field_map: " + field_map);
                                                for (Object key : field_map.keySet()) {
                                                    String str = "";
                                                    if (channelName.equals("Savings Account")) {
                                                        String keyValFromYAML = key.toString();
                                                        LOGGER.info("Key_IF " + key.toString());
                                                        LOGGER.info("SECTION.get(keyValFromYAML) "
                                                        + SECTION.get(keyValFromYAML));
                                                        LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                                        if (keyValFromYAML.equals("Citizenship Status")) {
                                                            keyValFromYAML = "Citizenship Status";
                                                            } else if (keyValFromYAML.equals("Minimum Initial Deposit")) {
                                                            keyValFromYAML = "Minimum Initial Deposit";
                                                            } else if (keyValFromYAML.equals("Min. Ave. Monthly Balance")) {
                                                            keyValFromYAML = "Min. Ave. Monthly Balance";
                                                            } else if (keyValFromYAML.equals("Fall Below Fee")) {
                                                            keyValFromYAML = "Fall Below Fee";
                                                            } else if (keyValFromYAML.equals("Monthly Account Fee")) {
                                                            keyValFromYAML = "Monthly Account Fee";
                                                            } else if (keyValFromYAML.equals("ATM Withdrawal Charges")) {
                                                            keyValFromYAML = "ATM Withdrawal Charges";
                                                            } else if (keyValFromYAML.equals("Cash Deposit Fee")) {
                                                            keyValFromYAML = "Cash Deposit Fee";
                                                            } else if (keyValFromYAML.equals("Counter Withdrawal Fee")) {
                                                            keyValFromYAML = "Counter Withdrawal Fee";
                                                            } else if (keyValFromYAML.equals("Passbook Replacement Fee")) {
                                                            keyValFromYAML = "Passbook Replacement Fee";
                                                            } else if (keyValFromYAML.equals("Incidental Overdraft Charge")) {
                                                            keyValFromYAML = "Incidental Overdraft Charge";
                                                            } else if (keyValFromYAML.equals("Other Fees(if any)")) {
                                                            keyValFromYAML = "Other Fees(if any)";
                                                            } else if (keyValFromYAML.equals("No. of ATMs Available")) {
                                                            keyValFromYAML = "No. of ATMs Available";
                                                            } else if (keyValFromYAML.equals("Online Banking")) {
                                                            keyValFromYAML = "Online Banking";
                                                        }
                                                        if (sectionMapKey.toString().equals("Eligibility")) {
                                                            str = getSavingLoanValue(sectionMapKey.toString(),
                                                            keyValFromYAML);
                                                            LOGGER.info("str for Savings-loan = not use_common " + str);
                                                            compareWebAndJson(filedKey.toString(),
                                                            SECTION.get(keyValFromYAML), str);
                                                            } else if (sectionMapKey.toString().equals("Fees and Charges")) {
                                                            str = getSavingLoanValue(sectionMapKey.toString(),
                                                            keyValFromYAML);
                                                            LOGGER.info("str for Savings-loan = not use_common " + str);
                                                            LOGGER.info("str for Savings-loan = not SECTION " + SECTION);
                                                            LOGGER.info("str for Savings-loan = not keyValFromYAML "
                                                            + keyValFromYAML);
                                                            compareWebAndJson(filedKey.toString(),
                                                            SECTION.get(keyValFromYAML), str);
                                                            } else if (sectionMapKey.toString().equals("Other Features")) {
                                                            str = getSavingLoanValue(sectionMapKey.toString(),
                                                            keyValFromYAML);
                                                            LOGGER.info("str for Savings-loan = not use_common " + str);
                                                            compareWebAndJson(filedKey.toString(),
                                                            SECTION.get(keyValFromYAML), str);
                                                        }
                                                        } else {
                                                        str = fetchValueFromJSONFor(removeBraces((String) field_map
                                                        .get(key)));
                                                        LOGGER.info(" strstrstrstr: " + str);
                                                        if (SECTION.containsKey(key)) {
                                                            LOGGER.info(" If VAl From Api str: " + str);
                                                            compareWebAndJson(filedKey.toString(), SECTION.get(key),
                                                            str);
                                                        }
                                                    }
                                                }
                                                } else {
                                                // Common fields
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                webElementCoutn++;
                } else {
                List<Map> tablefieldarray = (ArrayList<Map>) tablefields;
                int webElementCoutn = 0;
                for (Map singleMapInTable : tablefieldarray) {
                    LOGGER.info("SECTION maps: " + singleMapInTable.toString());
                    for (Object sectionMapKey : singleMapInTable.keySet()) {
                        LOGGER.info("SECTION: " + sectionMapKey.toString());
                        //
                        // GetExcelInput getInput = new GetExcelInput();
                        HashMap<String, String> SECTION = new HashMap<String, String>();
                        String parentelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                        "ParentElement");
                        String childelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                        "ClildElement");
                        String keyelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                        "KeyElement");
                        String valueelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                        "ValueElement");
                        String testDescriptionExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                        sectionMapKey.toString(), "Test Description");
                        String actualResultExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                        sectionMapKey.toString(), "Actual Result");
                        String expecedResultFromExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                        sectionMapKey.toString(), "Expected Test Result");
                        String executeOrNotExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                        sectionMapKey.toString(), "Execute");
                        executeOrNotExcel = executeOrNotExcels;
                        if (expecedResultFromExcel.length() > 0) {
                            expecedResultFromExcel = expecedResultFromExcel + " , " + expecedResultFromExcels;
                            } else {
                            expecedResultFromExcel = expecedResultFromExcels;
                        }
                        if (testDescriptionExcel.length() > 0) {
                            testDescriptionExcel = testDescriptionExcel + " , " + testDescriptionExcels;
                            } else {
                            testDescriptionExcel = testDescriptionExcels;
                        }
                        WebElement webElement = getWebDriver().findElement(By.xpath(parentelement));
                        List<WebElement> contentList = webElement.findElements(By.className(childelement));
                        for (WebElement singleSectionWebElement : contentList) {
                            String productNameWeb = singleSectionWebElement.findElement(By.className(valueelement))
                            .getText();
                            String producttitWeb = singleSectionWebElement.findElement(By.className(keyelement))
                            .getText();
                            SECTION.put(producttitWeb, productNameWeb);
                        }
                        LOGGER.info("SECTION From web: " + SECTION);
                        //
                        Map fields = (Map) singleMapInTable.get(sectionMapKey);
                        String channelUURL = getInput.get_A_Value_Using_Key_Of_A_Method(
                        EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
                        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
                        for (Object filedKey : fields.keySet()) {
                            LOGGER.info("filedKey" + filedKey.toString());
                            if (filedKey.toString().equals("fields")) {
                                LOGGER.info("filedKey" + filedKey.toString());
                                LOGGER.info("filedKeyValue " + fields.get(filedKey));
                                if (fields.get(filedKey) instanceof Map) {
                                    Map field_map = (Map) fields.get(filedKey);
                                    LOGGER.info(" field_map: " + field_map);
                                    for (Object key : field_map.keySet()) {
                                        String str = "";
                                        if (channelName.equals("auto-loans")) {
                                            LOGGER.info("Key_IF " + key.toString());
                                            LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                            String keyValFromYAML = key.toString();
                                            if (keyValFromYAML.equals("features.admin_fee")) {
                                                keyValFromYAML = "Admin Fee";
                                                } else if (keyValFromYAML.equals("features.other_fee")) {
                                                keyValFromYAML = "Other Fees";
                                                } else if (keyValFromYAML.equals("features.late_fee")) {
                                                keyValFromYAML = "Late Fee";
                                                } else if (keyValFromYAML.equals("features.early_settlement_fee")) {
                                                keyValFromYAML = "Early Settlement Fee";
                                                } else if (keyValFromYAML.equals("requirements.citizenship")) {
                                                keyValFromYAML = "Citizenship Status";
                                                } else if (keyValFromYAML.equals("requirements.age_min")) {
                                                keyValFromYAML = "Minimum Age";
                                                } else if (keyValFromYAML.equals("requirements.age_max")) {
                                                keyValFromYAML = "Maximum Age";
                                                } else if (keyValFromYAML.equals("Other Requirements")) {
                                                keyValFromYAML = "Other Requirements";
                                                } else if (keyValFromYAML.equals("requirements.employment")) {
                                                keyValFromYAML = "Employment Status";
                                                } else if (keyValFromYAML.equals("requirements.doc_ktp")) {
                                                keyValFromYAML = "KTP/ID Required?";
                                                } else if (keyValFromYAML.equals("requirements.doc_pay_slip")) {
                                                keyValFromYAML = "Salary Slip Required?";
                                                } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                                keyValFromYAML = "Coverage Area?";
                                            }
                                            if (!keyValFromYAML.equals("Coverage Area?")) {
                                                str = getAutoLoanValue(sectionMapKey.toString(), keyValFromYAML);
                                                LOGGER.info("str for Auto-loan = not use_common " + str);
                                                compareWebAndJson(filedKey.toString(), SECTION.get(keyValFromYAML), str);
                                            }
                                            } else if (channelName.equals("multi-purpose-loan")) {
                                            LOGGER.info("Key_IF " + key.toString());
                                            LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                            String keyValFromYAML = key.toString();
                                            if (keyValFromYAML.equals("features.admin_fee")) {
                                                keyValFromYAML = "Admin Fee";
                                                } else if (keyValFromYAML.equals("features.other_fee")) {
                                                keyValFromYAML = "Other Fees";
                                                } else if (keyValFromYAML.equals("features.late_fee")) {
                                                keyValFromYAML = "Late Fee";
                                                } else if (keyValFromYAML.equals("features.early_settlement_fee")) {
                                                keyValFromYAML = "Early Settlement Fee";
                                                } else if (keyValFromYAML.equals("requirements.citizenship")) {
                                                keyValFromYAML = "Citizenship Status";
                                                } else if (keyValFromYAML.equals("Other Requirement")) {
                                                keyValFromYAML = "Other Requirement";
                                                } else if (keyValFromYAML.equals("requirements.employment")) {
                                                keyValFromYAML = "Employment Status";
                                                } else if (keyValFromYAML.equals("requirements.doc_ktp")) {
                                                keyValFromYAML = "KTP/ID Required?";
                                                } else if (keyValFromYAML.equals("requirements.doc_pay_slip")) {
                                                keyValFromYAML = "Salary Slip Required?";
                                                } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                                keyValFromYAML = "Coverage Area?";
                                                } else if (keyValFromYAML.equals("requirements.income_min")) {
                                                keyValFromYAML = "Minimum Income";
                                                } else if (keyValFromYAML.equals("requirements.age")) {
                                                keyValFromYAML = "Age Requirement";
                                                } else if (keyValFromYAML.equals("features.collateral_type")) {
                                                keyValFromYAML = "Collateral Type";
                                                } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                                keyValFromYAML = "Coverage Area?";
                                                } else if (keyValFromYAML.equals("specifications.min_loan_amount_txt")) {
                                                keyValFromYAML = "Minimum Loan Amount";
                                                } else if (keyValFromYAML.equals("specifications.max_loan_amount_txt")) {
                                                keyValFromYAML = "Maximum Loan Amount";
                                                } else if (keyValFromYAML.equals("features.min_loan_tenure")) {
                                                keyValFromYAML = "Minimum Loan Tenure";
                                                } else if (keyValFromYAML.equals("features.max_loan_tenure")) {
                                                keyValFromYAML = "Maximum Loan Tenure";
                                                } else if (keyValFromYAML.equals("features.estimated_process_time")) {
                                                keyValFromYAML = "Estimated Process Time";
                                                } else if (keyValFromYAML.equals("features.usage_type")) {
                                                keyValFromYAML = "Usage Type";
                                            }
                                            if (!keyValFromYAML.equals("Coverage Area?")) {
                                                LOGGER.info("str for Multi- Key " + keyValFromYAML);
                                                if (!keyValFromYAML.equals("requirements.others_requirement")) {
                                                    LOGGER.info("str for Multi- single-Key " + keyValFromYAML);
                                                    str = getMultiLoanValue(sectionMapKey.toString(), keyValFromYAML);
                                                    LOGGER.info("str for Auto-loan = not use_common " + str);
                                                    compareWebAndJson(filedKey.toString(), SECTION.get(keyValFromYAML),
                                                    str);
                                                }
                                            }
                                            /*
                                            * else if(!keyValFromYAML.equals(
                                            * "Other Requirements")){
                                                *
                                                * str =
                                                * getMultiLoanValue(sectionMapKey
                                                * .toString(),keyValFromYAML);
                                                *
                                                * LOGGER.info(
                                                * "str for Auto-loan = not use_common "
                                                * +str);
                                                *
                                                * compareWebAndJson(filedKey.toString
                                                * (),SECTION.get(keyValFromYAML),
                                            * str); }
                                            */
                                        } else if (channelName.equals("personal-loans")
                                        && channelUURL
                                        .equals("https://www.duitpintar.com/en_sg/unsecured-loans")) {
                                            LOGGER.info("Key_IF " + key.toString());
                                            LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                            String keyValFromYAML = key.toString();
                                            if (keyValFromYAML.equals("features.late_payment_fee")) {
                                                keyValFromYAML = "Late Payment Fee";
                                                } else if (keyValFromYAML.equals("features.other_fee")) {
                                                keyValFromYAML = "Other Fees";
                                                } else if (keyValFromYAML.equals("requirements.citizenship")) {
                                                keyValFromYAML = "Citizenship Status";
                                                } else if (keyValFromYAML.equals("requirements.age")) {
                                                keyValFromYAML = "Age Requirement";
                                                } else if (keyValFromYAML.equals("requirements.employment")) {
                                                keyValFromYAML = "Employment Status";
                                                } else if (keyValFromYAML.equals("requirements.doc_ktp")) {
                                                keyValFromYAML = "KTP/ID Required?";
                                                } else if (keyValFromYAML.equals("requirements.doc_pay_slip")) {
                                                keyValFromYAML = "Salary Slip Required?";
                                                } else if (keyValFromYAML.equals("requirements.others_requirement")) {
                                                keyValFromYAML = "Others Requirement";
                                                } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                                keyValFromYAML = "Coverage Area?";
                                                } else if (keyValFromYAML.equals("features.admin_fee")) {
                                                keyValFromYAML = "Admin Fee";
                                                } else if (keyValFromYAML.equals("features.early_settlement_fee")) {
                                                keyValFromYAML = "Early Settlement Fee";
                                                } else if (keyValFromYAML.equals("specifications.min_loan_amount_txt")) {
                                                keyValFromYAML = "Minimum Loan Amount";
                                                } else if (keyValFromYAML.equals("specifications.max_loan_amount_txt")) {
                                                keyValFromYAML = "Maximum Loan Amount";
                                                } else if (keyValFromYAML.equals("specifications.max_loan_tenure_txt")) {
                                                keyValFromYAML = "Maximum Loan Tenure";
                                                } else if (keyValFromYAML.equals("Interest Rate")) {
                                                keyValFromYAML = "Interest Rate";
                                                } else if (keyValFromYAML.equals("features.approval_duration")) {
                                                keyValFromYAML = "Approval Duration";
                                                } else if (keyValFromYAML.equals("specifications.min_monthly_income_txt")) {
                                                keyValFromYAML = "Minimum Monthly Income";
                                            }
                                            LOGGER.info(" Unsecured-loan = not use_common ");
                                            if (!keyValFromYAML.equals("Coverage Area?")) {
                                                str = getUnSecuredLoanValue(sectionMapKey.toString(), keyValFromYAML);
                                                LOGGER.info("str for Unsecured-loan = not use_common " + str);
                                                compareWebAndJson(filedKey.toString(), SECTION.get(keyValFromYAML), str);
                                            }
                                            } else {
                                            str = fetchValueFromJSONFor(removeBraces((String) field_map.get(key)));
                                            LOGGER.info(" strstrstrstr: " + str);
                                            if (SECTION.containsKey(key)) {
                                                LOGGER.info(" If VAl From Api str: " + str);
                                                compareWebAndJson(filedKey.toString(), SECTION.get(key), str);
                                            }
                                        }
                                    }
                                    } else {
                                    ArrayList field_map = (ArrayList) fields.get(filedKey);
                                    if ((field_map.get(0)).equals("use_common")) {
                                        Object commonfields = productmap.get("common");
                                        Map commonfieldarray = (Map) commonfields;
                                        for (Object commonkey : commonfieldarray.keySet()) {
                                            LOGGER.info("commonkey" + commonkey);
                                            for (Object common : ((Map) commonfieldarray.get(commonkey)).keySet()) {
                                                LOGGER.info("common" + common);
                                                if (common.toString().equals("label")) {
                                                    LOGGER.info("commonkey2 "
                                                    + (((Map) commonfieldarray.get(commonkey))).get(common));
                                                    for (Object fieldKey : ((Map) (((Map) commonfieldarray
                                                    .get(commonkey))).get(common)).keySet()) {
                                                        LOGGER.info("commonkey3 "
                                                        + (String) ((Map) (((Map) commonfieldarray
                                                        .get(commonkey))).get(common)).get(fieldKey));
                                                        if (((String) ((Map) (((Map) commonfieldarray.get(commonkey)))
                                                    .get(common)).get(fieldKey)).contains("} {")) {
                                                            String[] string = ((String) ((Map) (((Map) commonfieldarray
                                                            .get(commonkey))).get(common)).get(fieldKey))
                                                            .split("\\} \\{");
                                                            LOGGER.info("commonkey4 " + string[0].toString()
                                                            + string[1].toString());
                                                            String str = "";
                                                            if (channelName.equals("auto-loans")) {
                                                                LOGGER.info("fieldKey " + fieldKey.toString());
                                                                LOGGER.info("SECTION.get(fieldKey) "
                                                                + SECTION.get(fieldKey));
                                                                LOGGER.info("commonkey.toString() "
                                                                + commonkey.toString());
                                                                String spliteMaxTenure = SECTION.get(fieldKey).split(
                                                                " ")[0].trim();
                                                                // String
                                                                // nodeValueToBeGetFromYAML
                                                                // =
                                                                // getAutoLoanCommonValue(commonkey.toString(),fieldKey.toString(),commonfieldarray,
                                                                // common);
                                                                str = fetchValueFromJSONFor(removeBraces(string[0]));
                                                                if (fieldKey.toString().trim().contains("Max. Tenure")) {
                                                                    compareWebAndJson(commonkey.toString(),
                                                                    spliteMaxTenure, str);
                                                                    } else {
                                                                    compareWebAndJson(commonkey.toString(),
                                                                    SECTION.get(fieldKey), str);
                                                                }
                                                                } else {
                                                                str = fetchValueFromJSONFor(removeBraces(string[0]))
                                                                + fetchValueFromJSONFor(removeBraces(string[1]));
                                                                compareWebAndJson(commonkey.toString(),
                                                                SECTION.get(fieldKey), str);
                                                            }
                                                            } else {
                                                            if (channelName.equals("auto-loans")) {
                                                                LOGGER.info("fieldKey not } { " + fieldKey.toString());
                                                                LOGGER.info("SECTION.get(fieldKey) } {"
                                                                + SECTION.get(fieldKey));
                                                                LOGGER.info("commonkey.toString() } {"
                                                                + commonkey.toString());
                                                                String spliteMaxTenure = SECTION.get(fieldKey).split(
                                                                " ")[0].trim();
                                                                String str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray
                                                                .get(commonkey))).get(common)).get(fieldKey)));
                                                                if (fieldKey.toString().trim().contains("Max. Tenure")) {
                                                                    if (SECTION.containsKey(fieldKey)) {
                                                                        compareWebAndJson(commonkey.toString(),
                                                                        spliteMaxTenure, str);
                                                                    }
                                                                } else if (fieldKey.toString().trim()
                                                                .contains("Monthly Interest Rate")) {
                                                                    double doubleValue = Double.parseDouble(str);
                                                                    double doubleValueDiv = (doubleValue / 12);
                                                                    LOGGER.info("doubleValueDiv : " + doubleValueDiv);
                                                                    // double
                                                                    // finalValue
                                                                    // =
                                                                    // Math.round(doubleValueDiv*100.0)/100.0;
                                                                    LOGGER.info("finalValue (Math.round) : "
                                                                    + doubleValueDiv);
                                                                    /*
                                                                    * DecimalFormat
                                                                    * df = new
                                                                    * DecimalFormat
                                                                    * ("#.##");
                                                                    * LOGGER
                                                                    * .info(
                                                                    * "kilobytes (DecimalFormat) : "
                                                                    * +
                                                                    * df.format
                                                                    * (
                                                                    * doubleValueDiv
                                                                    * ));
                                                                    * String
                                                                    * strValue
                                                                    * =
                                                                    * df.format
                                                                    * (
                                                                    * doubleValueDiv
                                                                    * ); double
                                                                    * dValue =
                                                                    * Double
                                                                    * .valueOf
                                                                    * (strValue
                                                                    * );
                                                                    * LOGGER.
                                                                    * info
                                                                    * ("Interest "
                                                                    * +dValue);
                                                                    * dValue =
                                                                    * dValue -
                                                                    * (0.01);
                                                                    * str =
                                                                    * String
                                                                    * .valueOf
                                                                    * (dValue);
                                                                    *
                                                                    * LOGGER.info
                                                                    * (
                                                                    * "commonkey5_Monthly Interest "
                                                                    * +str);
                                                                    */
                                                                    if (spliteMaxTenure.contains(".")) {
                                                                        spliteMaxTenure = spliteMaxTenure.replace(".",
                                                                        "");
                                                                    }
                                                                    str = String.valueOf(doubleValueDiv);
                                                                    if (str.contains(".")) {
                                                                        str = str.replace(".", "");
                                                                    }
                                                                    spliteMaxTenure = spliteMaxTenure.substring(0, 2);
                                                                    str = str.substring(0, 2);
                                                                    LOGGER.info("commonkey5_Monthly Interest " + str
                                                                    + "------WEB----" + spliteMaxTenure);
                                                                    if (SECTION.containsKey(fieldKey)) {
                                                                        compareWebAndJson(commonkey.toString(),
                                                                        spliteMaxTenure, str);
                                                                    }
                                                                    } else {
                                                                    if (SECTION.containsKey(fieldKey)) {
                                                                        compareWebAndJson(commonkey.toString(),
                                                                        SECTION.get(fieldKey), str);
                                                                    }
                                                                }
                                                                } else {
                                                                String str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray
                                                                .get(commonkey))).get(common)).get(fieldKey)));
                                                                if (SECTION.containsKey(fieldKey)) {
                                                                    compareWebAndJson(commonkey.toString(),
                                                                    SECTION.get(fieldKey), str);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                webElementCoutn++;
            }
            resultMap.put(TEST_RESULT.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResultFromExcel);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualFilterFailureResult);
            resultMap.put(TEST_RESULT.R_DESRIPTION, testDescriptionExcel);
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeOrNotExcel);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, isAllDetailsPassed);
            resultMap.put(TEST_RESULT.R_MESSAGE, actualFilterFailureResult);
            testContext.setAttribute(TestMethodName, resultMap);
            if (!isAllDetailsPassed) {
                Assert.fail(actualFilterFailureResult);
            }
            } catch (Exception e) {
            resultMap.put(TEST_RESULT.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
            resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
            resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeOrNotExcel);
            resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expectedResultExcel);
            resultMap.put(TEST_RESULT.R_DESRIPTION, testDescriptionExcel);
            resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualFilterFailureResult);
            testContext.setAttribute(TestMethodName, resultMap);
            LOGGER.info("Failure method name = " + resultMap.get(TEST_RESULT.R_METHOD_NAME));
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            Assert.fail(e.toString());
            // TODO Auto-generated catch block
        }
    }
    private String getAutoLoanCommonValue(String commonkey, String fieldKey, Map commonfieldarray, Object common)
    throws Exception {
        // TODO Auto-generated method stub
        String str = "";
        if (commonkey.toString().contains("Repayment Summary")) {
            if (fieldKey.equals("Max. Tenure")) {
                str = fetchValueFromJSONFor("product.features.max_tenure");
                LOGGER.info("commonkey5_split " + str);
                str = str.split(" ")[0].trim();
                LOGGER.info("commonkey5_split " + str);
                } else if (fieldKey.equals("Interest Rate (per annum)")) {
                str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray.get(commonkey)))
                .get(common)).get(fieldKey)));
                LOGGER.info("commonkey5_Inrest " + str);
                } else if (fieldKey.equals("Monthly Interest Rate")) {
                str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray.get(commonkey)))
                .get(common)).get(fieldKey)));
                double doubleValue = Double.parseDouble(str);
                double doubleValueDiv = (doubleValue / 12);
                LOGGER.info("doubleValueDiv : " + doubleValueDiv);
                double finalValue = Math.round(doubleValueDiv * 100.0) / 100.0;
                LOGGER.info("finalValue (Math.round) : " + finalValue);
                DecimalFormat df = new DecimalFormat("#.##");
                LOGGER.info("kilobytes (DecimalFormat) : " + df.format(finalValue));
                str = df.format(finalValue);
                LOGGER.info("commonkey5_Monthly Interest " + str);
            }
        }
        return str.trim();
    }
    private String getUnSecuredLoanValue(String commonkey, String fieldKey) throws Exception {
        String str = "";
        if (commonkey.toString().contains("Penalties & Late Fees")) {
            if (fieldKey.equals("Late Payment Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.late_payment_fee"));
                } else if (fieldKey.equals("Other Fees")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.cancellation_fee"));
            }
            } else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.citizenship"));
                if (str.equals("1")) {
                    str = str.replace("1", "Citizen").toString().trim();
                    } else if (str.equals("0")) {
                    str = str.replace("0", "Any").toString().trim();
                    } else if (str.equals("2")) {
                    str = str.replace("2", "Foreigner").toString().trim();
                }
                } else if (fieldKey.equals("Employment Status")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.employment"));
                if (str.equals("1")) {
                    str = str.replace("1", "Salaried Employed").toString().trim();
                    } else if (str.equals("0")) {
                    str = str.replace("0", "Any").toString().trim();
                    } else if (str.equals("2")) {
                    str = str.replace("2", "Self Employee").toString().trim();
                    } else if (str.equals("3")) {
                    str = str.replace("3", "Professional").toString().trim();
                }
                } else if (fieldKey.equals("Age Requirement")) {
                String ageminstr = fetchValueFromJSONFor("product.requirements.age_min");
                String agemaxstr = fetchValueFromJSONFor("product.requirements.age_max");
                str = ageminstr + " - " + agemaxstr + " years old";
                LOGGER.info("age_min " + str);
            }
            else if (fieldKey.equals("KTP/ID Required?")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.doc_ktp"));
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                } else if (fieldKey.equals("Salary Slip Required?")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.doc_pay_slip"));
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                } else if (fieldKey.equals("Others Requirement")) {
                str = fetchValueFromJSONFor(removeBraces("product.requirements.others_requirement"));
            }
            } else if (commonkey.toString().contains("One-time Fees & Charges")) {
            if (fieldKey.equals("Admin Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.admin_fee"));
                } else if (fieldKey.equals("Early Settlement Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.early_settlement_fee"));
            }
            } else if (commonkey.toString().contains("Key Product Features")) {
            if (fieldKey.equals("Minimum Loan Amount")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.min_loan_amount_txt"));
                } else if (fieldKey.equals("Maximum Loan Amount")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.max_loan_amount_txt"));
                } else if (fieldKey.equals("Maximum Loan Tenure")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.max_loan_tenure_txt"));
                } else if (fieldKey.equals("Interest Rate")) {
                str = fetchValueFromJSONFor(removeBraces("product.calculation_result.apr"));
                } else if (fieldKey.equals("Approval Duration")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.approval_duration.value"));
                } else if (fieldKey.equals("Minimum Monthly Income")) {
                str = fetchValueFromJSONFor(removeBraces("product.specifications.min_annual_income"));
            }
        }
        return str.trim();
    }
    private String getAutoLoanValue(String commonkey, String fieldKey) throws Exception {
        // TODO Auto-generated method stub
        String str = "";
        if (commonkey.toString().contains("Penalties & Late Fees")) {
            if (fieldKey.equals("Admin Fee")) {
                str = fetchValueFromJSONFor("product.features.admin_fee");
                LOGGER.info("Admin Fee " + str);
                } else if (fieldKey.equals("Other Fees")) {
                str = fetchValueFromJSONFor("product.features.other_fee");
                LOGGER.info("Other Fees " + str);
                } else if (fieldKey.equals("Late Fee")) {
                str = fetchValueFromJSONFor("product.features.late_fee");
                LOGGER.info("Late Fee " + str);
                } else if (fieldKey.equals("Early Settlement Fee")) {
                str = fetchValueFromJSONFor("product.features.early_settlement_fee");
                LOGGER.info("Early Settlement Fee " + str);
            }
            } else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor("product.requirements.citizen_only");
                LOGGER.info("citizen_only " + str);
                } else if (fieldKey.equals("Minimum Age")) {
                str = fetchValueFromJSONFor("product.requirements.age_min");
                LOGGER.info("age_min " + str);
                } else if (fieldKey.equals("Maximum Age")) {
                str = fetchValueFromJSONFor("product.requirements.age_max");
                LOGGER.info("age_max " + str);
                } else if (fieldKey.equals("Other Requirements")) {
                str = fetchValueFromJSONFor("product.requirements.other_requirement");
                LOGGER.info("other_requirement " + str);
                } else if (fieldKey.equals("Employment Status")) {
                str = fetchValueFromJSONFor("product.requirements.employment");
                LOGGER.info("employement " + str);
                } else if (fieldKey.equals("KTP/ID Required?")) {
                str = fetchValueFromJSONFor("product.requirements.doc_ktp");
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                LOGGER.info("doc_pay " + str);
                } else if (fieldKey.equals("Salary Slip Required?")) {
                str = fetchValueFromJSONFor("product.requirements.doc_pay_slip");
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                LOGGER.info("doc_pay " + str);
                } else if (fieldKey.equals("Coverage Area?")) {
                str = fetchValueFromJSONFor("requirements.coverage_area");
                LOGGER.info("doc_pay " + str);
            }
        }
        return str.trim();
    }
    private String getMultiLoanValue(String commonkey, String fieldKey) throws Exception {
        // TODO Auto-generated method stub
        String str = "";
        if (commonkey.toString().contains("Accelerated Repayment")) {
            if (fieldKey.equals("Admin Fee")) {
                str = fetchValueFromJSONFor("product.features.admin_fee");
                LOGGER.info("Admin Fee " + str);
                } else if (fieldKey.equals("Other Fees")) {
                str = fetchValueFromJSONFor("product.features.other_fee");
                LOGGER.info("Other Fees " + str);
                } else if (fieldKey.equals("Late Fee")) {
                str = fetchValueFromJSONFor("product.features.late_fee");
                LOGGER.info("Late Fee " + str);
                } else if (fieldKey.equals("Early Settlement Fee")) {
                str = fetchValueFromJSONFor("product.features.early_settlement_fee");
                if (str.isEmpty() || str == null || str.equals("null"))
                str = str.replace("null", "n/a").toString().trim();
                LOGGER.info("Early Settlement Fee " + str);
            }
            } else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor("product.requirements.citizen_only");
                LOGGER.info("citizen_only " + str);
                } else if (fieldKey.equals("Age Requirement")) {
                String ageminstr = fetchValueFromJSONFor("product.requirements.age_min");
                String agemaxstr = fetchValueFromJSONFor("product.requirements.age_max");
                str = ageminstr + " to " + agemaxstr + " years";
                LOGGER.info("age_min " + str);
                } else if (fieldKey.equals("Other Requirements")) {
                str = fetchValueFromJSONFor("product.requirements.other_requirement");
                LOGGER.info("other_requirement " + str);
                } else if (fieldKey.equals("Minimum Income")) {
                str = fetchValueFromJSONFor("product.requirements.income_min");
                if (str.equals("null"))
                str = str.replace("null", "0").toString().trim();
                LOGGER.info("Income-Minimum " + str);
                } else if (fieldKey.equals("Collateral Type")) {
                str = fetchValueFromJSONFor("product.features.collateral_type");
                LOGGER.info("Collateral " + str);
                } else if (fieldKey.equals("Employment Status")) {
                str = fetchValueFromJSONFor("product.requirements.employment");
                LOGGER.info("employement " + str);
                } else if (fieldKey.equals("KTP/ID Required?")) {
                str = fetchValueFromJSONFor("product.requirements.doc_ktp");
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                LOGGER.info("doc_ktp " + str);
                } else if (fieldKey.equals("Salary Slip Required?")) {
                str = fetchValueFromJSONFor("product.requirements.doc_pay_slip");
                if (str.equals("1")) {
                    str = str.replace("1", "Required").toString().trim();
                    } else {
                    str = str.replace("0", "Not Required").toString().trim();
                }
                LOGGER.info("doc_pay " + str);
            }
            } else if (commonkey.toString().contains("Key Product Features")) {
            if (fieldKey.equals("Minimum Loan Amount")) {
                str = fetchValueFromJSONFor("product.features.min_loan_amount");
                LOGGER.info("product.features.min_loan_amount " + str);
                } else if (fieldKey.equals("Maximum Loan Amount")) {
                str = fetchValueFromJSONFor("product.features.max_loan_amount");
                LOGGER.info("product.features.max_loan_amount " + str);
                } else if (fieldKey.equals("Minimum Loan Tenure")) {
                str = fetchValueFromJSONFor("product.features.min_loan_tenure");
                LOGGER.info("product.features.min_loan_tenure " + str);
                } else if (fieldKey.equals("Maximum Loan Tenure")) {
                str = fetchValueFromJSONFor("product.features.max_loan_tenure");
                LOGGER.info("Maximum Loan Tenure " + str);
                } else if (fieldKey.equals("Estimated Process Time")) {
                str = fetchValueFromJSONFor("product.features.estimated_process_time");
                LOGGER.info("product.features.estimated_process_time " + str);
                } else if (fieldKey.equals("Usage Type")) {
                str = fetchValueFromJSONFor("product.calculation_result.params.loan_purpose");
                LOGGER.info("Usage Type " + str);
            }
        }
        return str.trim();
    }
    private String getSavingLoanValue(String commonkey, String fieldKey) throws Exception {
        // TODO Auto-generated method stub
        String str = "";
        if (commonkey.toString().contains("Fees and Charges")) {
            if (fieldKey.equals("Min. Ave. Monthly Balance")) {
                str = fetchValueFromJSONFor("product.features.min_avg_monthly_bal");
                LOGGER.info("min_avg_monthly_bal " + str);
                } else if (fieldKey.equals("Fall Below Fee")) {
                str = fetchValueFromJSONFor("product.features.fall_below_fee");
                LOGGER.info("fall_below_fee" + str);
                } else if (fieldKey.equals("Monthly Account Fee")) {
                str = fetchValueFromJSONFor("product.features.monthly_account_fee");
                LOGGER.info("monthly_account_fee " + str);
                } else if (fieldKey.equals("ATM Withdrawal Charges")) {
                str = fetchValueFromJSONFor("product.features.atm_withdrawal_charges");
                LOGGER.info("atm_withdrawal_charges " + str);
                } else if (fieldKey.equals("Cash Deposit Fee")) {
                str = fetchValueFromJSONFor("product.features.cash_deposit_fee");
                LOGGER.info("monthly_account_fee " + str);
                } else if (fieldKey.equals("Counter Withdrawal Fee")) {
                str = fetchValueFromJSONFor("product.features.counter_withdrawal_fee");
                LOGGER.info("counter_withdrawal_fee " + str);
                } else if (fieldKey.equals("Passbook Replacement Fee")) {
                str = fetchValueFromJSONFor("product.features.passbook_replacement_fee");
                LOGGER.info("passbook_replacement_fee " + str);
                } else if (fieldKey.equals("Incidental Overdraft Charge")) {
                str = fetchValueFromJSONFor("product.features.incidental_overdraft_charge");
                LOGGER.info("incidental_overdraft_charge " + str);
                } else if (fieldKey.equals("Other Fees(if any)")) {
                str = fetchValueFromJSONFor("product.features.other_fees");
                LOGGER.info("other_fees " + str);
            }
            } else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor("product.features.nationality");
                LOGGER.info("citizen_only " + str);
                } else if (fieldKey.equals("Minimum Initial Deposit")) {
                str = fetchValueFromJSONFor("product.features.initial_deposit");
                LOGGER.info("age_min " + str);
            }
            } else if (commonkey.toString().contains("Other Features")) {
            if (fieldKey.equals("No. of ATMs Available")) {
                str = fetchValueFromJSONFor("product.features.no_of_atms_available");
                LOGGER.info("no_of_atms_available " + str);
                } else if (fieldKey.equals("Online Banking")) {
                str = fetchValueFromJSONFor("product.features.online_banking");
                LOGGER.info("online_banking " + str);
            }
        }
        return str.trim();
    }
    private String fetchValueFromJSONFor(String keyFromYAML) throws Exception {
        // TODO Auto-generated method stub
        String[] splittednode = keyFromYAML.split("\\.");
        // Object value = com.model.Constants.JSONStore.OutputJson;
        LOGGER.info("keyFromYAML fetchValueFromJSONFor : " + keyFromYAML);
        String individualproductJson = Singleproductjson();
        JSONObject singleProduct = new JSONObject(individualproductJson);
        ExcelInputData excelInput = ExcelInputData.getInstance();
        Object value = singleProduct;
        for (int i = 0; i < (splittednode.length); i++) {
        }
        for (int i = 0; i < (splittednode.length - 1); i++) {
            LOGGER.info("splittednode:" + splittednode);
            LOGGER.info("splittednode length:" + splittednode.length);
            if (value instanceof JSONObject) {
                if (((JSONObject) value).has("premium")) {
                    Object premiumObject = (Object) ((JSONObject) value).get("premium");
                    if (premiumObject instanceof JSONObject) {
                        JSONObject premiumProduct = ((JSONObject) value).getJSONObject("premium");
                        value = premiumProduct.getJSONObject("sponsored");
                        } else {
                        value = ((JSONObject) value).get(splittednode[i]);
                    }
                    } else {
                    value = ((JSONObject) value).get(splittednode[i]);
                    if (value instanceof JSONObject) {
                        //
                        } else {
                        if (splittednode[i].equals("features") || splittednode[i].equals("requirements")) {
                            JSONArray temp = (JSONArray) value;
                            for (int j = 0; j < temp.length(); j++) {
                                if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                                splittednode[i + 1])) {
                                    value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                                    if (((JSONObject) value).getString("value").equals(null)
                                    || ((JSONObject) value).getString("value").equals("null")
                                    || ((JSONObject) value).getString("value").isEmpty()) {
                                        splittednode[i + 1] = "value_str";
                                        LOGGER.info("value_str 123456:" + splittednode[0] + splittednode[1]
                                        + splittednode[2]);
                                        } else {
                                        splittednode[i + 1] = "value";
                                        LOGGER.info("value 12345678:" + splittednode[0] + splittednode[1]
                                        + splittednode[2]);
                                    }
                                }
                            }
                            } else {
                            value = ((JSONObject) (((JSONArray) value).get(0))).get(splittednode[i]);
                        }
                    }
                }
                } else {
                LOGGER.info("feature or requirement:" + splittednode[i]);
                if (splittednode[i].equals("features") || splittednode[i].equals("requirements")) {
                    value = (JSONArray) ((JSONObject) (((JSONArray) value).get(0))).getJSONArray(splittednode[i]);
                    JSONArray temp = (JSONArray) value;
                    for (int j = 0; j < temp.length(); j++) {
                        if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                        splittednode[i + 1])) {
                            value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                            if (((JSONObject) value).getString("value").equals(null)
                            || ((JSONObject) value).getString("value").equals("null")
                            || ((JSONObject) value).getString("value").isEmpty()) {
                                splittednode[i + 1] = "value_str";
                                LOGGER.info("value_str 123456:" + splittednode[0] + splittednode[1] + splittednode[2]);
                                } else {
                                splittednode[i + 1] = "value";
                                LOGGER.info("value 12345678:" + splittednode[0] + splittednode[1] + splittednode[2]);
                            }
                        }
                    }
                    } else {
                    value = ((JSONObject) (((JSONArray) value).get(0))).get(splittednode[i]);
                }
            }
        }
        String str = "";
        if (value instanceof JSONObject) {
            if (((JSONObject) value).get(splittednode[splittednode.length - 1]) != null) {
                cururl = getWebDriver().getCurrentUrl();
                if (splittednode[splittednode.length - 1].toString().equals("max_loan_tenure")
                && cururl.contains("renovation")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal) + " years";
                    System.out.print("calculation str" + str);
                } else if (splittednode[splittednode.length - 1].toString().equals("max_loan_tenure")
                && cururl.contains("education")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal) + " years";
                    System.out.print("caxlculation str" + str);
                } else if (splittednode[splittednode.length - 1].toString().equals("min_annual_income")
                && cururl.contains("www.duitpintar.com/en_sg/unsecured-loans")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal);
                    System.out.print("min_annual_income str" + str);
                } else if (splittednode[splittednode.length - 1].toString().equals("max_loan_tenure")
                && cururl.contains("http://www.moneysmart.sg/personal-loan")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal) + " years";
                    System.out.print("caxlculation str" + str);
                    } else {
                    str = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                }
                } else {
                LOGGER.info("value = null");
            }
            } else if (value instanceof JSONArray) {
            JSONArray temp = (JSONArray) value;
            for (int j = 0; j < temp.length(); j++) {
                LOGGER.info("splittednode[splittednode.length-1] = " + splittednode[splittednode.length - 1]);
                if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                splittednode[splittednode.length - 1])) {
                    value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                    if (((JSONObject) value).getString("value").equals(null)
                    || ((JSONObject) value).getString("value").equals("null")
                    || ((JSONObject) value).getString("value").isEmpty()) {
                        str = ((JSONObject) value).getString("value");
                        } else {
                        str = ((JSONObject) value).getString("value_str");
                    }
                }
            }
        }
        GetExcelInput getInput = new GetExcelInput();
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
        if (channelName.equals("auto-loans")) {
            if (str.isEmpty() || str.length() == 0 || str == null) {
                str = "n/a";
            }
            } else if (channelName.equals("multi-purpose-loan")) {
            LOGGER.info("-before " + channelName);
            LOGGER.info("-before-str " + str);
            if (str.isEmpty() || str == null || str.equals("null")) {
                if (str.equals("null") || str.equals("0")) {
                    str = "n/a";
                    LOGGER.info("-after " + str);
                    } else {
                    str = "n/a";
                }
                LOGGER.info("-after " + str);
            }
            } else if (channelName.equals("Savings Account")) {
            LOGGER.info("Savings Account-before " + channelName);
            LOGGER.info("Savings Account-before-str " + str);
            if (str.isEmpty() || str == null || str.equals("null")) {
                if (str.equals("null") || str.equals("0")) {
                    str = "0";
                    LOGGER.info("Savings Account-after " + str);
                    } else {
                    str = "n/a";
                }
                LOGGER.info("-after " + str);
            }
        } else if (channelName.equals("personal-loan")
        && cururl.contains("https://www.duitpintar.com/en_sg/unsecured-loans")) {
            LOGGER.info("unsecured-loans -before " + channelName);
            LOGGER.info("unsecured-loans -before-str " + str);
            if (str.isEmpty() || str == null || str.equals("null")) {
                if (str.equals("null") || str.equals("0")) {
                    str = "0";
                    LOGGER.info("unsecured-loans -after " + str);
                    } else {
                    str = "None";
                }
                LOGGER.info("-after " + str);
            }
        }
        return str;
    }
    private void compareWebAndJson(String sectionName, String valuefromwebsite, String valuefromapi) {
        LOGGER.info("valuefromwebsite: before " + valuefromwebsite);
        String finalvaluefromwebsite = removeBraces(valuefromwebsite);
        String finalvaluefromapi = removeBraces(valuefromapi);
        LOGGER.info("valuefromwebsite: " + finalvaluefromwebsite);
        LOGGER.info("valuefromapi: " + valuefromapi);
        LOGGER.info("finalvaluefromapi: " + finalvaluefromapi);
        if (finalvaluefromapi.trim().equals(finalvaluefromwebsite.trim())) {
            LOGGER.info("validation passed");
            } else {
            LOGGER.info("validation failed");
            isAllDetailsPassed = false;
            if (actualFilterFailureResult.length() > 0) {
                actualFilterFailureResult = actualFilterFailureResult + " , " + "SECTION NAME : " + sectionName
                + " VALIDATION : FAILED for ( Api Value = " + finalvaluefromapi + ", Web Value = "
                + finalvaluefromwebsite + ")";
                } else {
                actualFilterFailureResult = "SECTION NAME : " + sectionName
                + " VALIDATION : FAILED for  ( Api Value = " + finalvaluefromapi + ", Web Value = "
                + finalvaluefromwebsite + ")";
            }
        }
    }
    private String removeBraces(String string) {
        // TODO Auto-generated method stub
        if (string.contains("(")) {
            string = string.replace("(", "{").toString().trim();
            }
            if (string.contains(")")) {
            string = string.replace(")", "}").toString().trim();
        }
        if (string.contains("{")) {
                string = string.replace("{", "").trim();
                }
                if (string.contains("reformat_")) {
                    string = string.replaceAll("reformat_", "").toString().trim();
                }
                if (string.contains("<ul>")) {
                    string = string.replaceAll("<ul>", "").toString().trim();
                }
                if (string.contains("<br>")) {
                    LOGGER.info("string: " + string);
                    String str1 = (string.split("<br>")[0]).trim();
                    String str2 = (string.split("<br>")[1]).trim();
                    LOGGER.info("string: line-1 " + str1 + " line-2 " + str2);
                    string = str1 + "n" + str2 + "n";
                }
                if (string.contains("</ul>")) {
                    string = string.replaceAll("</ul>", "").toString().trim();
                }
                if (string.contains("<li>")) {
                    string = string.replaceAll("<li>", "").toString().trim();
                }
                if (string.contains("</li>")) {
                    string = string.replaceAll("</li>", "").toString().trim();
                }
                if (string.contains("<span>")) {
                    string = string.replaceAll("<span>", "").toString().trim();
                }
                if (string.contains("</span>")) {
                    string = string.replaceAll("</span>", "").toString().trim();
                    LOGGER.info("two lines :111222 " + string);
                    String str1 = (string.split("\\.")[0]).trim();
                    String str2 = (string.split("\\.")[1]).trim();
                    string = str1 + "." + "n" + str2;
                    LOGGER.info("two lines : " + string);
                }
                if (string.contains(".array_value.")) {
                    string = string.replaceAll(".array_value.", ".").toString().trim();
                }
                if (string.contains("_txt")) {
                    string = string.replaceAll("_txt", "").toString().trim();
                }
                if (string.contains("loan_tenure_units")) {
                    string = string.replaceAll("loan_tenure_units", "loan_tenure_unit").toString().trim();
                }
                if (string.contains("is defined ?")) {
                    string = (string.split("is defined ?")[0]).trim();
                }
                if (string.contains("is defined %")) {
                    string = (string.split("is defined %")[0]).trim();
                }
                if (string.contains(".value")) {
                    string = (string.split(".value")[0]).trim().toString();
                    LOGGER.info("string .value removed : " + string);
                }
                if (string.contains("'months'")) {
                    string = (string.split("'months'")[0]).trim().toString();
                    string = string.replaceAll("\\s*$", "").replaceAll("^\\s*", "");
                    LOGGER.info("after string 'months' removed : " + string);
                }
                if (string.contains("apr/12")) {
                    LOGGER.info("INSIDE : " + string);
                    string = (string.split("/12")[0]).trim().toString();
                    LOGGER.info("apr/12 : " + string);
                }
                if (string.contains("% if")) {
                    string = (string.split("% if")[1]).trim().toString();
                    string = (string.split("is empty %")[0]).trim().toString();
                    string = string.replaceAll("\\s*$", "").replaceAll("^\\s*", "");
                    LOGGER.info("is empty : " + string);
                }
                if (string.contains("IDR")) {
                    string = (string.split("IDR")[1]).trim().toString();
                    LOGGER.info("IDR : " + string);
                }
                if (string.contains("months")) {
                    string = (string.split("months")[0]).trim().toString();
                    LOGGER.info("months : " + string);
                }
                if (string.contains("Permanent")) {
                    string = (string.split("Permanent")[0]).trim().toString();
                    LOGGER.info("Permanent : " + string);
                }
                if (string.contains("}")) {
            string = string.replace("}", "").trim();
        }
        if (string.contains("|")) {
            string = (string.split("\\|")[0]).trim();
        }
        if (string.contains("%")) {
            string = string.replace("%", "").trim();
        }
        if (string.contains(",")) {
            string = string.replace(",", "").trim();
        }
        if (string.contains("$")) {
            string = string.replace("$", "").trim();
        }
        if (string.contains("Years")) {
            string = string.replace("Years", "").trim();
        }
        if (string.contains("Year")) {
            string = string.replace("Year", "").trim();
        }
        return string.trim();
    }
    private void detailform(WebElement webElement, String filtercomponentType, String calculationInput)
    throws Exception {
        // method body
        if (filtercomponentType.equals("textbox")) {
            webElement.clear();
            Thread.sleep(5000);
            webElement.sendKeys(calculationInput);
            } else if (filtercomponentType.equals("selectbox")) {
            webElement.click();
            WebElement iam = getWebDriver().findElement(By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
            iam.click();
            } else if (filtercomponentType.equals("checkbox")) {
            webElement.click();
            } else if (filtercomponentType.equals("radiobutton")) {
            webElement.click();
        }
    }
    private Object getParticularProductJson(String productName) throws Exception {
        // TODO Auto-generated method stub
        return productName;
    }
    private Object getParticularProductJsonFromSponsor(String productName, JSONObject fullJsonObject) throws Exception {
        JSONObject clickedProductJson = null;
        return clickedProductJson;
    }
    private Object getParticularProductJsonFromProductJson(String productName, JSONObject fullJsonObject)
    throws Exception {
        JSONObject clickedProductJson = null;
        JSONArray productArray = fullJsonObject.getJSONArray("product");
        for (int prodcount = 0; prodcount < productArray.length(); prodcount++) {
            JSONObject singleProductJson = productArray.getJSONObject(prodcount);
            if (singleProductJson.getString("name") != null) {
                if (singleProductJson.getString("name").equals(productName)) {
                    clickedProductJson = singleProductJson;
                }
            }
        }
        return clickedProductJson;
    }
    private String getParentElementFromExcelObject(String methodName) throws Exception {
        // TODO Auto-generated method stub
        String parentElementValue = getExcelObject(methodName, "ParentElement");
        return parentElementValue;
    }
    private String getChildElementFromExcelObject(String methodName) throws Exception {
        // TODO Auto-generated method stub
        String childElementValue = getExcelObject(methodName, "ClildElement");
        return childElementValue;
    }
    private String getFinalElementFromExcelObject(String methodName) throws Exception {
        // TODO Auto-generated method stub
        String webElementValue = getExcelObject(methodName, "FinalElement");
        return webElementValue;
    }
    private String getExcelObject(String methodName, String keyName) throws Exception {
        GetExcelInput getAMethod = new GetExcelInput();
        String valueFromExcel = getAMethod.get_A_Value_Using_Key_Of_A_Method(methodName, keyName);
        return valueFromExcel;
    }
    private void prepareDriver() throws Exception {
        // TODO Auto-generated method stub
        getWebDriver().get("http://www.moneysmart.sg/personal-loan/citibank-ready-credit");
        getWebDriver().manage().window().maximize();
    }
    public WebDriver getWebDriver() throws Exception {
        ExcelInputData excelInput = ExcelInputData.getInstance();
        WebDriver webDriver = excelInput.getWebDriver();
        return webDriver;
    }
    public String Singleproductjson() {
        GetExcelInput getInput = new GetExcelInput();
        String detailPagePermaLink = getInput.get_A_Value_Using_Key_Of_A_Method(
        EXCEL_METHODS_INPUT.DetailPageProduct_MethodNameKey, EXCEL_METHODS_INPUT.DetailPagePermaLink);
        LOGGER.info("detailPagePermaLink " + detailPagePermaLink);
        String apiurl = Constants.MYSTORE.SINGLEPRODUCTJSONAPI;
        String permalink = "&permalink=" + detailPagePermaLink;
        String permaurl = apiurl + permalink;
        LOGGER.info("perma : " + permaurl);
        WebserviceRequest webserviceRequest = new WebserviceRequest();
        webserviceListener = this;
        LOGGER.info("Webservice Request : " + permaurl);
        String jsonResult = webserviceRequest.GET(webserviceListener, permaurl);
        if (jsonResult.isEmpty() || jsonResult == null) {
            LOGGER.info("Result is empty or null");
        }
        return jsonResult;
    }
    @Override
    public void webserviceRequestSuccessListener(int statusCode, String statusMessage, String apiResult) {
        // TODO Auto-generated method stub
    }
    @Override
    public void webserviceRequestFailureListener(int statusCode, String statusMessage) {
        // TODO Auto-generated method stub
    }
}