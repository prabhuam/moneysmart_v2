package com.moneysmart.main;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import jxl.read.biff.BiffException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.model.CommonMethods;
import com.model.ExcelCalculationInputData;
import com.model.ExcelFilterInputData;
import com.model.ExcelIconInputData;
import com.model.ExcelInputData;
import com.model.ExcelProductValidationInputData;
import com.model.ExcelReader;
import com.model.ExcelTestimonialData;
import com.model.GetExcelInput;
import com.model.MethodInputData;
import com.model.Constants.TEST_RESULT;
import com.model.Constants.EXCEL_METHODS_INPUT;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ReadExcelInput {
    /*
    * Change the below two values according to your needs
    */
    private static final Logger LOGGER = Logger.getLogger(ReadExcelInput.class.getName());
    public void readXLSFile(String excelPath, String excelSheetName, String stopReadingExcelUptoCalculationData,
    String stoptReadingExcelIfCalculationData, String startReadingFilterMethods,
    String stopReadingFilterMethods, String startReadingExcelProductValidationData,
    String stoptReadingExcelProductValidationData) throws IOException {
        InputStream ExcelFileToRead = new FileInputStream(excelPath);
        @SuppressWarnings("resource")
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
        HSSFSheet sheet = wb.getSheet(excelSheetName);
        HSSFRow row;
        // HSSFCell cell;
        Iterator<?> rows = sheet.rowIterator();
        ArrayList<String> testMethodNameArray = getMethodNamesInIndexZero(excelPath, excelSheetName,
        stopReadingExcelUptoCalculationData);
        String previousTestimonialMethodName = "";
        String FinalTesttimonialMethodName = "";
        String previousIconMethodName = "";
        String FinalIconMethodName = "";
        String FinalTestMethodName = "";
        String FinalTestMethodExecute = "";
        ExcelInputData excelInputData = ExcelInputData.getInstance();
        HashMap<String, ExcelTestimonialData> testimonialNameAndValue = new HashMap<String, ExcelTestimonialData>();
        HashMap<String, ExcelIconInputData> iconNameAndValue = new HashMap<String, ExcelIconInputData>();
        ExcelTestimonialData testimonialInputData = new ExcelTestimonialData();
        ExcelIconInputData iconInputData = new ExcelIconInputData();
        boolean isReachedCalculationData = false;
        int calculationDataRowCount = 0;
        int calculationEndDataRowCount = 0;
        int filtersStartDataRowCount = 0;
        int filtersEndDataRowCount = 0;
        int productValidationStartDataRowCount = 0;
        int productValidationEndDataRowCount = 0;
        while (rows.hasNext()) {
            row = (HSSFRow) rows.next();
            Iterator cells = row.cellIterator();
            String TestMethodName = "";
            String previousMethodName = "";
            String cellKeyValue = "";
            String TestMethodExecute = "";
            outer: while (cells.hasNext()) {
                HSSFCell cell = (HSSFCell) cells.next();
                MethodInputData methodInputData = new MethodInputData();
                ArrayList<String> allTestimonialKeysWithDuplicateName = new ArrayList<String>();
                // will iterate over the Merged cells
                for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
                    CellRangeAddress region = sheet.getMergedRegion(i); // Region
                    // of
                    // merged
                    // cells
                    int colIndex = region.getFirstColumn(); // number of columns
                    // merged
                    int rowNum = region.getFirstRow(); // number of rows merged
                    // check first cell of the region
                    if (rowNum == cell.getRowIndex() && colIndex == cell.getColumnIndex()) {
                        if (colIndex == 0) {
                            TestMethodName = sheet.getRow(rowNum).getCell(colIndex).getStringCellValue();
                            if (!previousMethodName.equals(TestMethodName)) {
                                previousMethodName = TestMethodName;
                            }
                        }
                        if (colIndex == 1) {
                            TestMethodExecute = sheet.getRow(rowNum).getCell(colIndex).getStringCellValue();
                        }
                        continue outer;
                    }
                }
                // the data in merge cells is always present on the first cell.
                // All other cells(in merged region) are considered blank
                if (cell.getCellType() == HSSFCell.CELL_TYPE_BLANK || cell == null) {
                    continue;
                }
                /*
                * Read all methods except calculation data, testimonial data
                * and icons data
                */
                if (row.getRowNum() > 0) {
                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        /*
                        * Count of Calculation End Data
                        */
                        if (cell.getStringCellValue().contains(stoptReadingExcelIfCalculationData)) {
                            calculationEndDataRowCount = cell.getRowIndex();
                        }
                        /*
                        * Count of Filter Start Data
                        */
                        if (cell.getStringCellValue().equals(startReadingFilterMethods)) {
                            filtersStartDataRowCount = cell.getRowIndex();
                        }
                        /*
                        * Count of Filter End Data
                        */
                        if (cell.getStringCellValue().contains(stopReadingFilterMethods)) {
                            filtersEndDataRowCount = cell.getRowIndex();
                        }
                        /*
                        * Count of Product Validation Start Data
                        */
                        if (cell.getStringCellValue().equals(startReadingExcelProductValidationData)) {
                            productValidationStartDataRowCount = cell.getRowIndex();
                        }
                        /*
                        * Count of Product Validation End Data
                        */
                        if (cell.getStringCellValue().contains(stoptReadingExcelProductValidationData)) {
                            productValidationEndDataRowCount = cell.getRowIndex();
                        }
                    }
                    if (!isReachedCalculationData) {
                        if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                            if (cell.getStringCellValue().contains(stopReadingExcelUptoCalculationData)) {
                                calculationDataRowCount = cell.getRowIndex();
                                isReachedCalculationData = true;
                            }
                            if (cell.getColumnIndex() == 2) {
                                cellKeyValue = cell.getStringCellValue();
                            }
                            if (cell.getColumnIndex() == 3) {
                                for (int testMethodCount = 0; testMethodCount < testMethodNameArray.size(); testMethodCount++) {
                                    if (previousMethodName.contains(testMethodNameArray.get(testMethodCount))) {
                                        FinalTestMethodExecute = TestMethodExecute;
                                        FinalTestMethodName = testMethodNameArray.get(testMethodCount);
                                    }
                                }
                                /*
                                * Set single method here...
                                */
                                methodInputData.setMethodInput(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT,
                                FinalTestMethodExecute);
                                methodInputData.setMethodInput("" + cellKeyValue, "" + cell.getStringCellValue());
                                /*
                                * Set single testimonial method here...
                                */
                                if (FinalTestMethodName.contains(TEST_RESULT.R_TESTIMONIAL)) {
                                    for (int testMethodCount = 0; testMethodCount < testMethodNameArray.size(); testMethodCount++) {
                                        if (previousMethodName.contains(testMethodNameArray.get(testMethodCount))) {
                                            FinalTesttimonialMethodName = testMethodNameArray.get(testMethodCount);
                                        }
                                    }
                                    testimonialInputData.setMethodInput(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT,
                                    FinalTestMethodExecute);
                                    testimonialInputData.setMethodInput("" + cellKeyValue,
                                    "" + cell.getStringCellValue());
                                    testimonialNameAndValue.put(FinalTestMethodName, testimonialInputData);
                                }
                                /*
                                * Set single testimonial method here...
                                */
                                if (FinalTestMethodName.contains(TEST_RESULT.R_ICON)) {
                                    for (int testMethodCount = 0; testMethodCount < testMethodNameArray.size(); testMethodCount++) {
                                        if (previousMethodName.contains(testMethodNameArray.get(testMethodCount))) {
                                            FinalIconMethodName = testMethodNameArray.get(testMethodCount);
                                        }
                                    }
                                    iconInputData.setMethodInput(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT,
                                    FinalTestMethodExecute);
                                    iconInputData.setMethodInput("" + cellKeyValue, "" + cell.getStringCellValue());
                                    iconNameAndValue.put(FinalTestMethodName, iconInputData);
                                }
                                if (!isReachedCalculationData) {
                                    /*
                                    * Set all inputs except calculation data,
                                    * testimonial and icons
                                    */
                                    HashMap<String, MethodInputData> methodNameAndValue = new HashMap<String, MethodInputData>();
                                    methodNameAndValue.put(FinalTestMethodName, methodInputData);
                                    excelInputData.setExcelInput(methodNameAndValue);
                                    /*
                                    * Set testimonial input
                                    */
                                    if (FinalTestMethodName.contains(TEST_RESULT.R_TESTIMONIAL)) {
                                        if (!previousTestimonialMethodName.equals(FinalTestMethodName)) {
                                            previousTestimonialMethodName = FinalTestMethodName;
                                            excelInputData.setExcelTestimonialInputData(testimonialNameAndValue);
                                            testimonialInputData = new ExcelTestimonialData();
                                            } else {
                                        }
                                    }
                                    /*
                                    * Set three icon input
                                    */
                                    if (FinalTestMethodName.contains(TEST_RESULT.R_ICON)) {
                                        if (!previousIconMethodName.equals(FinalTestMethodName)) {
                                            previousIconMethodName = FinalTestMethodName;
                                            excelInputData.setExcelIconInputData(iconNameAndValue);
                                            iconInputData = new ExcelIconInputData();
                                            } else {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        readProductValidationMethodsAndItsInput(excelInputData, excelPath, excelSheetName, startReadingFilterMethods,
        stopReadingFilterMethods, filtersStartDataRowCount, filtersEndDataRowCount, calculationDataRowCount,
        calculationEndDataRowCount, startReadingExcelProductValidationData,
        stoptReadingExcelProductValidationData, productValidationStartDataRowCount,
        productValidationEndDataRowCount);
    }
    public void readProductValidationMethodsAndItsInput(ExcelInputData excelInputData, String excelPath,
    String excelSheetName, String startReadingFilterMethods, String stopReadingFilterMethods,
    int filtersStartDataRowCount, int filtersEndDataRowCount, int calculationDataRowCount,
    int calculationEndDataRowCount, String startReadingExcelProductValidationData,
    String stoptReadingExcelProductValidationData, int productValidationStartDataRowCount,
    int productValidationEndDataRowCount) {
        ArrayList<String> productMethodNameArray = getProductValidationMethodNamesInIndexZero(excelPath,
        excelSheetName, productValidationStartDataRowCount, stoptReadingExcelProductValidationData);
        GetExcelInput getInput = new GetExcelInput();
        for (int count = 0; count < excelInputData.getExcelProductValidationInputData().size(); count++) {
            ArrayList<HashMap<String, ExcelProductValidationInputData>> newexcelInputProdcutValidationArray = excelInputData
            .getExcelProductValidationInputData();
            HashMap<String, ExcelProductValidationInputData> excelInputProductValidationMap = newexcelInputProdcutValidationArray
            .get(count);
            String productValidationMethodNameKey = (String) excelInputProductValidationMap.keySet().toArray()[count];
            LOGGER.info("n ===================== Product Validation : " + productValidationMethodNameKey
            + "=====================");
            ExcelProductValidationInputData filterInputData = excelInputProductValidationMap
            .get(productValidationMethodNameKey);
            HashMap<String, String> filterInputDataMap = filterInputData.getMethodInput();
            Set<String> keys = filterInputDataMap.keySet();
            for (String key : keys) {
                String singleKeyOfASingleFilterMethod = getInput.get_A_Value_Using_Key_Of_Filters_Method(
                productValidationMethodNameKey, key);
                LOGGER.info("n Product Validation Key :  " + key + " = " + singleKeyOfASingleFilterMethod);
            }
        }
        int filterMethodInvocationCount = productMethodNameArray.size() - 1;
        LOGGER.info("===n No of  Product Validation methods " + filterMethodInvocationCount);
        readFilterMethodsAndItsInput(excelInputData, excelPath, excelSheetName, startReadingFilterMethods,
        stopReadingFilterMethods, filtersStartDataRowCount, filtersEndDataRowCount, calculationDataRowCount,
        calculationEndDataRowCount);
    }
    @SuppressWarnings("resource")
    public ArrayList<String> getProductValidationMethodNamesInIndexZero(String excelPath, String excelSheetName,
    int productValidationStartDataRowCount, String stoptReadingExcelProductValidationData) {
        String FinalTestMethodName = "";
        String FinalTestMethodExecute = "";
        String previousIconMethodName = "";
        boolean isSecondTimeValue = false;
        ExcelInputData excelInputData = ExcelInputData.getInstance();
        HashMap<String, ExcelProductValidationInputData> excelProductValidationInputDataMap = new HashMap<String, ExcelProductValidationInputData>();
        ArrayList<String> productMethodNameArray = new ArrayList<String>();
        try {
            HSSFWorkbook wb;
            InputStream ExcelFileToRead = new FileInputStream(excelPath);
            wb = new HSSFWorkbook(ExcelFileToRead);
            HSSFSheet sheet = wb.getSheet(excelSheetName);
            HSSFRow row;
            boolean isReachedFilterData = false;
            // HSSFCell cell;
            ExcelProductValidationInputData productValidationInputData = new ExcelProductValidationInputData();
            Iterator<?> rows = sheet.rowIterator();
            while (rows.hasNext()) {
                String cellKeyValue = "";
                String TestMethodExecute = "";
                row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    if (row.getRowNum() > 0) {
                        if (!isReachedFilterData) {
                            if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING
                            || cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                String cellValueInString = "";
                                if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                    cellValueInString = String.valueOf(cell.getNumericCellValue());
                                    } else {
                                    cellValueInString = cell.getStringCellValue();
                                }
                                if (cellValueInString.contains(stoptReadingExcelProductValidationData)) {
                                    isReachedFilterData = true;
                                }
                                if (!isReachedFilterData && cell.getColumnIndex() == 0
                                && cell.getRowIndex() > productValidationStartDataRowCount) {
                                    if (cellValueInString.length() > 0) {
                                        FinalTestMethodName = "" + cellValueInString;
                                        productMethodNameArray.add("" + cellValueInString);
                                    }
                                }
                                if (cell.getColumnIndex() == 1) {
                                    TestMethodExecute = cellValueInString;
                                }
                                if (cell.getColumnIndex() == 2) {
                                    cellKeyValue = cellValueInString;
                                }
                                if (cell.getColumnIndex() == 3) {
                                    /*
                                    * Set single method here...
                                    */
                                    if (!isReachedFilterData && cell.getRowIndex() > productValidationStartDataRowCount) {
                                        if (!previousIconMethodName.equals(FinalTestMethodName)) {
                                            FinalTestMethodExecute = TestMethodExecute;
                                        }
                                        productValidationInputData.setMethodInput(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT,
                                        FinalTestMethodExecute);
                                        productValidationInputData.setMethodInput("" + cellKeyValue, ""
                                        + cellValueInString);
                                        excelProductValidationInputDataMap.put(FinalTestMethodName,
                                        productValidationInputData);
                                        if (!previousIconMethodName.equals(FinalTestMethodName)) {
                                            previousIconMethodName = FinalTestMethodName;
                                            excelInputData
                                            .setExcelProductValidationInputData(excelProductValidationInputDataMap);
                                            productValidationInputData = new ExcelProductValidationInputData();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        
       
        return productMethodNameArray;
    }
    public void readFilterMethodsAndItsInput(ExcelInputData excelInputData, String excelPath, String excelSheetName,
    String startReadingFilterMethods, String stopReadingFilterMethods, int filtersStartDataRowCount,
    int filtersEndDataRowCount, int calculationDataRowCount, int calculationEndDataRowCount) {
        ArrayList<String> filterMethodNameArray = getFilterMethodNamesInIndexZero(excelPath, excelSheetName,
        filtersStartDataRowCount, stopReadingFilterMethods);
        GetExcelInput getInput = new GetExcelInput();
        for (int count = 0; count < excelInputData.getExcelFilterInputData().size(); count++) {
            ArrayList<HashMap<String, ExcelFilterInputData>> excelInputFilterArray = excelInputData
            .getExcelFilterInputData();
            HashMap<String, ExcelFilterInputData> excelInputFilterMap = excelInputFilterArray.get(count);
            String filterMethodNameKey = (String) excelInputFilterMap.keySet().toArray()[count];
            LOGGER.info("n ===================== Filters :  " + filterMethodNameKey + "=====================");
            ExcelFilterInputData filterInputData = excelInputFilterMap.get(filterMethodNameKey);
            HashMap<String, String> filterInputDataMap = filterInputData.getMethodInput();
            Set<String> keys = filterInputDataMap.keySet();
            for (String key : keys) {
                String singleKeyOfASingleFilterMethod = getInput.get_A_Value_Using_Key_Of_Filters_Method(
                filterMethodNameKey, key);
                LOGGER.info("n Filter Key :  " + key + " = " + singleKeyOfASingleFilterMethod);
            }
        }
        LOGGER.info("===n No of Filter methods " + filterMethodNameArray.size());
        int filterMethodInvocationCount = filterMethodNameArray.size();
        channelCalculationInputData(excelInputData, excelPath, excelSheetName, calculationDataRowCount,
        calculationEndDataRowCount, filterMethodInvocationCount);
    }
    @SuppressWarnings("resource")
    public ArrayList<String> getFilterMethodNamesInIndexZero(String excelPath, String excelSheetName,
    int filtersStartDataRowCount, String stopReadingFilterMethods) {
        String FinalTestMethodName = "";
        String FinalTestMethodExecute = "";
        String previousIconMethodName = "";
        boolean isSecondTimeValue = false;
        ExcelInputData excelInputData = ExcelInputData.getInstance();
        HashMap<String, ExcelFilterInputData> excelFilterInputDataMap = new HashMap<String, ExcelFilterInputData>();
        ArrayList<String> filterMethodNameArray = new ArrayList<String>();
        try {
            HSSFWorkbook wb;
            InputStream ExcelFileToRead = new FileInputStream(excelPath);
            wb = new HSSFWorkbook(ExcelFileToRead);
            HSSFSheet sheet = wb.getSheet(excelSheetName);
            HSSFRow row;
            boolean isReachedFilterData = false;
            // HSSFCell cell;
            ExcelFilterInputData filterInputData = new ExcelFilterInputData();
            Iterator<?> rows = sheet.rowIterator();
            while (rows.hasNext()) {
                String cellKeyValue = "";
                String TestMethodExecute = "";
                row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    if (row.getRowNum() > 0) {
                        if (!isReachedFilterData) {
                            if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                if (cell.getStringCellValue().contains(stopReadingFilterMethods)) {
                                    isReachedFilterData = true;
                                }
                                if (!isReachedFilterData && cell.getColumnIndex() == 0
                                && cell.getRowIndex() > filtersStartDataRowCount) {
                                    if (cell.getStringCellValue().length() > 0) {
                                        FinalTestMethodName = "" + cell.getStringCellValue();
                                        filterMethodNameArray.add("" + cell.getStringCellValue());
                                    }
                                }
                                if (cell.getColumnIndex() == 1) {
                                    TestMethodExecute = cell.getStringCellValue();
                                }
                                if (cell.getColumnIndex() == 2) {
                                    cellKeyValue = cell.getStringCellValue();
                                }
                                if (cell.getColumnIndex() == 3) {
                                    /*
                                    * Set single method here...
                                    */
                                    if (!isReachedFilterData && cell.getRowIndex() > filtersStartDataRowCount) {
                                        if (!previousIconMethodName.equals(FinalTestMethodName)) {
                                            FinalTestMethodExecute = TestMethodExecute;
                                        }
                                        filterInputData.setMethodInput(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT,
                                        FinalTestMethodExecute);
                                        filterInputData.setMethodInput("" + cellKeyValue,
                                        "" + cell.getStringCellValue());
                                        excelFilterInputDataMap.put(FinalTestMethodName, filterInputData);
                                        if (!previousIconMethodName.equals(FinalTestMethodName)) {
                                            previousIconMethodName = FinalTestMethodName;
                                            LOGGER.info("filterInputData = " + filterInputData.getMethodInput());
                                            LOGGER.info("excelFilterInputDataMap = " + excelFilterInputDataMap);
                                            excelInputData.setExcelFilterInputData(excelFilterInputDataMap);
                                            filterInputData = new ExcelFilterInputData();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        return filterMethodNameArray;
    }
    @SuppressWarnings("resource")
    public ArrayList<String> getMethodNamesInIndexZero(String excelPath, String excelSheetName,
    String stopReadingExcelUptoCalculationData) {
        ArrayList<String> testMethodNameArray = new ArrayList<String>();
        try {
            HSSFWorkbook wb;
            InputStream ExcelFileToRead = new FileInputStream(excelPath);
            wb = new HSSFWorkbook(ExcelFileToRead);
            HSSFSheet sheet = wb.getSheet(excelSheetName);
            HSSFRow row;
            boolean isReachedCalculationData = false;
            // HSSFCell cell;
            Iterator<?> rows = sheet.rowIterator();
            while (rows.hasNext()) {
                row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    if (row.getRowNum() > 0) {
                        if (!isReachedCalculationData) {
                            if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                if (cell.getStringCellValue().contains(stopReadingExcelUptoCalculationData)) {
                                    isReachedCalculationData = true;
                                }
                                if (cell.getColumnIndex() == 0) {
                                    if (cell.getStringCellValue().length() > 0) {
                                        testMethodNameArray.add("" + cell.getStringCellValue());
                                    }
                                }
                                } else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                if (cell.getColumnIndex() == 0) {
                                    String methodVal = String.valueOf(cell.getNumericCellValue());
                                    if (methodVal != null && methodVal.length() > 0) {
                                        testMethodNameArray.add("" + cell.getNumericCellValue());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        return testMethodNameArray;
    }
    public void channelCalculationInputData(ExcelInputData excelInputData, String excelPath, String excelSheetName,
    int readFromRow, int calculationEndDataRowCount, int filterMethodInvocationCount) {
        /*
        * CHANGE ME BASED ON CALCULATION COLUMN IN EXCEL
        */
        ArrayList<String> calculationRowHeaders = new ArrayList<String>();
        GetExcelInput getInput = new GetExcelInput();
        for (int count = 0; count < excelInputData.getExcelFilterInputData().size(); count++) {
            ArrayList<HashMap<String, ExcelFilterInputData>> excelInputFilterArray = excelInputData
            .getExcelFilterInputData();
            HashMap<String, ExcelFilterInputData> excelInputFilterMap = excelInputFilterArray.get(count);
            String filterMethodNameKey = (String) excelInputFilterMap.keySet().toArray()[count];
            calculationRowHeaders.add(filterMethodNameKey);
        }
        int calculationHeaderCellRowCoutn = readFromRow + 1;
        CommonMethods commonMethodobj = new CommonMethods();
        ExcelCalculationInputData getExcelData = new ExcelCalculationInputData();
        try {
            ExcelReader excelReaderObj = new ExcelReader(excelPath, excelSheetName);
            excelReaderObj.ColumnDictionary(calculationHeaderCellRowCoutn);
            } catch (BiffException e) {
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        HashMap<String, ArrayList<String>> calculationResultArray = commonMethodobj.readExcelDataWithRespectToHeaders(
        calculationRowHeaders, calculationHeaderCellRowCoutn, calculationEndDataRowCount);
        LOGGER.info("===n calculationRowHeaders :  " + calculationRowHeaders);
        LOGGER.info("===n calculationResultArray :  " + calculationResultArray);
        LOGGER.info("===n Calculation Result Array :  " + calculationResultArray.size());
        /*
        * CHANGE ME BASED ON CALCULATION COLUMN IN EXCEL
        */
        getExcelData.setMethodInput(calculationResultArray);
        excelInputData.setExcelCalculationInputData(getExcelData);
        Set<String> calculationKeys = calculationResultArray.keySet();
        ArrayList<String> singleCalculationValuesInArray = null;
        for (String singleCalcultionKey : calculationKeys) {
            singleCalculationValuesInArray = calculationResultArray.get(singleCalcultionKey);
            LOGGER.info("========" + singleCalcultionKey + "========");
            for (int countOfASingleCalculationHeader = 0; countOfASingleCalculationHeader < singleCalculationValuesInArray
            .size(); countOfASingleCalculationHeader++) {
                LOGGER.info("===n countOfASingleCalculationHeader "
                + singleCalculationValuesInArray.get(countOfASingleCalculationHeader));
            }
        }
        LOGGER.info("===n calucation count with header " + (singleCalculationValuesInArray.size()));
        LOGGER.info("===n calucation count without header " + (singleCalculationValuesInArray.size() - 1));
        int APICallMethodInvocationCount = singleCalculationValuesInArray.size() - 1;
        /*
        * call readYAMLPathFromExcelAndStoreItInObject (String YAMLPath) to
        * store YAML in Class objecy
        */
        String YAMLPath = getInput.get_A_Value_Using_Key_Of_A_Method("Open Channel Url", "YamlChannelPath");
        String YAMLChannelName = getInput.get_A_Value_Using_Key_Of_A_Method("Open Channel Url", "Yaml Name");
        String YAMLConfigName = getInput.get_A_Value_Using_Key_Of_A_Method("Open Channel Url", "YAMLAppConfigPath");
        String ExcelChannelName = getInput.get_A_Value_Using_Key_Of_A_Method("Open Channel Url", "Name");
        YAMLPath = YAMLPath.toString().trim();
        YAMLChannelName = YAMLChannelName.toString().trim();
        YAMLConfigName = YAMLConfigName.toString().trim();
        LOGGER.info("===n YAMLPath =  " + YAMLPath.toString().trim());
        LOGGER.info("===n YAMLChannelName =  " + YAMLChannelName.toString().trim());
        LOGGER.info("===n YAMLConfigName =  " + YAMLConfigName.toString().trim());
        if (YAMLConfigName != null) {
            LOGGER.info("===n YAMLconfigPath hereer =  " + YAMLConfigName.toString().trim());
            } else {
            LOGGER.info("===n NULL");
        }
        int productValidationInvocationCount = readYAMLPathFromExcelAndStoreItInObject(excelInputData, YAMLChannelName,
        YAMLPath, YAMLConfigName);
        /*
        * Run Test after reading the invocation count of calculation inputs and
        * filter methods
        */
        runTestClass(APICallMethodInvocationCount, filterMethodInvocationCount, productValidationInvocationCount,
        ExcelChannelName);
    }
    public int readYAMLPathFromExcelAndStoreItInObject(ExcelInputData excelInputData, String channelName,
    String YAMLPath, String yamlConfigPath) {
        /* Read Required Yaml and store in ExcelInput Data class */
        GetExcelInput getInput = new GetExcelInput();
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey,
        EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
        int productValidationInvocationCount = 0;
        if (pageUrl.contains("cerberus.moneysmart.sg/credit-cards")) {
            productValidationInvocationCount = 2;
            } else {
            YamlReader reader, newFilereader;
            try {
                LOGGER.info("===n YAMLPath =  " + YAMLPath);
                File f = new File(YAMLPath);
                reader = new YamlReader(new BufferedReader(new FileReader(f)));
                Object YAMLObject = reader.read();
                Map YAMLMap = (Map) YAMLObject;
                LOGGER.info("===n channelName =  " + channelName);
                Object YAMLParticularChannelObject;
                Object channelYAMLParticularChannelObject = YAMLMap.get(channelName);
                Map channelMapObject = (Map) channelYAMLParticularChannelObject;
                if (channelMapObject.containsKey("configuration_file")) {
                    YAMLPath = YAMLPath.split("product_channel.yml")[0];
                    String configurationFilePath = (String) channelMapObject.get("configuration_file");
                    YAMLPath = YAMLPath + configurationFilePath;
                    File newFile = new File(YAMLPath);
                    newFilereader = new YamlReader(new BufferedReader(new FileReader(newFile)));
                    YAMLParticularChannelObject = newFilereader.read();
                    } else {
                    LOGGER.info("===n channelName YAML =  " + channelName);
                    YAMLParticularChannelObject = YAMLMap.get(channelName);
                }
                LOGGER.info("===n YAMLPath =  " + YAMLPath);
                Map YAMLParticularChannelMap = (Map) YAMLParticularChannelObject;
                excelInputData.setYAMLData(YAMLParticularChannelObject);
                LOGGER.info("===n YAMLParticularChannelMap =  " + YAMLParticularChannelMap);
                Object TableFieldsObject = YAMLParticularChannelMap.get("table_fields");
                Map TableFieldsMap = (Map) TableFieldsObject;
                Object ColumnsObject = TableFieldsMap.get("columns");
                Map ColumnsMap = (Map) ColumnsObject;
                productValidationInvocationCount = ColumnsMap.size();
                if (ColumnsMap.containsKey("type")) {
                    if (channelName.equals("travel-insurance")) {
                        ArrayList<HashMap<String, ExcelProductValidationInputData>> excelInputProductValidatinArray = excelInputData
                        .getExcelProductValidationInputData();
                        LOGGER.info("===n travel-insurance product name == " + excelInputProductValidatinArray);
                        productValidationInvocationCount = excelInputProductValidatinArray.size() - 1;
                        } else {
                        productValidationInvocationCount = productValidationInvocationCount - 1;
                    }
                }
                readYAMLconfigPathFromExcelAndStoreItInObject(excelInputData, yamlConfigPath);
                } catch (Exception e) {
                // TODO Auto-generated catch block
                LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            }
        }
        /*
        * Add Plus one here, because bank name validation is added for all
        * channels irrespective of the
        */
        return productValidationInvocationCount + 1;
    }
    public void checkApiinexcel(ExcelInputData excelInputData, String configpath) {
    }
    public void readYAMLconfigPathFromExcelAndStoreItInObject(ExcelInputData excelInputData, String configpath) {
        LOGGER.info("===n YAMLconfigPath =  " + configpath.toString().trim());
        YamlReader reader;
        try {
            File newFile = new File(configpath.toString().trim());
            reader = new YamlReader(new BufferedReader(new FileReader(newFile)));
            Object YAMLconfigObject = reader.read();
            excelInputData.setYAMLConfigData(YAMLconfigObject);
            } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        
    }
    public void runTestClass(int calculationMethodInvocationCount, int filterMethodInvocationCount,
    int productValidationInvocationCount, String channelName) {
        try {
            // Start the TestNG Run via Java Class instead of XML file
            RunSingleSuiteTest runSuite = new RunSingleSuiteTest();
            // CHANGE ME BASED ON CHANNEL
            runSuite.SingleSuit(calculationMethodInvocationCount, filterMethodInvocationCount,
            productValidationInvocationCount, channelName);
            } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
    }
}