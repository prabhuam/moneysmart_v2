package com.model;
/**
*
* @author Shenll Technology Solutions
*
*/
public class Constants {
    public static final class EXCEL_METHODS_INPUT {
        /*
        * Initialize openwebpage method key
        */
        public static final String Openwebpage_ChaneelFilterTypeKey = "Category Code";
        public static final String Openwebpage_MethodNameKey = "Open Channel Url";
        public static final String Openwebpage_ChaneelNameKey = "Name";
        public static final String Openwebpage_ChaneelNameKeyinYaml = "Yaml Name";
        public static final String Openwebpage_ChanellURlKey = "URL";
        public static final String Openwebpage_Country = "Country";
        public static final String Less_DetailMethodNameKey = "Less Detail";
        public static final String ApplyNowMethodNameKey = "Apply Now";
        public static final String YamlChanellPathKey = " YamlChannelPath";
        public static final String Icon_MethodNameKey = "Icon";
        public static final String FAQ_MethodNameKey = "FAQ";
        public static final String Ask_A_Question_MethodNameKey = "Ask a Question";
        public static final String Refinancing_Rates_MethodNameKey = "Refinancing Rates";
        public static final String Testimonial_MethodNameKey = "Testimonial";
        public static final String CompareValidation_MethodNameKey = "Compare";
        public static final String TitleValidation_MethodNameKey = "Title Validation";
        public static final String DescriptionValidation_MethodNameKey = "Description Validation";
        public static final String BannerValidation_MethodNameKey = "Banner";
        public static final String BankWidgetValidation_MethodNameKey = "Bank widget";
        public static final String ShowMoreResultMethodNameKey = "Show More Result";
        public static final String FacebookLogin_Validation_MethodNameKey = "Facebook Login";
        public static final String EmailLogin_Validation_MethodNameKey = "Email Login";
        public static final String GooglePlusLogin_Validation_MethodNameKey = "GooglePlus Login";
        public static final String Detail_Title_MethodNameKey = "Detail Page Title";
        public static final String Detail_Reasons_MethodNameKey = "Reasons";
        public static final String ExpectedResultKey = "Expected Test Result";
        public static final String TestDescriptionKey = "Test Description";
        public static final String ActualResultKey = "Actual Result";
        public static final String DetailApplyNowMethodNameKey = "DetailPage Apply NowButton";
        public static final String DetailPageFooterApplyNowMethodNameKey = "Footer Apply NowButton";
        public static final String TitleValidation_TextKey = "Title";
        public static final String DescriptionValidation_DescriptionKey = "Description";
        public static final String DetailPageProduct_MethodNameKey = "Detail PageProduct";
        public static final String GotoSiteMethodNameKey = "Go to Site";
        public static final String FooterGotoSiteMethodNameKey = "Footer Go to Site";
        public static final String ProductCountMethodNameKey = "Product Count Validation";
        public static final String ResultHeadTittleValidation_MethodNameKey = "Result Head Title";
        public static final String Browser_MethodNameKey = "Browser";
        public static final String Browser_Firefox = "Firefox";
        public static final String Browser_Chrome = "Chrome";
        public static final String Browser_Safari = "Safari";
        public static final String DetailPagePermaLink = "PermaLink";
        public static final String DetailPageProductName = "Detail Product Name";
        /* DETAIL PAGE SECTION STARTS */
        public static final String DetailProductKey = "Detail Product";
        public static final String ProductNameKey = "Product Name";
        public static final String PermalinkKey = "Permalink";
        public static final String TravelInconvenienceMethodNameKey = "Travel Inconvenience";
        public static final String MedicalCoverageMethodNameKey = "Medical Coverage";
        public static final String PersonalProtectionMethodNameKey = "Personal Protection";
        public static final String TravelDelaywebKey = "Travel Delay WebElement";
        public static final String TravelDelaynodeKey = "Travel Delay";
        public static final String TravelDelaywebTitleKey = "Travel Delay KeyElement";
        public static final String TravelCancelationwebKey = "Travel Cancelation / Postponement WebElement";
        public static final String TravelCancelationnodeKey = "Travel Cancelation / Postponement";
        public static final String TravelCancelationTitleKey = "Travel Cancelation / Postponement KeyElement";
        public static final String DelayedBaggagewebKey = "Delayed Baggage WebElement";
        public static final String DelayedBaggagenodeKey = "Delayed Baggage";
        public static final String DelayedBaggageTitleKey = "Delayed Baggage KeyElement";
        public static final String OverseasMedicalExpenseswebKey = "OverseasMedicalExpenses WebElement";
        public static final String OverseasMedicalExpensesnodeKey = "Overseas Medical Expenses";
        public static final String OverseasMedicalExpensesTitleKey = "OverseasMedicalExpenses KeyElement";
        public static final String DailyHospitalAllowancewebKey = "DailyHospitalAllowance WebElement";
        public static final String DailyHospitalAllowancenodeKey = "Daily Hospital Allowance";
        public static final String DailyHospitalAllowanceTitleKey = "DailyHospitalAllowance KeyElement";
        public static final String DeathDisabillitywebKey = "Death / Total Permanent Disabillity WebElement";
        public static final String DeathDisabillitynodeKey = "Death / Total Permanent Disabillity";
        public static final String DeathDisabillityTitleKey = "Death / Total Permanent Disabillity KeyElement";
        public static final String TripCurtailmentwebKey = "Trip Curtailment WebElement";
        public static final String TripCurtailmentnodeKey = "Trip Curtailment";
        public static final String TripCurtailmentTitleKey = "Trip Curtailment KeyElement";
        public static final String MissedflightconnectionwebKey = "Missed flight connection WebElement";
        public static final String MissedflightconnectionnodeKey = "Missed flight connection";
        public static final String MissedflightconnectionTitleKey = "Missed flight connection KeyElement";
        public static final String LossDamageofBaggagewebKey = "Loss/Damage of Baggage WebElement";
        public static final String LossDamageofBaggagenodeKey = "Loss/Damage of Baggage";
        public static final String LossDamageofBaggageTitleKey = "Loss/Damage of Baggage KeyElement";
        public static final String LossofTravelDocumentswebKey = "Loss of Travel Documents WebElement";
        public static final String LossofTravelDocumentsnodeKey = "Loss of Travel Documents";
        public static final String LossofTravelDocumentsTitleKey = "Loss of Travel Documents KeyElement";
        public static final String PostTripMedicalExpenseswebKey = "Post-Trip Medical Expenses WebElement";
        public static final String PostTripMedicalExpensesnodeKey = "Post-Trip Medical Expenses";
        public static final String PostTripMedicalExpensesTitleKey = "Post-Trip Medical Expenses KeyElement";
        public static final String SponsoredImageValidation_MethodNameKey = "Sponsored Image";
        /* DETAIL PAGE SECTION ENDS */
    }
    public static final class TEST_RESULT {
        /*
        * Initialize test case RESULT KEY
        */
        public static final String R_TESTIMONIAL = "Testimonial";
        public static final String R_ICON = "Icon";
        public static final String R_METHOD_EXECUTE_OR_NOT = "Execute";
        public static final String R_IS_SUCCESS = "IsMethodSuccess";
        public static final String R_IS_EXCEPTION = "ExceptionOccured";
        public static final String R_METHOD_NAME = "MethodName";
        public static final String R_METHOD_PRIOTITY = "MethodPriority";
        public static final String R_MESSAGE = "Message";
        public static final String R_EXCEPTION_ERROR_MESSAGE = "Exception";
        public static final String R_COMMENTS = "Comments";
        public static final String R_ACTUAL_RESULT = "ActualResult";
        public static final String R_EXPECTED_RESULT = "ExpectedResult";
        public static final String R_DESRIPTION = "Description";
        public static final String R_ERROR_RESULT = "ActualResult";
        public static final String R_MULTIPLE_EXECUTE_FOR_TESTIMONIAL = "ExecuteOrNot";
        public static final String R_MULTIPLE_EXECUTE_FOR_ICON = "ExecuteOrNot";
    }
    public static final class TEST_CASE_TITLE {
        /*
        * Initialize test case titles
        */
        public static final String T_OPEN_MONEY_SMART_WEBPAGE = "Open home loan webpage";
        public static final String T_USERNAME_VALIDATION = "Username validation";
        public static final String T_PASSWORD_VALIDATION = "Password validation";
    }
    public static final class TEST_CASE_SUCCESS_MESSAGES {
        /*
        * Initialize test case success messages
        */
        public static final String SM_WEBPAGE_LOADED = "Home Loan webpage loaded successfully";
        public static final String SM_OPEN_LOGIN_POPUP = "Login popup opened successfully";
        public static final String SM_USERNAME_VALIDATION = "Username is validated";
        public static final String SM_ENTERED_USERNAME = "Username entered";
        public static final String SM_PASSWORD_VALIDATION = "Password is validated";
        public static final String SM_ENTERED_PASSWORD = "Password entered";
        public static final String SM_LOGIN = "Logged in successfully";
        public static final String SM_GET_ACCESS_TOKEN = "Retrieve Access Token to call home loan webservice and get result";
        public static final String SM_CALL_WEBSERVICE = "Call webservice to get the home loan data";
        public static final String SM_READ_CALCULATION_INPUT_FROM_EXCEL = "Read calculation input from excel";
        public static final String SM_DETAIL = "Detail Validated Successully";
    }
    public static final class TEST_CASE_FAILURE_MESSAGES {
        /*
        * Initialize test case failure messages
        */
        public static final String FM_USERNAME_VALIDATION = "Username is not validated";
        public static final String FM_PASSWORD_VALIDATION = "Password is not validated";
        public static final String FM_USERNAME_OR_PASSWORD_VALIDATION = "Username or Password is invalid";
        public static final String FM_GET_ACCESS_TOKEN = "Failed to retrive access token";
        public static final String FM_CALL_WEBSERVICE = "Failed to connect webservice";
    }
    public static final class MYSTORE {
        public static String ACESSTOKEN;
        public static String SINGLEPRODUCTJSONAPI;
    }
}