package com.model;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import java.util.ArrayList;
import java.util.HashMap;

public class ExcelCalculationInputData {

	private HashMap<String, ArrayList<String>> calculationData;

	public ExcelCalculationInputData() {
		this.calculationData = new HashMap<String, ArrayList<String>>();
	}

	public void setMethodInput(HashMap<String, ArrayList<String>> calculationData) {
		this.calculationData = calculationData;
	}

	public HashMap<String, ArrayList<String>> getMethodInput() {
		return calculationData;
	}

}
