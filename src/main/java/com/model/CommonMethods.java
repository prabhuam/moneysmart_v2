package com.model;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import java.util.ArrayList;
import java.util.HashMap;

import java.util.logging.Level;

import java.util.logging.Logger;

public class CommonMethods {
	private static final Logger LOGGER = Logger.getLogger(CommonMethods.class.getName());

	public ArrayList<ArrayList<HashMap<String, String>>> readExcelData(ArrayList<String> calculationDataHeaders,
			int calculateFromCount, int calculateToCount) {
		// Get the data from excel file
		ArrayList<ArrayList<HashMap<String, String>>> allCalculationHeader = new ArrayList<ArrayList<HashMap<String, String>>>();

		for (int rowCnt = calculateFromCount; rowCnt < calculateToCount; rowCnt++) {
			ArrayList<HashMap<String, String>> singleCalculationHeader = new ArrayList<HashMap<String, String>>();
			for (int caluationHeaderCount = 0; caluationHeaderCount < calculationDataHeaders.size(); caluationHeaderCount++) {
				HashMap<String, String> headerValueMap = new HashMap<String, String>();
				headerValueMap.put(calculationDataHeaders.get(caluationHeaderCount), ExcelReader.ReadCell(
						ExcelReader.GetCell(calculationDataHeaders.get(caluationHeaderCount)), rowCnt));
				singleCalculationHeader.add(headerValueMap);
			}

			allCalculationHeader.add(singleCalculationHeader);
		}
		return allCalculationHeader;
	}

	public HashMap<String, ArrayList<String>> readExcelDataWithRespectToHeaders(
			ArrayList<String> calculationDataHeaders, int calculateFromCount, int calculateToCount) {
		// Get the data from excel file
		HashMap<String, ArrayList<String>> allCalculationHeader = new HashMap<String, ArrayList<String>>();

		for (int caluationHeaderCount = 0; caluationHeaderCount < calculationDataHeaders.size(); caluationHeaderCount++) {
			ArrayList<String> singleCalculationHeader = new ArrayList<String>();
			for (int rowCnt = calculateFromCount; rowCnt < calculateToCount; rowCnt++) {
				String singleCellValue = ExcelReader.ReadCell(
						ExcelReader.GetCell(calculationDataHeaders.get(caluationHeaderCount)), rowCnt);

				LOGGER.info("===\n singleCellValue :  " + singleCellValue);

				singleCalculationHeader.add(singleCellValue);
			}

			LOGGER.info("===\n allCalculationHeader :  " + allCalculationHeader);
			LOGGER.info("===\n allCalculationHeader :  " + calculationDataHeaders.get(caluationHeaderCount));

			allCalculationHeader.put(calculationDataHeaders.get(caluationHeaderCount), singleCalculationHeader);
		}
		return allCalculationHeader;
	}
}
