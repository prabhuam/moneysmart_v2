package com.model;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import java.util.HashMap;

public class ExcelProductValidationInputData {
	public HashMap<String, String> productValidationInput;

	public ExcelProductValidationInputData() {
		this.productValidationInput = new HashMap<String, String>();
	}

	public void setMethodInput(String key, String value) {
		this.productValidationInput.put(key, value);
	}

	public HashMap<String, String> getMethodInput() {
		return productValidationInput;
	}
}
