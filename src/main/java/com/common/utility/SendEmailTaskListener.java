package com.common.utility;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
public interface SendEmailTaskListener {
	public void sendEmailSuccessListener(int statusCode, String statusMessage);
	public void sendEmailFailureListener(int statusCode, String statusMessage);
}
