package com.common.utility;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
/**
* A program that demonstrates how to upload files from local computer to a
* remote FTP server using Apache Commons Net API.
*
* @author www.codejava.net
*/
public class FTPFileUploadTask {
     static final Logger LOGGER = Logger.getLogger(FTPFileUploadTask.class.getName());
     static final int FAILURE_CODE = 0;
    private static final int SUCCESS_CODE = 1;
    private static final String SUCCESS_MESSAGE = "File is uploaded successfully.";
     static final String EXCEPTION_MESSAGE = "Exception during upload : ";
    /*
    * FTP credentials. Please keep these credentials securely
    */
    private String server = "shenll.net";
    private int port = 21;
     String user = "demoshenll";
     String pass = "D@mo2015eN05L!";
     String workingDirectory = "/MoneySmartTesting";
     String uploadStatus = "File upload status";
    public String uploadThisFileToServer(FTPFileUploadListener fileUploadListener, String uploadFilePath,
    String uploadFileName) {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            // Changes working directory
            boolean success = ftpClient.changeWorkingDirectory(workingDirectory);
            showServerReply(ftpClient);
            if (success) {
                LOGGER.info("Successfully changed working directory.");
                } else {
                LOGGER.info("Failed to change working directory. See server's reply.");
            }
            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File(uploadFilePath + uploadFileName);
            InputStream inputStream = new FileInputStream(firstLocalFile);
            LOGGER.info("Uload File Path : " + uploadFilePath);
            LOGGER.info("File uploading task started...");
            boolean done = ftpClient.storeFile(uploadFileName, inputStream);
            inputStream.close();
            if (done) {
                uploadStatus = SUCCESS_MESSAGE;
                fileUploadListener.uploadTaskSuccessListener(SUCCESS_CODE, uploadStatus);
                } 
            LOGGER.info("Upload Status message : " + uploadStatus);
            } catch (IOException ex) {
            LOGGER.info("Error: " + ex.getMessage());
            LOGGER.log(Level.SEVERE, "EXCEPTION", ex);
            uploadStatus = EXCEPTION_MESSAGE + ex.getMessage();
            fileUploadListener.uploadTaskFailureListener(FAILURE_CODE, uploadStatus);
            } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
                } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "EXCEPTION", ex);
            }
        }
        return uploadStatus;
    }
    private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                LOGGER.info("SERVER: " + aReply);
            }
        }
    }
}