package com.common.utility;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
public interface WebserviceRequestListener {
	public void webserviceRequestSuccessListener(int statusCode, String statusMessage, String apiResult);
	public void webserviceRequestFailureListener(int statusCode, String statusMessage);
}
